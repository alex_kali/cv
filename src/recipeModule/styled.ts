import styled from "styled-components";

export const RecipeAppStyled = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`