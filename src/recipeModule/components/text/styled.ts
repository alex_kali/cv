import styled from "styled-components";

export const H1 = styled.h1`
  font-size: 20px;
`

export const H2 = styled.h2`
  font-size: 16px;
`

export const H4 = styled.h4`
  font-size: 14px;
`

export const Title = styled(H2)`
  margin-top: 10px;
  margin-bottom: 5px;
`