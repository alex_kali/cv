import styled from "styled-components";

export const TagsInputStyled = styled.div`
  position: relative;
  z-index: 10;
`

export const DropItemsStyled = styled.div`
  position: absolute;
  left: 0;
  top: 100%;
  width: 100%;
  background: rgba(200, 200, 200, 1);
  padding: 10px;
`

export const ItemStyled = styled.label`
  margin-bottom: 6px;
  &:last-child {
    margin-bottom: 0;
  }
`