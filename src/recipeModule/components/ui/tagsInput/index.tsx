import React, {FC, useCallback, useEffect, useRef, useState} from "react";
import {useField} from "react-redux-hook-form";
import {
  CheckboxInputStyled,
  CheckboxTextStyled, CustomCheckStyled, CustomRadioStyled, TagCancelStyled, TagStyled,
  WrapperCheckboxInputStyled,
  WrapperCheckboxItemStyled, WrapperTagStyled
} from "src/recipeModule/components/ui/selectInput/styled";
import {initiateRequest} from "src/recipeModule/components/ui/tagsInput/request";
import {DropItemsStyled, ItemStyled, TagsInputStyled} from "src/recipeModule/components/ui/tagsInput/styled";
import {TextInputStyled} from "src/recipeModule/components/ui/textInput/styled";
import {useOutsideClick} from "src/recipeModule/utils/useOutsideClick";
interface ITagsInput {
  name: string;
  url: string;
}


export const TagsInput:FC<ITagsInput> = ({name, url}) => {
  const {useData} = useField({
    name: name,
  })
  
  const [data, setData] = useData()
  const ref:any = useRef()
  const getData = useCallback(initiateRequest({url: url}), [url]);

  const [tags, setTags] = useState<Array<string>>([])
  const [text, setText] = useState('');
  const [isOpen, setIsOpen] = useState(false)
  
  useEffect(() => {
    getData(text, setTags)
  }, [text])
  
  const onChange = (value: string) => {
    if(data){
      if(data.indexOf(value) === -1){
        setData([...data,value])
      }else{
        const newData = (data.filter((i:string) => i !== value))
        if(newData.length) {
          setData(newData)
        }else{
          setData(undefined)
        }
      }
    }else{
      setData([value])
    }
  }
  
  useOutsideClick(ref, () => setIsOpen(false))

  return (
    <TagsInputStyled>
      {data && data.map((name:string) => (
        <WrapperTagStyled key={name}>
          <TagStyled>
            {name}
            <TagCancelStyled onClick={() => onChange(name)}/>
          </TagStyled>
        </WrapperTagStyled>
      ))}
      <div ref={ref} onClick={() => setIsOpen(true)}>
        <TextInputStyled
          isFocus={isOpen}
          value={text || ''}
          onChange={(e)=> {setText(e.target.value)}}
        />
        {tags.length !== 0 && isOpen && (
          <DropItemsStyled onClick={(e) => e.stopPropagation()}>
            {tags.map((name) => (
              <ItemStyled key={name} htmlFor={name}>
                <WrapperCheckboxItemStyled checked={data ? data.includes(name) : false}>
                  <CheckboxTextStyled>{name}</CheckboxTextStyled>
                  <CheckboxInputStyled
                    id={name}
                    type="checkbox"
                    name={name}
                    value={name}
                    checked={data ? data.includes(name) : false}
                    onChange={(e) => onChange(e.target.value)}
                  />
                  <CustomRadioStyled className={'checkbox'}><CustomCheckStyled /></CustomRadioStyled>
                </WrapperCheckboxItemStyled>
              </ItemStyled>
            ))}
          </DropItemsStyled>
        )}
      </div>
    </TagsInputStyled>
  )
}