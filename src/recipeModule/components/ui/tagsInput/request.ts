import {wrapperFetch} from "react-redux-request-controller";


export const initiateRequest = ({ url }: {url: string}) => {
  let search: string | null = null ;
  
  const get = (text: string, setData: (data: Array<string>) => void) => {
    if(text) {
      search = text
      setTimeout(async () => {
        if(search === text) {
          const response: any = await wrapperFetch({url: `${url}${search}/`, method: 'GET'})
          let data = await response.json();
          if(data.search === search) {
            setData(data.data)
          }
        }
      }, 500)
    }else{
      setData([])
    }
  }
  
  return get
}