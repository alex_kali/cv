import {FC} from "react";
import {
  DescriptionStyled,
  ImageStyled,
  NameStyled,
  RecipePreviewStyled, WrapperContentStyled
} from "src/recipeModule/components/ui/recipe/styled";
import {useNavigation} from "src/recipeModule/utils/useNavigation";

interface IRecipePreview {
  id: number | string;
  img: string;
  name: string;
  description: string;
}

export const RecipePreview:FC<IRecipePreview> = ({id, name, img, description}) => {
  const navigate = useNavigation()
  
  return (
    <RecipePreviewStyled onClick={() => navigate(`/recipe/recipe/${id}`)}>
      <NameStyled>{name}</NameStyled>
      <WrapperContentStyled>
        <ImageStyled image={img}/>
        <DescriptionStyled>{description}</DescriptionStyled>
      </WrapperContentStyled>
    </RecipePreviewStyled>
  )
}