import {H1, H2} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const RecipePreviewStyled = styled.a`
  width: calc(50% - 5px);
  height: 250px;
  border: 2px solid rgba(0,0,0,0.4);
  padding: 6px;
  cursor: pointer;
  border-radius: 4px;
  display: flex;
  flex-direction: column;
`

export const ImageStyled = styled.div<{image: string}>`
  width: 220px;
  height: 100%;
  background-image: url(${props => props.image});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  border-radius: 4px;
  flex: 1 1 220px;
`

export const WrapperContentStyled = styled.div`
  display: flex;
  flex-direction: row;
  flex-grow: 1;
  flex-shrink: 1;
  overflow: hidden;
`


export const NameStyled = styled(H1)`
  text-align: center;
  margin-top: 3px;
  margin-bottom: 7px;
  padding-left: 10px;
  padding-right: 10px;
`

export const DescriptionStyled = styled(H2)`
  display: block;
  flex: 1 1 250px;
  padding: 10px;
`