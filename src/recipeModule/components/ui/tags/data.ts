import {ReactComponent as arm} from "./icons/arm.svg";
import {ReactComponent as defaultIcon} from "./icons/default.svg";
import {ReactComponent as df} from "./icons/df.svg";
import {ReactComponent as egg} from "./icons/egg.svg";
import {ReactComponent as frozen} from "./icons/frozen.svg";
import {ReactComponent as gf} from "./icons/gf.svg";
import {ReactComponent as healthy} from "./icons/healthy.svg";
import {ReactComponent as heart} from "./icons/heart.svg";
import {ReactComponent as sugar} from "./icons/sugar.svg";
import {ReactComponent as thin} from "./icons/thin.svg";
import {ReactComponent as vegeterian} from "./icons/vegeterian.svg";

export const tagIcons: any = {
  "Freezable": frozen,
  "Healthy": healthy,
  "High-protein": arm,
  "Low calorie": thin,
  "Low fat": thin,
  "Vegetarian": vegeterian,
  "Gluten-free": gf,
  "Vegan": vegeterian,
  "Dairy-free": df,
  "Egg-free": egg,
  "Easily doubled": heart,
  "Easily halved": heart,
  "Low sugar": sugar,
  "Keto": defaultIcon,
  "Low carb": defaultIcon,
  "High-fibre": defaultIcon,
  "Nut-free": defaultIcon,
}