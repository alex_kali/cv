import styled from "styled-components";

export const TagsStyled = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`

export const TagStyled = styled.div`
  margin-right: 30px;
  display: flex;
  align-items: center;
  gap: 10px;
  padding: 2px;
`

export const TagIconStyled = styled.div`
  width: 15px;
  height: 15px;
  svg {
    width: 100%;
    height: 100%;
  }
`

export const TagNameStyled = styled.div`

`