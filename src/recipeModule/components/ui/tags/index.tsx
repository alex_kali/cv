import React, {FC} from "react";
import {tagIcons} from "src/recipeModule/components/ui/tags/data";
import {TagIconStyled, TagNameStyled, TagsStyled, TagStyled} from "src/recipeModule/components/ui/tags/styled";

interface ITags {
  tags: Array<string>;
}
export const Tags:FC<ITags> = ({tags}) => {
  return (
    <TagsStyled>
      {tags.map((item) => {
        const Icon = tagIcons?.[item] || tagIcons['default']
        
        return (
          <TagStyled>
            <TagIconStyled>
              <Icon/>
            </TagIconStyled>
            <TagNameStyled>{item}</TagNameStyled>
          </TagStyled>
        )})}
    </TagsStyled>
  )
}