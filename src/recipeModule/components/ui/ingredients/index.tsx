import {FC} from "react";
import {IngredientsStyled, IngredientStyled} from "src/recipeModule/components/ui/ingredients/styled";
import {IIngredient} from "src/recipeModule/store/detail/model";

interface IIngredients {
  ingredients: Array<IIngredient>;
}
export const Ingredients:FC<IIngredients> = ({ingredients}) => {
  
  return (
    <IngredientsStyled>
      {ingredients.map((item) => (<IngredientStyled>{`${item.name} ${item.value}`}</IngredientStyled>))}
    </IngredientsStyled>
  )
}