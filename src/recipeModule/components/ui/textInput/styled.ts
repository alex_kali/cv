import {
  BORDER_INPUT,
  BORDER_RADIUS_INPUT, COLOR_INPUT, FONT_SIZE_INPUT,
  HEIGHT_INPUT,
  MARGIN_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT, PADDING_INPUT,
  WIDTH_INPUT
} from "../styles";
import styled from "styled-components";


export const TextInputStyled = styled.input<{isFocus?: boolean}>`
  background: none;
  margin-top: ${MARGIN_INPUT};
  margin-bottom: ${MARGIN_INPUT};
  width: 100%;
  border: ${BORDER_INPUT};
  border-radius: ${BORDER_RADIUS_INPUT};
  height: ${HEIGHT_INPUT};
  font-size: ${FONT_SIZE_INPUT};
  padding-left: ${PADDING_INPUT};
  padding-right: ${PADDING_INPUT};
  color: ${COLOR_INPUT};
  opacity: ${props => props.isFocus ? `${OPACITY_FOCUS_INPUT}` : `${OPACITY_INPUT}`};
  &:focus{
    outline: none;
    opacity: ${OPACITY_FOCUS_INPUT};
  }
`