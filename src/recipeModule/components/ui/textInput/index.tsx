import {FC} from "react";
import {useField} from "react-redux-hook-form";
import {TextInputStyled} from "src/recipeModule/components/ui/textInput/styled";

interface ITextInput {
  name: string;
}

export const TextInput:FC<ITextInput> = ({name}) => {
  const {useData} = useField({
    name: name,
  })
  
  const [data, changeData] = useData()
  
  return (
    <TextInputStyled
      value={data || ''}
      onChange={(e)=> {changeData(e.target.value)}}
    />
  )
}