export const MARGIN_INPUT = '4px'
export const HEIGHT_INPUT = '40px'
export const BORDER_RADIUS_INPUT = '40px'
export const FONT_SIZE_INPUT = '18px'
export const PADDING_INPUT = '15px'
export const COLOR_INPUT = '#73777D'
export const BORDER_INPUT = '2px solid #73777D'
export const WIDTH_INPUT = 'calc(100% - 34px)'

export const ERROR_COLOR_INPUT = 'rgb(129,46,46)'
export const ERROR_BORDER_COLOR_INPUT = 'rgb(129,46,46)'

export const OPACITY_INPUT = '0.3'
export const OPACITY_FOCUS_INPUT = '0.9'