import {FC} from "react";
import {
  StepDescriptionsStyled,
  StepNameStyled,
  StepsStyled,
  StepStyled
} from "src/recipeModule/components/ui/steps/styled";

interface ISteps {
  steps: Array<string>;
}

export const Steps:FC<ISteps> = ({steps}) => {
  
  return (
    <StepsStyled>
      {steps.map((item, index) => (
        <StepStyled>
          <StepNameStyled>STEP {index + 1}</StepNameStyled>
          <StepDescriptionsStyled>{item}</StepDescriptionsStyled>
        </StepStyled>
      ))}
    </StepsStyled>
  )
}