import {H2, H4} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const StepsStyled = styled.div`

`

export const StepStyled = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
`

export const StepNameStyled = styled(H2)`
  margin-bottom: 4px;
`

export const StepDescriptionsStyled = styled(H4)`

`