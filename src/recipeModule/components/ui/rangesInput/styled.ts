import {H2} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const RangesInputStyled = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
`

export const RangeStyled = styled.div<{isDisable: boolean}>`
  height: 25px;
  display: flex;
  justify-content: space-between;
  transition-duration: 0.2s;
  ${props => props.isDisable ? `
    opacity: 0.5;
  ` : ``}
`

export const NameStyled = styled(H2)`
  &:first-letter {
    text-transform: uppercase;
  }
`

export const ValueStyled = styled.div`

`

export const WrapperRangeStyled = styled.div`
  width: calc(100% - 100px);
  position: relative;
  display: flex;
  justify-content: space-between;

  .input-range__slider{
    background: rgba(120,120,120,1);
    border-color: rgba(120,120,120,1);
  }
  
  .input-range__track--active{
    background: rgba(120,120,120,1);
  }
  
  * {
    transition-duration: 0.05s;
  }
  
  .input-range {
    position: absolute;
    left: 40px;
    top: 2px;
    width: calc(100% - 100px);
  }
  
  .input-range__label {
    display: none;
  }
  
  & > .input-range:nth-child(2){
    z-index: 2;
    .input-range__track {
      background: rgba(0,0,0,0);
    }
    .input-range__track--active {
      background: #eeeeee;
    }
  }

  & > .input-range:nth-child(3) {
    .input-range__slider-container {
      z-index: 3;
    }
  }
`