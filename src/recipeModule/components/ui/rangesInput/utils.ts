export const stepToValue = (step: number, name: string) => {
  if(name === 'kcal'){
    if(step < 20) {
      return step
    }
    if(step < 48) {
      return (step - 18) * 10
    }
    return (step * 5 - 210) * 10
  }else{
    if(step < 20) {
      return step / 10
    }
    if(step < 48) {
      return step - 18
    }
    return step * 5 - 210
  }
}
