import {FC} from "react";
import {InputRange} from "src/recipeModule/components/ui/rangesInput/inputRange";
import {RangesInputStyled} from "src/recipeModule/components/ui/rangesInput/styled";
import "react-input-range/lib/css/index.css";
interface IRangesInput {
  data: Array<string>;
  name: string;
}

export const RangesInput:FC<IRangesInput> = ({data}) => {
  return (
    <RangesInputStyled>
      {data.map((name) => (
        <InputRange name={name}/>
      ))}
    </RangesInputStyled>
  )
}