import {FC, useEffect, useState} from "react";
import {default as MainInputRange} from 'react-input-range';
import {useField} from "react-redux-hook-form";
import {
  RangeStyled,
  NameStyled,
  WrapperRangeStyled,
  ValueStyled
} from "src/recipeModule/components/ui/rangesInput/styled";
import {stepToValue} from "src/recipeModule/components/ui/rangesInput/utils";

interface IInputRange {
  name: string;
}

export const InputRange:FC<IInputRange> = ({name}) => {
  const {useData} = useField({
    name: name,
  })
  
  const [, changeData] = useData()
  
  
  const [valueStart, setValueStart] = useState(0)
  const [valueEnd, setValueEnd] = useState(102)
  const [isDisable, setIsDisable] = useState(true)
  
  useEffect(() => {
    if((valueStart !== 0 || valueEnd !== 102) && isDisable){
      setIsDisable(false);
      
    }else if(valueStart === 0 && valueEnd === 102 && !isDisable){
      setIsDisable(true);
      changeData(undefined);
    }
  }, [valueStart, valueEnd, isDisable])
  
  useEffect(() => {
    if(valueStart === 0 && valueEnd === 102){
      changeData(undefined);
    }else{
      changeData({
        from: stepToValue(valueStart, name),
        to: stepToValue(valueEnd, name),
      });
    }
  }, [valueStart, valueEnd])
  
  return (
    <RangeStyled isDisable={isDisable}>
      <NameStyled>{name}</NameStyled>
      <WrapperRangeStyled>
        <ValueStyled>
          {stepToValue(valueStart, name)}
        </ValueStyled>
        <MainInputRange
          maxValue={102}
          minValue={0}
          value={valueStart}
          onChange={(value: any) => {
            if(value <= valueEnd){
              setValueStart(value)
            }
          }}
          draggableTrack
        />
        <MainInputRange
          maxValue={102}
          minValue={0}
          value={valueEnd}
          onChange={(value: any) => {
            if(value >= valueStart){
              setValueEnd(value)
            }
          }}
          draggableTrack
        />
        <ValueStyled>
          {stepToValue(valueEnd,name)}
        </ValueStyled>
      </WrapperRangeStyled>
    </RangeStyled>
  )
}