import React, { FC, useContext} from 'react';
import {formNameContext, useIsValidForm, onSubmitContext, formDataParser} from 'react-redux-hook-form';
import {store} from "src/recipeModule/store/store";
import {SubmitButtonStyled} from "./styled";


interface ISubmitButton {
  text: string;
  className?: string;
}
export const onSubmitForm = (formName: string, onSubmit: (props:any) => void) => {
  onSubmit(formDataParser(store.getState().formController.formController?.[formName])[0])
}


export const SubmitButton: FC<ISubmitButton> = (props) => {
  const formName = useContext(formNameContext)
  const isValidForm = useIsValidForm(formName)
  const onSubmit = useContext(onSubmitContext)
  
  const onClick = () => {
    if(isValidForm){
      onSubmitForm(formName, onSubmit)
    }
  }
  return (
    <SubmitButtonStyled className={props.className} isValidForm={isValidForm} onClick={onClick}>
      {props.text}
    </SubmitButtonStyled>
  )
};
