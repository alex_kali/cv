import styled from "styled-components";
import {BORDER_INPUT, BORDER_RADIUS_INPUT, COLOR_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT} from "../styles";

interface ISubmitButtonStyled {
  isValidForm: boolean;
}

export const SubmitButtonStyled = styled.button<ISubmitButtonStyled>`
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 2px solid rgba(0,0,0,0.4);
  border-radius: ${BORDER_RADIUS_INPUT};
  margin-top: 10px;
  font-size: 16px;
  padding-left: 30px;
  padding-right: 30px;
  color: rgba(0,0,0,0.4);
  transition-duration: 0.3s;
  &:hover{
    border-color: rgba(0,0,0,0.8);
    color:  rgba(0,0,0,0.8);
  }
  ${props => `
    opacity: ${props.isValidForm ? OPACITY_FOCUS_INPUT : OPACITY_INPUT};
    &:hover{
      cursor: ${props.isValidForm ? '' : 'not-allowed'};
    }
  `}
`
