import {FC} from "react";
import {
  ParamNameStyled,
  ParamsStyled,
  ParamStyled,
  ParamValueStyled
} from "src/recipeModule/components/ui/params/styled";
import {IParam} from "src/recipeModule/store/detail/model";

interface IParams {
  params: Array<IParam>;
}
export const Params:FC<IParams> = ({params}) => {
  return (
    <ParamsStyled>
      {params.map((item) => (
        <ParamStyled>
          <ParamNameStyled>{item.name}</ParamNameStyled>
          <ParamValueStyled>{item.name === 'kcal' ? item.value : item.value + 'g'}</ParamValueStyled>
        </ParamStyled>
      ))}
    </ParamsStyled>
  )
}