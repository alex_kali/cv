import {H2} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const IconTagsStyled = styled.div`
  display: flex;
  margin-top: 6px;
  margin-bottom: 6px;
`

export const TagStyled = styled.div`
  margin-right: 30px;
  display: flex;
  align-items: center;
  gap: 10px;
`

export const TagIconStyled = styled.div`
  width: 25px;
  height: 25px;
  svg {
    width: 100%;
    height: 100%;
  }
`

export const TagNameStyled = styled(H2)`

`