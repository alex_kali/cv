import {FC} from "react";
import {IconTagsStyled, TagIconStyled, TagNameStyled, TagStyled} from "src/recipeModule/components/ui/iconTags/styled";

interface IIconsTags {
  tags: Array<{name: string | 0, icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>}>;
}

export const IconTags:FC<IIconsTags> = ({tags}) => {
  return (
    <IconTagsStyled>
      {tags.map((item) => item.name && (
        <TagStyled>
          <TagIconStyled>
            <item.icon/>
          </TagIconStyled>
          <TagNameStyled>{item.name}</TagNameStyled>
        </TagStyled>
      ))}
    </IconTagsStyled>
  )
}