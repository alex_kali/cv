import styled from "styled-components";
import ReactPaginate from 'react-paginate';
export const PaginationStyled = styled(ReactPaginate)`
  display: flex;
  justify-content: space-between;
  margin-left: auto;
  margin-right: auto;
  width: 600px;
  margin-top: 20px;
  margin-bottom: 20px;
  li {
    cursor: pointer;
  }
  
  .selected a {
    color: blue;
  }
`