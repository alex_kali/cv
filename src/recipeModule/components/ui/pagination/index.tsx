import React, {FC} from 'react';

import {PaginationStyled} from "src/recipeModule/components/ui/pagination/styled";

interface IPaginatedItems {
  pages: number;
  onChange: (value: {selected: number}) => void,
  activePage: number;
}

export const PaginatedItems:FC<IPaginatedItems> = ({pages, onChange, activePage}) => {
  return (
    <PaginationStyled
      breakLabel="..."
      nextLabel=">"
      onPageChange={onChange}
      pageRangeDisplayed={5}
      pageCount={pages}
      initialPage={activePage}
      previousLabel="<"
    />
  );
}