import {H2} from "src/recipeModule/components/text/styled";
import styled from "styled-components";
import {ReactComponent as check} from "./icons/check.svg";

export const WrapperCheckboxInputStyled = styled.div`
  padding-top: 4px;
  padding-bottom: 4px;
`

export const CheckboxInputStyled = styled.input`
  display: none;
`

interface IWrapperCheckboxItemStyled{
  checked: boolean;
}

export const WrapperCheckboxItemStyled = styled.div<IWrapperCheckboxItemStyled>`
  display: flex;
  justify-content: space-between;
  flex-direction: row-reverse;
  align-items: center;
  width: 400px;
  max-width: 100%;
  clear: both;
  margin-top: 4px;
  margin-bottom: 4px;
  transition-duration: 0.1s;
  cursor: pointer;
  input:checked ~ .checkbox{
    & > *{
      height: 14px;
    }
  }
  ${props => props.checked ? 'opacity: 1;' : 'opacity: 0.4;' }
`

export const CheckboxTextStyled = styled(H2)`
  margin-left: 15px;
  width: calc(100% - 50px);
`

export const CustomRadioStyled = styled.div`
  width: 14px;
  height: 14px;
  float: left;
  border: 2px solid grey;
  margin-left: 15px;
  box-sizing: content-box;
`

export const CustomCheckStyled = styled(check)`
  width: 14px;
  height: 0px;
  transition-duration: 0.1s;
  box-sizing: content-box;
`

export const WrapperTagStyled = styled.div`
  display: inline-block;
  margin-top: 5px;
  margin-bottom: 5px;
`

export const TagStyled = styled.div`
  display: flex;
  margin-right: 10px;
  margin-left: 10px;
  justify-content: space-between;
  align-items: center;
  gap: 6px;
  background: grey;
  padding: 5px 10px;
  border-radius: 8px;
  
  
`

export const TagCancelStyled = styled.div`
  width: 12px;
  height: 12px;
  background: black;
  cursor: pointer;
`