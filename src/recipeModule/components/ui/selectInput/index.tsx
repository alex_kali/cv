import React, {FC} from 'react';
import {
  CheckboxInputStyled,
  CheckboxTextStyled, CustomCheckStyled,
  CustomRadioStyled,
  WrapperCheckboxInputStyled,
  WrapperCheckboxItemStyled
} from "./styled";
import {useField} from "react-redux-hook-form";

interface ICheckboxInput {
  name: string;
  data: Array<string>;
  required?: boolean;
}

export const SelectInput: FC<ICheckboxInput> = (props) => {
  const {useData} = useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData] = useData()
  
  const onChange = (e: any) => {
    if(data){
      if(data.indexOf(e.target.value) === -1){
        changeData([...data,e.target.value])
      }else{
        const newData = (data.filter((i:string) => i !== e.target.value))
        if(newData.length) {
          changeData(newData)
        }else{
          changeData(undefined)
        }
      }
    }else{
      changeData([e.target.value])
    }
  }
  
  
  return (
    <WrapperCheckboxInputStyled>
      {props.data.map((i)=>
        <label htmlFor={i} key={i}>
          <WrapperCheckboxItemStyled checked={data ? data.includes(i) : false}>
            <CheckboxTextStyled>{i}</CheckboxTextStyled>
            <CheckboxInputStyled id={i} type="checkbox" name={props.name} value={i} onChange={onChange}/>
            <CustomRadioStyled className={'checkbox'}><CustomCheckStyled /></CustomRadioStyled>
          </WrapperCheckboxItemStyled>
        </label>
      )}
    </WrapperCheckboxInputStyled>
  )
};