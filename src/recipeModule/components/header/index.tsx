import {useLocation} from "react-router-dom";
import {HeaderStyled, LinkStyled, WrapperLinksStyled} from "src/recipeModule/components/header/styled";
import {useNavigation} from "src/restaurantModule/utils/useNavigation";

export const Header = () => {
  const location = useLocation()
  const navigate = useNavigation()
  return (
    <HeaderStyled>
      <WrapperLinksStyled>
        <LinkStyled onClick={()=>navigate('/recipe/list/1')} isActive={location.pathname.includes('/recipe/list')}>All</LinkStyled>
        <LinkStyled onClick={()=>navigate('/recipe/search')} isActive={location.pathname === '/recipe/search'}>Search</LinkStyled>
      </WrapperLinksStyled>
    </HeaderStyled>
  )
}