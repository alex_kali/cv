import {H1} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const HeaderStyled = styled.header`
  height: 60px;
  width: 100%;
  border-bottom: 2px solid rgba(0,0,0,0.2);
`

export const WrapperLinksStyled = styled.div`
  width: 200px;
  height: 100%;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const LinkStyled = styled(H1)<{isActive: boolean}>`
  color: ${props => props.isActive ? `rgba(0,0,0,0.8)` : `rgba(0,0,0,0.2)`};
  transition-duration: 0.3s;
  cursor: pointer;
  &:hover{
    color: rgba(0,0,0,0.8);
  }
`