import {useEffect} from "react";
import {useSelector} from "react-redux";
import {RecipePreview} from "src/recipeModule/store/list/model";
import {setResults} from "src/recipeModule/store/search/slices";
import {getComplexity, getParams, getResult, getTags} from "src/recipeModule/store/search/thunks";
import {RootState, store} from "src/recipeModule/store/store";


export const Search = {
  useComplexity: () => {
    const data = useSelector((state: RootState) => state.search.complexity) as any
  
    useEffect(() => {
      const isLoading = store.getState().requestController.isLoading.includes(`getComplexity`) as boolean
      if(!data && !isLoading){
        store.dispatch(getComplexity())
      }
    }, [data])
    
    return data
  },
  
  useTags: () => {
    const data = useSelector((state: RootState) => state.search.tags) as any
  
    useEffect(() => {
      const isLoading = store.getState().requestController.isLoading.includes(`getTags`) as boolean
      if(!data && !isLoading){
        store.dispatch(getTags())
      }
    }, [data])
  
    return data
  },
  
  useParams: () => {
    const data = useSelector((state: RootState) => state.search.params) as any
    
    useEffect(() => {
      const isLoading = store.getState().requestController.isLoading.includes(`getParams`) as boolean
      if(!data && !isLoading){
        store.dispatch(getParams())
      }
    }, [data])
    
    return data
  },
  
  getResult: (data: ISearch) => store.dispatch(getResult(data)),
  
  useResult: () => {
    
    useEffect(() => {
      
      return () => {
        store.dispatch(setResults(null))
      }
    }, [])
    
    return useSelector((state: RootState) => state.search.results) as null | Array<RecipePreview>
  }
  
}

export interface ISearch {
  search?: string;
  complexity?: Array<string>;
  ingredients?: Array<string>;
  tags?: Array<string>;
  carbs?: {from: number, to: number};
  fat?: {from: number, to: number};
  fibre?: {from: number, to: number};
  kcal?: {from: number, to: number};
  protein?: {from: number, to: number};
  salt?: {from: number, to: number};
  saturates?: {from: number, to: number};
  sugar?: {from: number, to: number};
}