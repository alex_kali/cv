import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  complexity: null,
  tags: null,
  params: null,
  results: null,
};

export const searchSlice = createSlice({
  name: 'search',
  initialState,
  reducers: {
    setComplexity: (state, action) => {
      state.complexity = action.payload
    },
    setTags: (state, action) => {
      state.tags = action.payload
    },
    setParams: (state, action) => {
      state.params = action.payload
    },
    setResults: (state, action) => {
      state.results = action.payload
    },
  },
});

export const { setComplexity, setTags, setParams, setResults } = searchSlice.actions

export default searchSlice.reducer;