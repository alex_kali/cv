import {requestController} from "react-redux-request-controller";
import {ISearch} from "src/recipeModule/store/search/model";
import {setComplexity, setParams, setResults, setTags} from "src/recipeModule/store/search/slices";
import {AppThunk} from "src/recipeModule/store/store";


export const getComplexity = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getComplexity`,
    url: `/food/complexity/`,
    method: 'GET',
    action: setComplexity
  }))
};

export const getTags = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getTags`,
    url: `/food/tags/`,
    method: 'GET',
    action: setTags
  }))
};

export const getParams = (): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getParams`,
    url: `/food/params/`,
    method: 'GET',
    action: setParams
  }))
};

export const getResult = (data: ISearch): AppThunk => async (
  dispatch: any
) => {
  const newData:any = {}
  for (let i in data) {
    if(data[i as keyof typeof data]){
      newData[i] = data[i as keyof typeof data]
    }
  }
  

  dispatch(requestController({
    id: `getParams`,
    url: `/food/search/`,
    method: 'POST',
    params: data,
    action: setResults
  }))
};