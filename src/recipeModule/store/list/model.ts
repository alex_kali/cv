import {useEffect} from "react";
import {useSelector} from "react-redux";
import {clearRecipeList} from "src/recipeModule/store/list/slices";
import {getListRecipe} from "src/recipeModule/store/list/thunks";
import {RootState, store} from "src/recipeModule/store/store";

export const RecipeList = {
  page: null as null | number,
  
  useData: function(page: number) {
    this.page = page
    const data = useSelector((state: RootState) => state.recipeList.recipeList) as null | Array<RecipePreview>
  
    useEffect(() => {
      if(this.page !== null) {
        store.dispatch(clearRecipeList())
      }
    }, [page])
    
    useEffect(() => {
      if(!data && !this.getIsLoading()){
        store.dispatch(getListRecipe(page))
      }
    }, [data])
    return data
  },
  
  useCountPages: () => useSelector((state: RootState) => state.recipeList.pages) as null | number,
  
  getIsLoading: function (){
    return store.getState().requestController.isLoading.includes(`getListRecipe${this.page}`) as boolean
  }
}

export interface RecipePreview {
  id: string | number;
  img: string;
  name: string;
  description: string;
}

