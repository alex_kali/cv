import {requestController} from "react-redux-request-controller";
import {setRecipeList} from "src/recipeModule/store/list/slices";
import {AppThunk} from "src/recipeModule/store/store";


export const getListRecipe = (page: number): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getListRecipe${page}`,
    url: `/food/offset/${page * 30 - 30}/`,
    method: 'GET',
    action: setRecipeList
  }))
};