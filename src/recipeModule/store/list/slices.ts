import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  recipeList: null,
  pages: null,
};

export const recipeListSlice = createSlice({
  name: 'recipeList',
  initialState,
  reducers: {
    setRecipeList: (state, action) => {
      state.recipeList = action.payload.list
      state.pages = action.payload.pages
    },
    clearRecipeList: (state) => {
      state.recipeList = null
    },
  },
});

export const { setRecipeList, clearRecipeList } = recipeListSlice.actions

export default recipeListSlice.reducer;