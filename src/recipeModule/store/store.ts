import {configureStore, ThunkAction, Action, combineReducers} from '@reduxjs/toolkit';
import recipeDetailReducer from './detail/slices';
import recipeListReducer from './list/slices';
import searchReducer from './search/slices';
import { formControllerReducer } from 'react-redux-hook-form';
import {requestControllerReducer} from "react-redux-request-controller";

const combinedReducer = combineReducers({
  formController: formControllerReducer, // подключение formControllerReducer
  requestController: requestControllerReducer, // подключение requestControllerReducer
  recipeDetail: recipeDetailReducer,
  recipeList: recipeListReducer,
  search: searchReducer,
});

const rootReducer = (state:any, action:any) => {
  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

window.dispatch = store.dispatch

window.getState = store.getState()

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;