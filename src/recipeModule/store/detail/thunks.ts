import {requestController} from "react-redux-request-controller";
import {setRecipeDetail} from "src/recipeModule/store/detail/slices";
import {AppThunk} from "src/recipeModule/store/store";


export const getRecipeDetail = (id: number | string): AppThunk => async (
  dispatch: any
) => {
  dispatch(requestController({
    id: `getRecipeDetail${id}`,
    url: `/food/${id}/`,
    method: 'GET',
    action: setRecipeDetail
  }))
};