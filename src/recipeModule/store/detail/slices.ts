import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  recipeDetail: null
};

export const recipeDetailSlice = createSlice({
  name: 'recipeDetail',
  initialState,
  reducers: {
    setRecipeDetail: (state, action) => {
      state.recipeDetail = action.payload
    },
  },
});

export const { setRecipeDetail } = recipeDetailSlice.actions

export default recipeDetailSlice.reducer;