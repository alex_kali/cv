import {useEffect} from "react";
import {useSelector} from "react-redux";
import {setRecipeDetail} from "src/recipeModule/store/detail/slices";
import {getRecipeDetail} from "src/recipeModule/store/detail/thunks";
import {RootState, store} from "src/recipeModule/store/store";

export const RecipeDetail = {
  id: null as null | number | string,
  
  useData: function(id: number | string) {
    this.id = id
    const data = useSelector((state: RootState) => state.recipeDetail.recipeDetail) as IRecipe | null
  
    useEffect(() => {
      if(data !== null){
        store.dispatch(setRecipeDetail(null))
      }
    }, [id])
    
    useEffect(() => {
      if(!data && !this.getIsLoading()){
        store.dispatch(getRecipeDetail(id))
      }
    }, [data])

    return data
  },
  
  getIsLoading: function (){
    return store.getState().requestController.isLoading.includes(`getRecipeDetail${this.id}`) as boolean
  }
}

export interface IRecipe {
  id: string | number;
  name: string;
  description: string;
  complexity: string;
  cook: number;
  prep: number;
  img: string;
  steps: Array<string>;
  tags: Array<string>;
  ingredients: Array<IIngredient>;
  params: Array<IParam>;
}

export interface IIngredient {
  name: string;
  value: string;
}

export interface IParam {
  name: string;
  value: string;
}