import React from "react";
import {Provider} from "react-redux";
import {Route, Switch, useRouteMatch} from "react-router-dom";
import {Footer} from "src/recipeModule/components/footer";
import {Header} from "src/recipeModule/components/header";
import {DetailPage} from "src/recipeModule/pages/detail";
import {ListPage} from "src/recipeModule/pages/list";
import {SearchPage} from "src/recipeModule/pages/search";
import {store} from "src/recipeModule/store/store";
import {RecipeAppStyled} from "src/recipeModule/styled";

export function RecipeApp() {
  let { path } = useRouteMatch();
  // @ts-ignore
  import('./style.css');
  
  return (
    <RecipeAppStyled>
      <Provider store={store}>
        <Header/>
        <Switch>
          <Route path={`${path}/recipe/:id`}>
            <DetailPage/>
          </Route>
          <Route path={`${path}/search`}>
            <SearchPage/>
          </Route>
          <Route path={`${path}/list/:page`}>
            <ListPage/>
          </Route>
        </Switch>
        <Footer/>
      </Provider>
    </RecipeAppStyled>
  );
}