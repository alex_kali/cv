import {useParams} from "react-router-dom";
import {PaginatedItems} from "src/recipeModule/components/ui/pagination";
import {RecipePreview} from "src/recipeModule/components/ui/recipe";
import {DetailPageStyled} from "src/recipeModule/pages/detail/styled";
import {ListPageStyled} from "src/recipeModule/pages/list/styled";
import {RecipeList} from "src/recipeModule/store/list/model";
import {useNavigation} from "src/recipeModule/utils/useNavigation";

export const ListPage = () => {
  const { page }:any = useParams()
  const list = RecipeList.useData(page)
  const pages = RecipeList.useCountPages()
  const navigation = useNavigation()
  
  if(!list || !pages){
    return null
  }

  return (
    <DetailPageStyled>
      <ListPageStyled>
        {list.map((item) => <RecipePreview {...item}/>)}
      </ListPageStyled>
      <PaginatedItems
        pages={pages}
        activePage={page - 1}
        onChange={(item) => {navigation(`/recipe/list/${item.selected + 1}`)}}
      />
    </DetailPageStyled>
  )
}