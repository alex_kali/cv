import styled from "styled-components";

export const ListPageStyled = styled.div`
  display: flex;
  flex-wrap: wrap;
  align-content: flex-start;
  justify-content: space-between;
  gap: 10px;
  margin-top: 30px;
  margin-bottom: 10px;
`