import styled from "styled-components";

export const SearchPageStyled = styled.div`

`

export const ColumnStyled = styled.div`
  display: flex;
  gap: 30px;
`

export const LeftStyled = styled.div`
  width: calc(100% - 200px);
`

export const RightStyled = styled.div`
  width: 200px;
`