import {useForm, WrapperForm} from "react-redux-hook-form";
import {Title} from "src/recipeModule/components/text/styled";
import {SubmitButton} from "src/recipeModule/components/ui/button";
import {RangesInput} from "src/recipeModule/components/ui/rangesInput";
import {RecipePreview} from "src/recipeModule/components/ui/recipe";
import {SelectInput} from "src/recipeModule/components/ui/selectInput";
import {TagsInput} from "src/recipeModule/components/ui/tagsInput";
import {TextInput} from "src/recipeModule/components/ui/textInput";
import {DetailPageStyled} from "src/recipeModule/pages/detail/styled";
import {ListPageStyled} from "src/recipeModule/pages/list/styled";
import {ColumnStyled, LeftStyled, RightStyled, SearchPageStyled} from "src/recipeModule/pages/search/styled";
import {ISearch, Search} from "src/recipeModule/store/search/model";

export const SearchPage = () => {
  const form = useForm({name: 'search'})
  
  const complexity = Search.useComplexity()
  const tags = Search.useTags()
  const params = Search.useParams()
  const result = Search.useResult()
  
  if(!complexity || !tags || !params){
    return null
  }
  
  return (
    <WrapperForm form={form} onSubmit={(data:ISearch) => Search.getResult(data)}>
      <DetailPageStyled>
        
        <ColumnStyled>
          <LeftStyled>
            
            <Title>Search</Title>
            <TextInput name="search"/>
  
            <Title>Ingredients</Title>
            <TagsInput
              name="ingredients"
              url="/food/ingredients/"
            />
  
            <Title>Params</Title>
            <RangesInput
              name="params"
              data={params}
            />
            
            <SubmitButton text="Search"/>
            
          </LeftStyled>
          
          <RightStyled>
            <Title>Complexity</Title>
            <SelectInput
              name="complexity"
              data={complexity}
            />
  
            <Title>Tags</Title>
            <SelectInput
              name="tags"
              data={tags}
            />
          </RightStyled>
        </ColumnStyled>
        {result && (
          <ListPageStyled>
            {result.map((item) => <RecipePreview {...item}/>)}
          </ListPageStyled>
        )}
      </DetailPageStyled>
    </WrapperForm>
  )
}