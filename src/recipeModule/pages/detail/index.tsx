import {useParams} from "react-router-dom";
import {H2} from "src/recipeModule/components/text/styled";
import {IconTags} from "src/recipeModule/components/ui/iconTags";
import {Ingredients} from "src/recipeModule/components/ui/ingredients";
import {Params} from "src/recipeModule/components/ui/params";
import {Steps} from "src/recipeModule/components/ui/steps";
import {Tags} from "src/recipeModule/components/ui/tags";
import {
  CookDescriptionsStyled,
  DetailPageStyled,
  ImageStyled,
  LeftStyled,
  NameStyled,
  RightStyled, RowStyled, TitleStyled, WrapperIngredientsStyled, WrapperStepsStyled
} from "src/recipeModule/pages/detail/styled";
import {RecipeDetail} from "src/recipeModule/store/detail/model";

import {ReactComponent as prep} from "./icons/prep.svg";
import {ReactComponent as food} from "./icons/food.svg";

export const DetailPage = () => {
  const { id }:any = useParams()
  const data = RecipeDetail.useData(id)
  
  if(!data) {
    return null
  }

  return (
    <DetailPageStyled>
      <RowStyled>
        <LeftStyled>
          <ImageStyled image={data.img}></ImageStyled>
        </LeftStyled>
        <RightStyled>
          
          <NameStyled>
            {data.name}
          </NameStyled>
          
          <IconTags tags={[
            {name: data.prep && 'Prep: ' + data.prep + ' min', icon: prep},
            {name: data.cook && 'Cook: ' + data.cook + ' min', icon: prep},
            {name: data.complexity, icon: food},
          ]}/>
          
          <CookDescriptionsStyled>
            {data.description}
          </CookDescriptionsStyled>
          <Tags tags={data.tags}/>
          <Params params={data.params}/>
        </RightStyled>
      </RowStyled>
      <RowStyled>
        <WrapperIngredientsStyled>
          <TitleStyled>Ingredients</TitleStyled>
          <Ingredients ingredients={data.ingredients}/>
        </WrapperIngredientsStyled>
        <WrapperStepsStyled>
          <TitleStyled>Method</TitleStyled>
          <Steps steps={data.steps}/>
        </WrapperStepsStyled>
      </RowStyled>
    </DetailPageStyled>
  )
}