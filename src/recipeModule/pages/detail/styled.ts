import {H1, H2} from "src/recipeModule/components/text/styled";
import styled from "styled-components";

export const DetailPageStyled = styled.div`
  width: 100%;
  max-width: 1000px;
  margin-left: auto;
  margin-right: auto;
  min-height: 400px;
  padding-top: 20px;
  padding-bottom: 20px;
`

export const RowStyled = styled.div`
  display: flex;
  gap: 40px;
`

export const LeftStyled = styled.div`
  width: 270px;
  height: 270px;
`

export const ImageStyled = styled.div<{image: string}>`
  width: 100%;
  height: 100%;
  background-image: url(${props => props.image});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
`

export const RightStyled = styled.div`
  width: calc(100% - 40px - 270px);
`

export const NameStyled = styled(H1)`
  text-align: center;
`

export const TitleStyled = styled(H1)`
  margin-top: 6px;
  margin-bottom: 4px;
`

export const CookDescriptionsStyled = styled(H2)`
  margin-bottom: 6px;
  margin-top: 6px;
`

export const WrapperIngredientsStyled = styled.div`
  margin-top: 15px;
  width: 300px;
`

export const WrapperStepsStyled = styled.div`
  margin-top: 15px;
  width: calc(100% - 40px - 350px);
`