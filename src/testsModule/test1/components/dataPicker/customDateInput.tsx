import {FC, useEffect, useState } from "react";
import {
  CustomDateInputStyled, DateIconStyled,
  DateInput,
  DateTitleInputStyled
} from "src/testsModule/test1/components/dataPicker/styled";
import {Icons} from "src/testsModule/test1/constants/icons";
import {formatter} from "src/testsModule/test1/utils/formatter";

interface ICustomDateInput {
  onChange: (data: string) => void,
  toDefault: () => void,
}

export const CustomDateInput:FC<ICustomDateInput> = ({onChange, toDefault}) => {
  const [data, setData] = useState('')
  
  useEffect(() => {
    if(`${formatter(data).string().onlyNumber().data}`.length === 12){
      onChange(data)
    }else{
      toDefault()
    }
  }, [data])
  
  return (
    <CustomDateInputStyled>
      <DateTitleInputStyled>Указать даты</DateTitleInputStyled>
      <DateInput
        mask={"99.99.99-99.99.99"}
        alwaysShowMask
        onChange={(e:any)=> {
          if(e.target.value){
            setData(e.target.value)
          }
        }}
      />
      <DateIconStyled><Icons.calendar/></DateIconStyled>
    </CustomDateInputStyled>
  )
}