import { FC, useRef, RefObject } from "react";
import { useField } from "react-redux-hook-form";
import {CustomDateInput} from "src/testsModule/test1/components/dataPicker/customDateInput";
import {
  CalendarIconStyled,
  ContentStyled,
  DataPickerStyled,
  IconStyled, TitleStyled
} from "src/testsModule/test1/components/dataPicker/styled";
import {DropDownPopup} from "src/testsModule/test1/components/dropDownPopup";
import {Icons} from "src/testsModule/test1/constants/icons";
import {useOutsideClick} from "src/testsModule/test1/utils/hooks/useOutsideHook";
import {usePopup} from "src/testsModule/test1/utils/hooks/usePopup";
import {ReactComponent as Arrow} from "./icons/arrow.svg";

interface IDataPicker {
  list: Array<{title: string, value: string}>,
  value: string,
  name: string,
}

export const DataPicker:FC<IDataPicker> = ({list, name, value}) => {
  const ref = useRef<any>()
  const {isView, open, close} = usePopup()
  useOutsideClick([ref], close, isView)
  
  const {useData} = useField({
    name: name,
    initialValue: list.find((i) => i.value === value)?.value,
  })
  
  const [data, setData] = useData()
  console.log(data)
  const toNext = () => {
    const index = list.findIndex((item) => item.value === data)
    if(index !== -1) {
      index === list.length - 1 ? setData(list[0].value) : setData(list[index + 1].value)
    }else{
      setData(list[0].value)
    }
  }
  
  const toPrev = () => {
    const index = list.findIndex((item) => item.value === data)
    if(index !== -1) {
      index === 0 ? setData(list[list.length - 1].value) : setData(list[index - 1].value)
    }else{
      setData(list[0].value)
    }
  }
  
  const toDefault = () => {
    if(list.findIndex((item) => item.value === data) === -1){
      setData(list.find((i) => i.value === value)?.value)
    }
  }
  
  return (
    <DataPickerStyled ref={ref}>
      <IconStyled onClick={toPrev}><Arrow/></IconStyled>
      
      <ContentStyled onClick={open}>
        <CalendarIconStyled><Icons.calendar/></CalendarIconStyled>
        <TitleStyled>{list.find((i) => i.value === data)?.title || data}</TitleStyled>
      </ContentStyled>
      
      <IconStyled isRotate onClick={toNext}><Arrow/></IconStyled>
  
      {isView &&
        <DropDownPopup
          list={list}
          value={data}
          onChange={(data)=>{setData(data.value)}}
          after={<CustomDateInput toDefault={toDefault} onChange={(value) => setData(value) }/>}
        />
      }
    </DataPickerStyled>
  )
}