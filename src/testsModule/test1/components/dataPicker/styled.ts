import styled from "styled-components";
import InputMask from 'react-input-mask';

export const DataPickerStyled = styled.div`
  display: flex;
  gap: 12px;
  height: 24px;
  align-items: center;
  cursor: pointer;
  position: relative;
`

export const IconStyled = styled.div<{isRotate?: boolean}>`
  ${({isRotate}) => isRotate ? 'transform: rotate(180deg);' : ''}
  width: 16px;
  height: 24px;
  svg{
    width: 100%;
    height: 100%;
  }
  &:hover {
    path {
      fill: #002CFB;
    }
  }
`

export const ContentStyled = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
`

export const CalendarIconStyled = styled.div`
  width: 16px;
  height: 18px;
  
  svg{
    width: 100%;
    height: 100%;
  }
`

export const TitleStyled = styled.div`
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  color: #005FF8;
`

export const CustomDateInputStyled = styled.div`
  margin-left: 20px;
  margin-bottom: 20px;
`

export const DateTitleInputStyled = styled.div`
  font-weight: 400;
  font-size: 14px;
  line-height: 28px;
`

export const DateInput = styled(InputMask)`
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 28px;
  letter-spacing: 0.04em;

  color: #ADBFDF;
`

export const DateIconStyled = styled.div`
  position: absolute;
  right: 21px;
  bottom: 26px;
`