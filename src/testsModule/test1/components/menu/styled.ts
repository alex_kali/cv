import {Theme} from "src/testsModule/test1/constants/theme";
import {Button} from "src/testsModule/test1/ui/button";
import styled from "styled-components";

export const MenuStyled = styled.div`
  width: 240px;
  background: ${Theme.colors.darkBlue};
  padding-top: 20px;
  padding-bottom: 20px;
  flex: 0 0 auto;
`

export const WrapperLogo = styled.div`
  margin-left: 12px;
  height: 28px;
  width: 109px;
`

export const ItemListMenuStyled = styled.div`
  margin-top: 32px;
  margin-bottom: 64px;
`

export const MenuItemStyled = styled.div<{isActive: boolean}>`
  display: flex;
  gap: 12px;
  padding-left: 9px;
  padding-right: 12px;
  height: 52px;
  align-items: center;
  cursor: pointer;
  transition-duration: 0.6s;
  border-left: ${({isActive}) => isActive ? '3px solid #002CFB;background: rgba(216, 228, 251, 0.32);' : '3px solid rgba(0,0,0,0);'};
`

export const MenuItemIconStyled = styled.div<{isActive: boolean}>`
  width: 24px;
  height: 24px;
  
  
  svg {
    width: 100%;
    height: 100%;
  }
  * {
    transition-duration: 0.6s;
    fill: ${({isActive}) => isActive ? 'white;' : 'rgba(255, 255, 255, 0.6);'};
  }
`

export const MenuTitleItemStyled = styled.div<{isActive: boolean}>`
  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  transition-duration: 0.6s;
  color: ${({isActive}) => isActive ? 'white;' : 'rgba(255, 255, 255, 0.6);'};
`

export const DecorStyled = styled.div<{isActive: boolean}>`
  background: #FFD500;
  box-shadow: 0px 3px 8px rgba(237, 218, 1, 0.5);
  border-radius: 50%;
  margin-left: auto;
  transition-duration: 0.6s;
  ${({isActive}) => isActive ? 'opacity: 1;height: 8px;width: 8px;' : 'opacity: 0;height: 0px;width: 0px;'};
`

export const WrapperButtonStyled = styled.div`
  display: flex;
  flex-direction: column;
  gap: 32px;
  padding-left: 20px;
  padding-right: 20px;
`

export const CustomButton = styled(Button)`
  padding-left: 46px;
`