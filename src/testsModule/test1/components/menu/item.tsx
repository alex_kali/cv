import {
  DecorStyled,
  MenuItemIconStyled,
  MenuItemStyled,
  MenuTitleItemStyled
} from "src/testsModule/test1/components/menu/styled";
import React, {FC} from "react";
import { useHistory } from "react-router-dom";

export interface IMenuItem {
  href: string,
  Icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>,
  title: string,
}
export const MenuItem:FC<IMenuItem> = ({href, Icon, title}) => {
  const history = useHistory()
  const isActive= history.location.pathname === '/tests/test1/' + href

  return (
    <MenuItemStyled isActive={isActive} onClick={() => history.push('/tests/test1/' + href)}>
      <MenuItemIconStyled isActive={isActive}><Icon/></MenuItemIconStyled>
      <MenuTitleItemStyled isActive={isActive}>{title}</MenuTitleItemStyled>
      <DecorStyled isActive={isActive}/>
    </MenuItemStyled>
  )
}