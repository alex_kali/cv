import {useHistory, useRouteMatch } from "react-router-dom";
import {MenuItem} from "src/testsModule/test1/components/menu/item";
import {ListItemMenu} from "src/testsModule/test1/components/menu/listItemMenu";
import {
  CustomButton,
  ItemListMenuStyled,
  MenuStyled, WrapperButtonStyled,
  WrapperLogo
} from "src/testsModule/test1/components/menu/styled";
import {Icons} from "src/testsModule/test1/constants/icons";
import {Button} from "src/testsModule/test1/ui/button";


export const Menu = () => {
  let history = useHistory();

  return (
    <MenuStyled>
      <WrapperLogo><Icons.logo/></WrapperLogo>
      <ItemListMenuStyled>
        {ListItemMenu.map((item) => <MenuItem {...item} key={item.href}/>)}
      </ItemListMenuStyled>
      <WrapperButtonStyled>
        <Button title={'Добавить заказ'} type={'primary'} Icon={Icons.plus}/>
        <CustomButton title={'Оплата'} type={'primary'} Icon={Icons.exclamation}/>
      </WrapperButtonStyled>
    </MenuStyled>
  )
}