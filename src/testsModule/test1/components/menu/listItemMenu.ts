import {IMenuItem} from "src/testsModule/test1/components/menu/item";
import {Icons} from "src/testsModule/test1/constants/icons";

export const ListItemMenu:Array<IMenuItem> = [
  {
    title: 'Итоги',
    href: '1',
    Icon: Icons.results,
  },
  {
    title: 'Заказы',
    href: '2',
    Icon: Icons.orders,
  },
  {
    title: 'Сообщения',
    href: '3',
    Icon: Icons.messages,
  },
  {
    title: 'Звонки',
    href: 'calls',
    Icon: Icons.calls,
  },
  {
    title: 'Контрагенты',
    href: '5',
    Icon: Icons.people,
  },
  {
    title: 'Документы',
    href: '6',
    Icon: Icons.documents,
  },
  {
    title: 'Исполнители',
    href: '7',
    Icon: Icons.perm,
  },
  {
    title: 'Отчеты',
    href: '8',
    Icon: Icons.briefcase,
  },
  {
    title: 'База знаний',
    href: '9',
    Icon: Icons.library,
  },
  {
    title: 'Настройки',
    href: '10',
    Icon: Icons.settings,
  },
]