import { useForm, WrapperForm } from "react-redux-hook-form";
import {
  CustomAvatar, CustomDropDown,
  DateStyled,
  HeaderStyled,
  SearchIconStyled,
  WrapperStatusStyled
} from "src/testsModule/test1/components/header/styled";
import {Icons} from "src/testsModule/test1/constants/icons";
import {ProgressBar} from "src/testsModule/test1/ui/progressBar";

export const Header = () => {
  const form = useForm({name: 'filter'})
  
  return (
    <HeaderStyled>
      <DateStyled>
        Среда, 13 окт
      </DateStyled>
      <WrapperStatusStyled>
        <ProgressBar status={20 / 30} title={{text: 'Новые звонки ', status: '20 из 30 шт'}} color={'green'}/>
        <ProgressBar status={40 / 100} title={{text: 'Качество разговоров ', status: '40%'}} color={'yellow'}/>
        <ProgressBar status={67 / 100} title={{text: 'Конверсия в заказ ', status: '67%'}} color={'red'}/>
      </WrapperStatusStyled>
      <SearchIconStyled>
        <Icons.search/>
      </SearchIconStyled>
      <WrapperForm form={form}>
        <CustomDropDown name={'company'} value={'1'} list={[{value: '1', title: 'ИП Сидорова Александра Михайловна'}]} size={'big'}/>
      </WrapperForm>
      <CustomAvatar url={'https://s3-alpha-sig.figma.com/img/8d73/9a56/510d8848d81d795dff331c2fc29811e6?Expires=1687132800&Signature=iHkTDEmoJG87LQ~xBALVFj5ovvX4zM6o0MnJyCTq2ENQpWw4yUOz1YlxVicddifT4t7FJplG9LlQc1Fo5Ep8OW8~857DYNXyw-DEoOsAXmf7qL2b0B8OMcHaV0xv0w-TwaeITjcnJTe~xTk9LMJPTJxGXKvKwbKAsgj7ySaLpE0Y6TVVNuZa0qicBJQRx698ClfC-BfBg-B-Ga0S1h~ncCnljjs69JCiUb~coCBgiGOEAQ3Er30DXBbEyAQcnmNUzgt0VvyA1y7gobGa7nBPq7TqIQKzmDZ-BjdzrSSLPsd9H0MBKFJ4fQvjYw8lbAU7I63xxoz4Xr98nRXA3hMx3g__&Key-Pair-Id=APKAQ4GOSFWCVNEHN3O4'}/>
    </HeaderStyled>
  )
}