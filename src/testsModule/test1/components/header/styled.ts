import {DropDown} from "src/testsModule/test1/components/inputs/dropDown";
import {Avatar} from "src/testsModule/test1/ui/avatar";
import styled from "styled-components";

export const HeaderStyled = styled.div`
  width: 100%;
  height: 64px;
  background: white;
  box-shadow: 0px 4px 5px #E9EDF3;
  padding-left: 120px;
  padding-right: 114px;
  display: flex;
  align-items: center;
  flex-direction: row;
`

export const DateStyled = styled.div`
  font-weight: 400;
  font-size: 15px;
  line-height: 22.2px;

  color: #899CB1;
`

export const WrapperStatusStyled = styled.div`
  display: flex;
  gap: 56px;
  margin-left: 86px;
`

export const SearchIconStyled = styled.div`
  margin-left: auto;
  margin-right: 64px;
  width: 16px;
  height: 16px;
  svg{
    width: 100%;
    height: 100%;
  }
`

export const CustomAvatar = styled(Avatar)`

`

export const CustomDropDown = styled(DropDown)`
  margin-top: 2px;
  margin-right: 40px;
`