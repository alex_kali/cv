import styled from "styled-components";

export const DropDownStyled = styled.div<{size: 'big' | 'small'}>`
  display: flex;
  gap: 4px;
  height: ${({size}) => size === 'big' ? '24px;' : '21px;'};
  cursor: pointer;
  position: relative;
`

export const TitleStyled = styled.div<{size: 'big' | 'small'}>`
  font-weight: 400;
  letter-spacing: 0em;
  line-height: 21px;
  color: rgba(137, 156, 177, 1);
  white-space: nowrap;
  font-size: ${({size}) => size === 'big' ? '15px;' : '14px;'}
  
`

export const ArrowStyled = styled.div<{size: 'big' | 'small'}>`
  ${({size}) => size === 'big' ? 'width: 24px;height: 24px;' : 'width: 18px;height: 21px;'}
  svg{
    width: 100%;
    height: 100%;
  }
`