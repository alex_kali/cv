import {DropDownPopup} from "src/testsModule/test1/components/dropDownPopup";
import {ArrowStyled, DropDownStyled, TitleStyled} from "src/testsModule/test1/components/inputs/dropDown/styled";
import React, {FC, useRef} from "react";
import {Icons} from "src/testsModule/test1/constants/icons";
import {useOutsideClick} from "src/testsModule/test1/utils/hooks/useOutsideHook";
import {usePopup} from "src/testsModule/test1/utils/hooks/usePopup";
import { useField } from "react-redux-hook-form";

interface IDropDown {
  list: Array<{title: string, value: string}>,
  value: string,
  name: string,
  className?: string;
  size: 'big' | 'small';
}

export const DropDown:FC<IDropDown> = ({value,className, size, list, name}) => {
  const ref = useRef<any>()
  const {isView, open, close} = usePopup()
  useOutsideClick([ref], close, isView)
  
  const {useData} = useField({
    name: name,
    initialValue: list.find((i) => i.value === value)?.value,
  })
  
  const [data, setData] = useData()
  
  return (
    <DropDownStyled className={className} onClick={open} size={size} ref={ref}>
      <TitleStyled size={size}>{list.find((i) => i.value === data)?.title}</TitleStyled>
      <ArrowStyled size={size}><Icons.arrow/></ArrowStyled>
      
      {isView &&
        <DropDownPopup
          list={list}
          value={data}
          onChange={(value)=>{setData(value.value)}}
        />
      }
    </DropDownStyled>
  )
}