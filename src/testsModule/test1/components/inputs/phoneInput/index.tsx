import {
  PhoneClearIconStyled,
  PhoneInputStyled, PhoneSearchIconStyled,
  PhoneStyled
} from "src/testsModule/test1/components/inputs/phoneInput/styled";
import React, {FC, useState} from 'react';
import {useField} from "react-redux-hook-form";
import {Icons} from "src/testsModule/test1/constants/icons";
import {formatter} from "src/testsModule/test1/utils/formatter";

interface IPhone {
  name: string;
  className?: string;
}

export const PhoneInput:FC<IPhone> = ({name, className}) => {
  const {useData} = useField({
    name: name,
    initialValue: '7',
  })
  
  const [data, changeData] = useData()
  const [isFocus, setIsFocus] = useState(false)
  const isEmpty = data == '7' || data === ''
  
  return (
    <PhoneStyled isEmpty={isEmpty} className={className}>
      <PhoneSearchIconStyled isFocus={isFocus}><Icons.search/></PhoneSearchIconStyled>
      <PhoneInputStyled
        onFocus={() => {setIsFocus(true)}}
        onBlur={() => {setIsFocus(false)}}
        value={isEmpty ? '' : data}
        placeholder={'Поиск по звонкам'}
        mask={isEmpty ? '' : "+7 (999) 999-99-99"}
        maskChar=""
        alwaysShowMask={false}
        onChange={(e:any)=> {
          if(e.target.value){
            changeData(formatter(e.target.value).string().onlyNumber().data )
          }
        }}
      />
      <PhoneClearIconStyled isEmpty={isEmpty} onClick={() => changeData('7')}><Icons.close/></PhoneClearIconStyled>
    </PhoneStyled>
  )
}