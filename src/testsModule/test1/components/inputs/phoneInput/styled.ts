import styled from "styled-components";
import InputMask from 'react-input-mask';

export const PhoneStyled = styled.div<{isEmpty: boolean}>`
  background: #FFFFFF;

  border: 1px solid #002CFB;
  border-radius: 48px;
  padding: 0px 18px;
  margin-top: 4px;
  margin-bottom: 4px;
  height: 48px;
  display: flex;
  gap: 12px;
  align-items: center;
  width: fit-content;
  ${({isEmpty}) => isEmpty ? 'border-color: rgba(0,0,0,0); background: none;padding-left: 0;' : ''}
`

export const PhoneSearchIconStyled = styled.div<{isFocus: boolean}>`
  width: 16px;
  height: 16px;

  path{
    transition-duration: 0.4s;
    ${({isFocus}) => isFocus ? 'fill: #002CFB;' : ''}
  }

  svg{
    width: 100%;
    height: 100%;
  }
`

export const PhoneClearIconStyled = styled.div<{isEmpty: boolean}>`
  width: 24px;
  height: 24px;
  margin-left: 100px;
  cursor: pointer;
  path{
    transition-duration: 0.4s;
  }
  &:hover {
    path {
      fill: #002CFB;;
    }
  }

  svg{
    width: 100%;
    height: 100%;
  }

  ${({isEmpty}) => isEmpty ? 'display: none;' : ''}
`

export const PhoneInputStyled = styled(InputMask)`
  font-weight: 400;
  font-size: 14px;
  line-height: 140%;

  &::placeholder {
    color: #5E7793;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
  }
`
