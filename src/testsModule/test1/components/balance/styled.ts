import styled from "styled-components";

export const BalanceStyled = styled.div`
  height: 40px;
  display: flex;
  gap: 10px;
  padding-left: 12px;
  padding-right: 12px;
  align-items: center;
  background: #FFFFFF;
  border-radius: 48px;
`

export const TitleStyled = styled.div`
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: 0em;
  color: #899CB1;

`

export const MoneyStyled = styled.div`
  display: inline;
  color: black;

`

export const IconStyled = styled.div`
  width: 24px;
  height: 24px;
  path {
    fill: #005FF8;
  }
  svg {
    width: 100%;
    height: 100%;
  }
`