import {BalanceStyled, IconStyled, MoneyStyled, TitleStyled} from "src/testsModule/test1/components/balance/styled";
import {Icons} from "src/testsModule/test1/constants/icons";

export const Balance = () => {
  return (
    <BalanceStyled>
      <TitleStyled>Баланс:<MoneyStyled> 272 ₽ </MoneyStyled></TitleStyled>
      <IconStyled><Icons.plus/></IconStyled>
    </BalanceStyled>
  )
}