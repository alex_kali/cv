import styled from "styled-components";

export const DropDownMenuStyled = styled.div`
  width: 204px;
  background: #FFFFFF;
  border: 1px solid #EAF0FA;
  box-shadow: 0px 0px 26px rgba(233, 237, 243, 0.8);
  
  border-radius: 4px;
  position: absolute;
  z-index: 2;
  top: calc(100% + 8px);
  right: 0;
`

export const ItemStyled = styled.div<{isActive: boolean}>`
  height: 40px;
  padding-left: 20px;
  display: flex;
  align-items: center;

  font-weight: 400;
  font-size: 14px;
  line-height: 28px;
  color: ${({isActive}) => isActive ? '#002CFB !important' : '#899CB1'};

  &:hover {
    background: rgba(0, 44, 251, 0.13);
    color: #122945;
  }
`