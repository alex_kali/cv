import { FC } from "react";
import {DropDownMenuStyled, ItemStyled} from "src/testsModule/test1/components/dropDownPopup/styled";

interface IDropDownMenu {
  list: Array<{title: string, value: string}>,
  value: string,
  onChange: (value: {title: string, value: string}) => void,
  after?: React.ReactNode,
}

export const DropDownPopup:FC<IDropDownMenu> = ({list, value, onChange, after}) => {
  return (
    <DropDownMenuStyled>
      {list.map((item) => <ItemStyled onClick={() => onChange(item)} isActive={item.value === value} key={item.value}>{item.title}</ItemStyled>)}
      {after}
    </DropDownMenuStyled>
  )
}