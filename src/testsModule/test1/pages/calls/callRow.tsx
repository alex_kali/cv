import { FC, useState } from "react";
import {Icons} from "src/testsModule/test1/constants/icons";
import {PlayCall} from "src/testsModule/test1/pages/calls/playCall";
import {
  AvatarStyled,
  CallArrowIconStyled,
  CallRowStyled, CallSpotStyled, DurationStyled,
  EmployeeSpotStyled, MarkSpotStyled, SourceSpotStyled,
  TimeSpotStyled,
  TypeSpotStyled
} from "src/testsModule/test1/pages/calls/styled";
import {ICall} from "src/testsModule/test1/store/calls/model";
import {getHoursMinutes} from "src/testsModule/test1/utils/formatter/getHoursMinutes";

export const CallRow:FC<ICall> = ({in_out, status, date, to_number, source, time, partner_data, person_avatar, record}) => {
  const [isHover, setIsHover] = useState(false);
  
  return (
    <CallRowStyled isHover={isHover} onMouseEnter={() => setIsHover(true)} onMouseLeave={() => setIsHover(false)}>
      <TypeSpotStyled><CallArrowIconStyled in_out={in_out} status={status}><Icons.callArrow/></CallArrowIconStyled></TypeSpotStyled>
      <TimeSpotStyled>{getHoursMinutes(date)}</TimeSpotStyled>
      <EmployeeSpotStyled><AvatarStyled src={person_avatar}/></EmployeeSpotStyled>
      <CallSpotStyled>{to_number}</CallSpotStyled>
      <SourceSpotStyled>{source}</SourceSpotStyled>
      <MarkSpotStyled>не нашел</MarkSpotStyled>
      <DurationStyled>{!!time && <PlayCall partner_id={partner_data.id} time={time} isHover={isHover} record={record}/>}</DurationStyled>
    </CallRowStyled>
  )
}