import {PhoneInput} from "src/testsModule/test1/components/inputs/phoneInput";
import {Theme} from "src/testsModule/test1/constants/theme";
import styled from "styled-components";

export const CallsPageStyled = styled.div`

`

export const RowStyled = styled.div`
  margin-top: 20px;
  display: flex;
  justify-content: end;
  height: 40px;
  align-items: center;
  gap: 48px;
`

export const FilterStyled = styled.div`
  height: 48px;
  margin-top: 20px;
  display: flex;
  align-items: center;
  justify-content: end;
  gap: 32px;
`

export const CustomPhoneInput = styled(PhoneInput)`
  margin-right: auto;
`

export const CallsListStyled = styled.div`
  margin-top: 7px;
  background: #FFFFFF;
  box-shadow: 0px 4px 5px #E9EDF3;
  border-radius: 8px;
  display: flow-root;
`

export const HeaderCallListStyled = styled.div`
  display: flex;
  padding-left: 40px;
  padding-right: 40px;
  margin-top: 24px;
  margin-bottom: 20px;
  height: 16px;
  padding-top: 2px;

  font-weight: 400;
  font-size: 14px;
  line-height: 100%;
  color: #899CB1;
`

export const CallRowStyled = styled.div<{isHover: boolean}>`
  padding-left: 40px;
  padding-right: 40px;
  height: 65px;
  display: flex;
  align-items: center;

  font-weight: 400;
  font-size: 15px;
  line-height: 140%;
  color: #122945;
  position: relative;
  cursor: pointer;
  ${({isHover}) => isHover ? 'background: rgba(212, 223, 243, 0.17);' : ''}
  
  &:before, &:after {
    content: '';
    border-top: 1px solid #EAF0FA;
    width: calc(100% - 40px * 2);
    left: 40px;
    position: absolute;
    ${({isHover}) => isHover ? 'width: 100%; left: 0px' : 'width: calc(100% - 40px * 2);left: 40px;'}
  }
  &:before {
    top: 0;
  }
  &:after{
    bottom: -1px;
  }
  
`

export const CallArrowIconStyled = styled.div<{in_out: boolean, status: string}>`
  width: 24px;
  height: 24px;
  
  ${({in_out}) => in_out ? 'transform: rotate(180deg);' : ''}
  
  svg {
    path {
      fill: ${({in_out, status}) => status === 'Не дозвонился' ? Theme.colors.red : in_out ? Theme.colors.green : '#005FF8'};
    }
    width: 100%;
    height: 100%;
  }
`

export const TypeSpotStyled = styled.div`
  flex: 0 0 calc(23px + 30px);
`

export const TimeSpotStyled = styled.div`
  flex: 0 0 calc(41px + 48px);
`

export const EmployeeSpotStyled = styled.div`
  flex: 0 0 calc(68px + 60px);
`

export const AvatarStyled = styled.div<{src: string}>`
  height: 32px;
  width: 32px;
  border-radius: 20px;
  background-image: url(${({src}) => src});
  background-size: cover;
  background-position: center;
`

export const CallSpotStyled = styled.div`
  flex: 1 1 calc(45px + 281px);
`

export const SourceSpotStyled = styled.div`
  flex: 1 1 calc(60px + 154px);
`

export const MarkSpotStyled = styled.div`
  flex: 1 1 calc(151px);
`

export const DurationStyled = styled.div`
  flex: 1 1 calc(352px);
  display: flex;
  justify-content: end;
`

export const PlayCallStyled = styled.div`
  height: 48px;
  width: 352px;
  border-radius: 48px;
  background: #EAF0FA;
  display: flex;
  padding-left: 19px;
  align-items: center;
`

export const TimeTitleStyled = styled.div`
  font-weight: 400;
  font-size: 15px;
  line-height: 145%;
  padding-top: 2px;
  width: 36px;
`

export const PlayButtonStyled = styled.div`
  height: 24px;
  width: 24px;
  border-radius: 24px;
  margin-left: 12px;
  background: white;
  
  svg {
    width: 100%;
    height: 100%;
  }
`

export const PlayStatusStyled = styled.div<{currentValue: number}>`
  margin-left: 8px;
  height: 4px;
  width: 164px;
  background: #ADBFDF;
  border-radius: 50px;
  position: relative;
  &:before {
    content: '';
    border-radius: 50px;
    position: absolute;
    display: block;
    left: 0;
    top: 0;
    height: 100%;
    background: #002CFB;
    width: ${({currentValue}) => {
      return Math.round(currentValue) || 0
    }}%;
  }
  
  &:hover {
    span{
      display: block;
    }
  }
`

export const TitlePlayStatusStyled = styled.span`
  display: none;
  position: absolute;
  cursor: default;
  top: -20px;
  left: 0;
  width: 50px;
  height: 16px;
  margin-left: -25px;
  text-align: center;
  &:hover{
    display: none !important;
  }
`
export const DownloadButtonStyled = styled.div`
  margin-left: 12px;
  
  height: 24px;
  width: 24px;
  
  cursor: pointer;
  svg {
    width: 100%;
    height: 100%;
  }
`

export const CloseButtonStyled = styled.div`
  width: 24px;
  height: 24px;
  margin-left: 12px;
  svg {
    width: 100%;
    height: 100%;
  }
`