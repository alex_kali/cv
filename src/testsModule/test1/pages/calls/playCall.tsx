import { FC, useEffect, useRef, useState } from "react";
import {Icons} from "src/testsModule/test1/constants/icons";
import {
  CloseButtonStyled,
  DownloadButtonStyled,
  PlayButtonStyled,
  PlayCallStyled, PlayStatusStyled,
  TimeTitleStyled, TitlePlayStatusStyled
} from "src/testsModule/test1/pages/calls/styled";
import {secondsToTime} from "src/testsModule/test1/utils/formatter/secondsToTime";

interface IPlayCall {
  time: number;
  isHover: boolean;
  record: string;
  partner_id: string | number;
}

export const PlayCall:FC<IPlayCall> = ({time, isHover, record, partner_id}) => {
  let {status, currentTime, duration, toPlay, toPause, toDownload, changeCurrentTime, toOff} = useAudio(`getRecord?record=${record}&partnership_id=${partner_id}`)
  const {statusBarRef, titleStatusBarRef} = useStatusBar(isHover, (duration || time), changeCurrentTime)
  
  return (
    <>
      {isHover &&
        <PlayCallStyled>
          <TimeTitleStyled>{secondsToTime(Math.round(currentTime)) || '0:00'}</TimeTitleStyled>
          <PlayButtonStyled>
            {status === 'off' && <Icons.play onClick={toPlay}/>}
            {status === 'loading' && <Icons.play/>}
            {status === 'pause' && <Icons.play onClick={toPlay}/>}
            {status === 'play' && <Icons.pause onClick={toPause}/>}
            {status === 'end' && <Icons.refresh onClick={toPlay}/>}
          </PlayButtonStyled>
          <PlayStatusStyled currentValue={currentTime / (duration || time) * 100} ref={statusBarRef}>
            <TitlePlayStatusStyled ref={titleStatusBarRef}></TitlePlayStatusStyled>
          </PlayStatusStyled>
          <DownloadButtonStyled onClick={toDownload}><Icons.download/></DownloadButtonStyled>
          {status !== 'off' && <CloseButtonStyled onClick={toOff}><Icons.close/></CloseButtonStyled>}
        </PlayCallStyled>
      }
      {!isHover && secondsToTime(time)}
    </>
  )
}

const useStatusBar = (isHover: boolean, duration: number, onClick: (time :number) => void) => {
  const statusBarRef = useRef<any>()
  const titleStatusBarRef = useRef<any>()
  
  useEffect(() => {
    if(isHover){
      const slider = statusBarRef.current
      const slidertitle = titleStatusBarRef.current
      const sliderOffsetX = slider.getBoundingClientRect().left - document.documentElement.getBoundingClientRect().left;
  
      const sliderWidth = slider.offsetWidth - 1;
  
      slider.addEventListener('click', (event:any) => {
        const currentMouseXPos = (event.clientX + window.pageXOffset) - sliderOffsetX;
        let sliderValAtPos = Math.round(currentMouseXPos / sliderWidth * 100 + 1);
        if(sliderValAtPos < 0) sliderValAtPos = 0;
        if(sliderValAtPos > 100) sliderValAtPos = 100;
  
        onClick(Math.round(duration / 100 * sliderValAtPos))
      })
      
      slider.addEventListener('mousemove', function(event:any) {
        const currentMouseXPos = (event.clientX + window.pageXOffset) - sliderOffsetX;
        let sliderValAtPos = Math.round(currentMouseXPos / sliderWidth * 100 + 1);
        if(sliderValAtPos < 0) sliderValAtPos = 0;
        if(sliderValAtPos > 100) sliderValAtPos = 100;
        slidertitle.innerHTML = secondsToTime( Math.round(duration / 100 * sliderValAtPos));
        slidertitle.style.left = currentMouseXPos + 'px';
      });
    }
  }, [isHover])
  
  return {
    statusBarRef: statusBarRef,
    titleStatusBarRef: titleStatusBarRef,
    
  }
}

export const useAudio = (url: string) => {
  const audio = useRef<HTMLAudioElement | null>()
  const urlFile = useRef<string>('')
  const [status, setStatus] = useState<'play' | 'pause' | 'off' | 'loading' | 'end'>('off')
  const [time, setTime] = useState(0)
  const [duration, setDuration] = useState<number | null>(null)
  const loadFile = async () => {
    setStatus('loading')
    const params:any = {
      "headers": {
        'Accept': 'application/json',
        "content-type": "audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3",
        'Content-Transfer-Encoding': 'binary',
        'Content-Disposition': 'filename="record.mp3"'
      },
      "method": "POST",
      "mode": "cors",
      "credentials": "include"
    }
  
    params["headers"]["Authorization"] = 'Bearer testtoken'
  
    const response = await fetch( 'https://api.skilla.ru/mango/' + url, params);
    const data = await response.blob()
    url = URL.createObjectURL(data);
    urlFile.current = url
    audio.current = new Audio(url)
    audio.current.addEventListener('timeupdate', (item) => {
      // @ts-ignore
      setTime(item.currentTarget.currentTime)
    })
    audio.current.addEventListener("loadeddata", function() {
      setDuration(this.duration)
    });
  
    audio.current.addEventListener("ended", function() {
      setStatus('end')
    });
  
  }
  
  const playFile = async () => {
    if(status === 'off' || status === 'end'){
      if(audio.current){
        audio.current.play()
      }else{
        await loadFile()
        // @ts-ignore
        audio.current.play()
      }
      // @ts-ignore
      setDuration(audio.current.duration)
    }else if(audio && status === 'pause'){
      audio.current?.play()
    }
    setStatus('play')
  }
  
  const downloadFile = async () => {
    if(status === 'off'){
      await loadFile()
      setStatus('off')
    }
    if(status !== 'loading'){
      let a = document.createElement('a');
      a.download = 'Звонок.mp3'
      a.href = urlFile.current;
      a.click();
    }
  }
  
  const toPause = () => {
    if(audio.current){
      audio.current.pause()
      setStatus('pause')
    }
  }
  
  const toOff = () => {
    if(audio.current) {
      audio.current.pause()
    }
    setStatus('off')
    // @ts-ignore
    audio.current.currentTime = 0
  }
  
  const changeCurrentTime = async (time: number) => {
    if(status === 'off'){
      await playFile()
    }
    // @ts-ignore
    audio.current.currentTime = time
  }
  
  return {
    status: status,
    toPlay: playFile,
    toPause: toPause,
    toDownload: downloadFile,
    currentTime: time,
    duration: duration,
    changeCurrentTime: changeCurrentTime,
    toOff: toOff,
  }
}