import {CallRow} from "src/testsModule/test1/pages/calls/callRow";
import {
  CallsListStyled, CallSpotStyled, DurationStyled, EmployeeSpotStyled,
  HeaderCallListStyled, MarkSpotStyled, SourceSpotStyled,
  TimeSpotStyled,
  TypeSpotStyled
} from "src/testsModule/test1/pages/calls/styled";
import {Calls} from "src/testsModule/test1/store/calls/model";

export const CallsList = () => {
  const calls = Calls.useData()

  if(!calls){
    return null
  }
  
  return (
    <CallsListStyled>
      <HeaderCallListStyled>
        <TypeSpotStyled>Тип</TypeSpotStyled>
        <TimeSpotStyled>Время</TimeSpotStyled>
        <EmployeeSpotStyled>Сотрудник</EmployeeSpotStyled>
        <CallSpotStyled>Звонок</CallSpotStyled>
        <SourceSpotStyled>Источник</SourceSpotStyled>
        <MarkSpotStyled>Оценка</MarkSpotStyled>
        <DurationStyled>Длительность</DurationStyled>
      </HeaderCallListStyled>
      {calls.map((call) => <CallRow key={call.id} {...call}/>)}
    </CallsListStyled>
  )
}