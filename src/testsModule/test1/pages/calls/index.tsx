import {Balance} from "src/testsModule/test1/components/balance";
import {DataPicker} from "src/testsModule/test1/components/dataPicker";
import {CallsList} from "src/testsModule/test1/pages/calls/callsList";
import {Filter} from "src/testsModule/test1/pages/calls/filter";
import {CallsPageStyled, RowStyled} from "src/testsModule/test1/pages/calls/styled";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {getFiltersCalls} from "src/testsModule/test1/store/calls/thunks";
import {dispatch} from "src/testsModule/test1/store/store";
import { useRef } from "react";

export const CallsPage = () => {
  const form = useForm({
    name: 'filter',
  })
  
  const crutch = useRef(0)
  return (
    <CallsPageStyled>
      <WrapperForm form={form} onChange={(data:any) => {
        if(crutch.current !== 2){
          crutch.current +=1
        }else{
          dispatch(getFiltersCalls(data))
        }
      }}>
        <RowStyled>
          <Balance/>
          <DataPicker
            name={'date'}
            list={[
              {value: '3', title: '3 дня'},
              {value: '7', title: 'Неделя'},
              {value: '30', title: 'Месяц'},
              {value: '365', title: 'Год'},
          ]} value={'3'}/>
        </RowStyled>
        
        <Filter/>
      </WrapperForm>
      <CallsList/>
    </CallsPageStyled>
  )
}