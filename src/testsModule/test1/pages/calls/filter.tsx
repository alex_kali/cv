import {useForm, WrapperForm} from "react-redux-hook-form";
import {DropDown} from "src/testsModule/test1/components/inputs/dropDown";
import {CustomPhoneInput, FilterStyled} from "src/testsModule/test1/pages/calls/styled";

export const Filter = () => {
  
  return (
    <FilterStyled>
      <CustomPhoneInput name={'search'}/>
      
      <DropDown
        name={'type'}
        size={'small'}
        value={"Все типы"}
        list={[{value: "Все типы", title: "Все типы"},{value: "Входящие", title: "Входящие"},{value: "Исходящие", title: "Исходящие"}]}
      />
      <DropDown name={'employees'} size={'small'} value={"Все сотрудники"} list={[{value: "Все сотрудники", title: "Все сотрудники"}]}/>
      <DropDown name={'calls'} size={'small'} value={"Все звонки"} list={[{value: "Все звонки", title: "Все звонки"}]}/>
      <DropDown name={'sources'} size={'small'} value={"Все источники"} list={[{value: "Все источники", title: "Все источники"}]}/>
      <DropDown name={'marks'} size={'small'} value={"Все оценки"} list={[{value: "Все оценки", title: "Все оценки"}]}/>
      <DropDown name={'errors'} size={'small'} value={"Все ошибки"} list={[{value: "Все ошибки", title: "Все ошибки"}]}/>
    </FilterStyled>
  )
}