import styled from "styled-components";

export const Test1AppStyled = styled.div`
  display: flex;
  height: 100vh;
`

export const WrapperContentStyled = styled.div`
  height: 100vh;
  flex: 1 1 auto;
  overflow: auto;
  scrollbar-width: none;
  &::-webkit-scrollbar {
    display: none;
  }
`

export const ContentStyled = styled.div`
  height: 100vh;
  flex: 1 1 auto;
  padding-left: 120px;
  padding-right: 120px;
`