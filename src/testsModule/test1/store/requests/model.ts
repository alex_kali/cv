import {addFailed, addLoading, removeLoading} from "src/testsModule/test1/store/requests/slices";
import {dispatch, getState} from "src/testsModule/test1/store/store";
import {useSelector} from "react-redux";

export const Request = {
  add: (id: string) => {
    dispatch(addLoading({id}))
  },
  success: (id: string) => {
    dispatch(removeLoading({id}))
  },
  failed: (id: string) => {
    dispatch(addFailed({id}))
    dispatch(removeLoading({id}))
  },
  
  useIsLoading: (id: string) => useSelector((state: any) => (state.requests.loadings as Array<string>).includes(id)),
  useIsFailed: (id: string) => useSelector((state: any) => (state.requests.failed as Array<string>).includes(id)),
  
  getIsLoading: (id: string) => (getState().requests.loadings as Array<string>).includes(id),
  getIsFailed: (id: string) => (getState().requests.failed as Array<string>).includes(id),
}