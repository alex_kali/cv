import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  loadings: [],
  failed: [],
};

export const requestsSlice = createSlice({
  name: 'requests',
  initialState,
  reducers: {
    addLoading: (state, action) => {
      const isErrorIndexOf = state.failed.indexOf(action.payload.id)
      if(isErrorIndexOf !== -1){
        state.isError.splice(isErrorIndexOf, 1);
      }
      state.loadings.push(action.payload.id)
    },
    removeLoading: (state, action) => {
      const isLoadingIndexOf = state.isLoading.indexOf(action.payload.id)
      if(isLoadingIndexOf !== -1){
        state.loadings.splice(isLoadingIndexOf, 1);
      }
    },
    addFailed: (state, action) => {
      state.failed.push(action.payload.id)
    },
  },
});

export const { addLoading, removeLoading, addFailed } = requestsSlice.actions

export default requestsSlice.reducer;