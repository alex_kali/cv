/* eslint-disable @typescript-eslint/indent */
import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import { formControllerReducer } from 'react-redux-hook-form';
import callsReducer from './calls/slices';
import requestsReducer from './requests/slices';

export const store = configureStore({
  reducer: {
    formController: formControllerReducer, // подключение formControllerReducer
    requests: requestsReducer,
    calls: callsReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export const dispatch = store.dispatch
export const getState = store.getState

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;