import {setCalls} from "src/testsModule/test1/store/calls/slices";
import {AppThunk} from "src/testsModule/test1/store/store";
import {wrapperFetch} from "src/testsModule/test1/utils/fetch/wrapperFetch";

export const getCalls = (): AppThunk => async (
  dispatch: any
) => {
  const response = await wrapperFetch({url: 'getList', method: 'POST'})
  const data = await response.json();
  
  dispatch(setCalls({data: data.results}))
};

export const getFiltersCalls = (filters:any): AppThunk => async (
  dispatch: any
) => {
  let filter = '?'
  if(filters.search){
    filter += `search=${filters.search}&`
  }
  if(filters.date){
    if(filters.date === '3' || filters.date === '7' || filters.date === '30' || filters.date === '365'){
      const nowData = new Date().toISOString().slice(0, 10)
      let date = new Date()
      let dateOffset = (24*60*60*1000);
      date.setTime(date.getTime() - dateOffset *(+filters.date));
  
      filter += `date_start=${date.toISOString().slice(0, 10)}&date_end=${nowData}&`
    }else{
      let [start, end] = filters.date.split('-')
      start = start.split('.')
      start = `20${start[2]}-${start[1]}-${start[0]}`
      end = end.split('.')
      end = `20${end[2]}-${end[1]}-${end[0]}`
      filter += `date_start=${start}&date_end=${end}&`
    }
  }
  
  if(filters.type !== 'Все типы'){
    filter += `in_out=${filters.type === 'Исходящие' ? 1 : 0}&`
  }
  
  
  
  const response = await wrapperFetch({url: 'getList' + filter , method: 'POST'})
  const data = await response.json();
  
  dispatch(setCalls({data: data.results}))
};
