import {createSlice} from "@reduxjs/toolkit";
export const initialState: any = {
  calls: null,
};

export const callsSlice = createSlice({
  name: 'calls',
  initialState,
  reducers: {
    setCalls: (state, action) => {
      state.calls = action.payload.data
    },
  },
});

export const { setCalls,  } = callsSlice.actions

export default callsSlice.reducer;