import { useEffect } from "react"
import {getCalls} from "src/testsModule/test1/store/calls/thunks";
import {dispatch, RootState} from "src/testsModule/test1/store/store";
import {useSelector} from "react-redux";

export const Calls = {
  useData: () => {
    const data = useSelector((state: RootState) => state.calls.calls) as Array<ICall>
    useEffect(() => {
      if(!data){
        dispatch(getCalls())
      }
    })

    return data
  }
}

export interface ICall {
  id: number | string;
  status: 'Дозвонился' | 'Не дозвонился';
  to_number: string;
  from_number: string;
  source: string;
  person_id: string | number;
  person_name: string;
  person_surname: string;
  person_avatar: string;
  time: number;
  in_out: boolean;
  date: string;
  partner_data: {
    id: string | number;
    phone: string;
  };
  record: string;
}