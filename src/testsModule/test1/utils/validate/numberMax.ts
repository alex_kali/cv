export const numberMax = function(this: any, value: number, messageError?:string ) {
  delete this.min
  if(this.data < value){
    this.isValidate = false
    this.messageError+= messageError || `Максимальное значение ${value}. `
  }
  return this
}