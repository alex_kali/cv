import {IStringValidate} from "./index";

export const stringEmail = function(this: IStringValidate, messageError?:string ) {
  const re = /\S+@\S+\.\S+/
  if(!re.test(this.data)){
    this.isValidate = false
    this.messageError+= messageError || `Enter the correct email. `
  }
  return this
}