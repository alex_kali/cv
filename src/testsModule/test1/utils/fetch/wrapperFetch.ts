interface IWrapperFetch {
  url: string;
  method: "POST" | "GET" | "DELETE" | "PUT";
  body?: object;
}

export const wrapperFetch = async (props:IWrapperFetch) => {
  const params:any = {
    "headers": {
      'Accept': 'application/json',
      "content-type": "application/json",
    },
    "method": props.method,
    "mode": "cors",
    "credentials": "include"
  }
  if(props.body){
    params["body"] = JSON.stringify(props.body)
  }
  
  params["headers"]["Authorization"] = 'Bearer testtoken'
  
  return await fetch( 'https://api.skilla.ru/mango/' + props.url, params);
}