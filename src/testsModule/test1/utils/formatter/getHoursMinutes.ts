export const getHoursMinutes = (date: string) => {
  const unParsedDate = new Date(date);
  let hours = unParsedDate.getHours() + ''
  let minutes = unParsedDate.getMinutes() + ''
  if(hours.length === 1){
    hours = '0'+ hours
  }
  if(minutes.length === 1){
    minutes = '0'+ minutes
  }
  return `${hours}:${minutes}`
}