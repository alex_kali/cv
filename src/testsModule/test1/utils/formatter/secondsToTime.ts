export const secondsToTime = (data?: number) => {
  if(data){
    let seconds = data % 60 + ''
    const minutes = Math.trunc(data / 60) + ''
    if(seconds.length === 1){
      seconds = '0' + seconds
    }
    return `${minutes}:${seconds}`
  }else{
    return ''
  }
}