export let onlyNumber = function(this: any) {
  this.data = parseInt(this.data.replace(/\D+/g,"")) || ''
  return this
}