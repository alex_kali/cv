import { useState } from "react"

export const usePopup = () => {
  const [isView, setIsView] = useState(false)
  return {
    isView: isView,
    open: () => setIsView(true),
    close: () => setIsView(false)
  }
}