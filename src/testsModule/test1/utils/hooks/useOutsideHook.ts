import { useEffect, RefObject, useCallback } from 'react';


export function useOutsideClick(
  refs: Array<RefObject<HTMLElement> | undefined>,
  handler?: () => void,
  isNeed?: boolean,
) {
  
  const handleClickOutside = useCallback(
    function handleClickOutside(event: any) {
      if (!handler) return;
    
      // Clicked browser's scrollbar
      if (
        event.target === document.getElementsByTagName('html')[0] &&
        event.clientX >= document.documentElement.offsetWidth
      )
        return;
    
      let containedToAnyRefs = false;
      for (const rf of refs) {
        if (rf && rf.current && rf.current.contains(event.target)) {
          containedToAnyRefs = true;
          break;
        }
      }
    
      // Not contained to any given refs
      if (!containedToAnyRefs) {
        handler();
      }
    }
  , [])
  
  useEffect(() => {
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, []);
  
  useEffect(() => {
    if(isNeed){
      document.addEventListener('mousedown', handleClickOutside);
    }else{
      document.removeEventListener('mousedown', handleClickOutside);
    }
  }, [isNeed, refs])
}