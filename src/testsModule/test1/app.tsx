import {
  Route, Switch, useRouteMatch,
} from "react-router-dom";
import {Header} from "src/testsModule/test1/components/header";
import {Menu} from "src/testsModule/test1/components/menu";
import {CallsPage} from "src/testsModule/test1/pages/calls";
import {store} from "src/testsModule/test1/store/store";
import {ContentStyled, Test1AppStyled, WrapperContentStyled} from "src/testsModule/test1/styled";
import {Provider} from "react-redux";

export const Test1App = () => {
  // @ts-ignore
  import('./style.css');
  
  let { path } = useRouteMatch();
  
  return (
    <Provider store={store}>
      <Test1AppStyled>
        <Menu/>
        <WrapperContentStyled>
          <Header/>
          <ContentStyled>
            <Switch>
              <Route path={`${path}/calls`}>
                <CallsPage/>
              </Route>
            </Switch>
          </ContentStyled>
        </WrapperContentStyled>
      </Test1AppStyled>
    </Provider>
  )
}