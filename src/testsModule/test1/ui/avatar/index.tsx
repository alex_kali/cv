import {Icons} from "src/testsModule/test1/constants/icons";
import {ArrowStyled, AvatarStyled, ImageStyled} from "src/testsModule/test1/ui/avatar/styled";
import React, {FC} from "react";

interface IAvatar {
  url: string,
  className?: string,
}

export const Avatar:FC<IAvatar> = ({url, className}) => {
  return (
    <AvatarStyled className={className}>
      <ImageStyled url={url}/>
      
      <ArrowStyled><Icons.arrow/></ArrowStyled>
    </AvatarStyled>
  )
}