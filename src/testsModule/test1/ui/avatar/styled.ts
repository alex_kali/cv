import styled from "styled-components";

export const AvatarStyled = styled.div`
  display: flex;
  gap: 4px;
  align-items: center;
  cursor: pointer;
`

export const ImageStyled = styled.div<{url: string}>`
  height: 40px;
  width: 40px;
  border-radius: 50px;
  background-image: url("${props => props.url}");
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
`

export const ArrowStyled = styled.div`
  margin-top: 2px;
  width: 24px;
  height: 24px;
  svg{
    width: 100%;
    height: 100%;
  }
`