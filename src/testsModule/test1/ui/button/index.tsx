import {ButtonStyled, IconStyled, TitleStyled} from "src/testsModule/test1/ui/button/styled";
import React, {FC} from "react";

interface IButton {
  title: string,
  Icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>,
  size?: 'big' | 'small',
  type?: 'primary' | 'secondary' | 'clear',
  className?: string;
}

export const Button:FC<IButton> = ({title, size, type, Icon, className}) => {
  return (
    <ButtonStyled size={size} buttonType={type} className={className}>
      <TitleStyled>
        {title}
      </TitleStyled>
      {Icon &&
        <IconStyled>
          <Icon/>
        </IconStyled>
      }
    </ButtonStyled>
  )
}