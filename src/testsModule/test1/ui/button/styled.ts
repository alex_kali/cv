import styled from "styled-components";

export const ButtonStyled = styled.button<{buttonType?: 'primary' | 'secondary' | 'clear', size?: 'big' | 'small'}>`
  padding-left: 24px;
  padding-right: 20px;
  height: 52px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #FFFFFF;
  border-radius: 4px;
  gap: 17px;
  
  ${({buttonType}) => buttonType === 'primary' ? 'background: #005FF8;' : ''}


  font-weight: 500;
  font-size: 16px;
  line-height: 148%;
`

export const TitleStyled = styled.div`
  padding-top: 2px;
  flex: 1 1 auto;
`

export const IconStyled = styled.div`
  width: 24px;
  height: 24px;
  margin-left: auto;
  svg {
    width: 100%;
    height: 100%;
  }
`