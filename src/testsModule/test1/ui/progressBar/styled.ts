import {Theme} from "src/testsModule/test1/constants/theme";
import styled from "styled-components";

export const ProgressBarStyled = styled.div`
  display: flex;
  flex-direction: column;
  gap: 7px;
`

export const TitleStyled = styled.div`
  font-weight: 400;
  font-size: 14px;
  line-height: 148%;
  white-space: nowrap;
  
`

export const TitleStatusStyled = styled.div<{color: 'green' | 'yellow' | 'red'}>`
  display: inline;
  color: ${({color}) => Theme.colors[color]};
`

export const StatusStyled = styled.div<{status: number, color: 'green' | 'yellow' | 'red'}>`
  height: 6px;
  width: 156px;
  border-radius: 12px;
  background: #DEE6F5;
  position: relative;
  overflow: hidden;
  &:after {
    content: '';
    width: ${({status}) => status * 100}%;
    background: ${({color}) => Theme.colors[color]};
    height: 100%;
    position: absolute;
    left: 0;
    top: 0;
  }
`