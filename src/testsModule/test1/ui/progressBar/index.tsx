import {
  ProgressBarStyled,
  StatusStyled,
  TitleStatusStyled,
  TitleStyled
} from "src/testsModule/test1/ui/progressBar/styled";
import React, {FC} from "react";

interface IProgressBar {
  title?: {
    text: string;
    status: string;
  },
  status: number,
  color: 'green' | 'yellow' | 'red',
}

export const ProgressBar:FC<IProgressBar> = ({title, color, status}) => {
  return (
    <ProgressBarStyled>
      {title &&
        <TitleStyled>{title.text}<TitleStatusStyled color={color}>{title.status}</TitleStatusStyled></TitleStyled>
      }
      <StatusStyled status={status} color={color}/>
    </ProgressBarStyled>
  )
}