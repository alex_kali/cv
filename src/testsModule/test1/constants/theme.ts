export const Theme = {
  colors: {
    darkBlue: '#091336',
    green: '#28A879',
    yellow: '#FFD500',
    red: '#EA1A4F',
  }
}