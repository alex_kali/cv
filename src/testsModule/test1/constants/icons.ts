import {ReactComponent as logo} from "./icons/logo.svg";
import {ReactComponent as documents} from "./icons/documents.svg";
import {ReactComponent as briefcase} from "./icons/briefcase.svg";
import {ReactComponent as library} from "./icons/library.svg";
import {ReactComponent as messages} from "./icons/messages.svg";
import {ReactComponent as orders} from "./icons/orders.svg";
import {ReactComponent as people} from "./icons/people.svg";
import {ReactComponent as perm} from "./icons/perm.svg";
import {ReactComponent as results} from "./icons/results.svg";
import {ReactComponent as settings} from "./icons/settings.svg";
import {ReactComponent as calls} from "./icons/calls.svg";
import {ReactComponent as plus} from "./icons/plus.svg";
import {ReactComponent as exclamation} from "./icons/exclamation.svg";
import {ReactComponent as search} from "./icons/search.svg";
import {ReactComponent as arrow} from "./icons/arrow.svg";
import {ReactComponent as calendar} from "./icons/calendar.svg";
import {ReactComponent as close} from "./icons/close.svg";
import {ReactComponent as callArrow} from "./icons/callArrow.svg";
import {ReactComponent as play} from "./icons/play.svg";
import {ReactComponent as download} from "./icons/download.svg";
import {ReactComponent as pause} from "./icons/pause.svg";
import {ReactComponent as refresh} from "./icons/refresh.svg";

export const Icons = {
  logo: logo,
  documents: documents,
  briefcase: briefcase,
  library: library,
  messages: messages,
  orders: orders,
  people: people,
  perm: perm,
  results: results,
  settings: settings,
  calls: calls,
  plus: plus,
  exclamation: exclamation,
  search: search,
  arrow: arrow,
  calendar: calendar,
  close: close,
  callArrow: callArrow,
  play: play,
  download: download,
  pause: pause,
  refresh:refresh,
}