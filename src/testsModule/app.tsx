import React from 'react';
import {Provider} from "react-redux";
import {
  Route, Switch, useRouteMatch,
} from "react-router-dom";
import {Test1App} from "src/testsModule/test1/app";

declare global {
  interface Window {
    dispatch:any;
  }
}

export function TestsApp() {
  let { path } = useRouteMatch();

  return (
    <>
      <Switch>
        <Route path={`${path}/test1`}>
          <Test1App/>
        </Route>
      </Switch>
    </>
  );
}
