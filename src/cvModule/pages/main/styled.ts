import styled from "styled-components";

export const WrapperMainPageStyled = styled.div`
  padding: 13px 15px;
  position: relative;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  &:before {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    width: 100%;
    height: 50%;
    top: 0;
    background: #DDDFDE;
    z-index: -1;
  }
  
  &:after {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    width: 100%;
    height: 50%;
    top: 50%;
    background: #FEEDE6;
    z-index: -1;
  }
`
export const MainPageStyled = styled.main`
  position: relative;
  padding: 20px;
  background: white;
  width: 595px;
  min-height: 842px;
  display: flex;
  flex-direction: column;
`

export const LeftBlockStyled = styled.div`
  height: calc(100% - 70px);
  width: 39%;
`

export const RightBlockStyled = styled.div`
  height: calc(100% - 70px);
  width: 61%;
  margin-top: -15px;
`

export const BottomBlockStyled = styled.div`
  left: 0;
  bottom: 0;
  height: 70px;
  width: 100%;
  margin-top: auto;
`

export const MainBlockStyled = styled.div`
  display: flex;
  flex-direction: row;
  column-gap: 40px;
`

export const LinkStyled = styled.a`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 9px;

  overflow-wrap: anywhere;
  
  cursor: pointer;
  transition-duration: 0.3s;
  &:hover {
    color: #d08e1b;
  }
`

export const TextStyled = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 9px;
`