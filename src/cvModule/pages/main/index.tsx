import {useState} from "react";
import {useTranslation} from "react-i18next/hooks";
import {Description} from "src/cvModule/components/description";
import {Footer} from "src/cvModule/components/footer";
import {Header} from "src/cvModule/components/header";
import {List} from "src/cvModule/components/list";
import {Table} from "src/cvModule/components/table";
import {UnorderedList} from "src/cvModule/components/unorderedList";
import {
  BottomBlockStyled,
  LeftBlockStyled, LinkStyled, MainBlockStyled,
  MainPageStyled,
  RightBlockStyled, TextStyled,
  WrapperMainPageStyled
} from "src/cvModule/pages/main/styled";
import contactIcon from "./icons/contact.svg";
import gantelIcon from "./icons/gear.svg";
import educationIcon from "./icons/education.svg";
import experienceIcon from "./icons/experience.svg";
import bulbIcon from "./icons/bulb.png";
import {ReactComponent as home} from "./icons/home.svg";
import {ReactComponent as email} from "./icons/email.svg";
import {ReactComponent as internet} from "./icons/internet.svg";
import {ReactComponent as tel} from "./icons/tel.svg";

export const MainPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  const  [t, ]  = useTranslation();
  
  return (
    <WrapperMainPageStyled>
      
      <MainPageStyled
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        <Header isOpen={isOpen}/>
        <MainBlockStyled>
          <LeftBlockStyled>
            <Description
              title={t("name")}
              subTitle={t("title")}
              description={t("description")}
            />
            <Table
              title={{icon: contactIcon, text:  t("CONTACT")}}
              rows={[
                {icon: home, text: t("home")},
                {icon: tel, text: '+7 999 600 32 25'},
                {icon: email, text: 'lexa29.03.1999@gmail.com'},
                {icon: internet, text: 'http://81.200.145.79:3000/cv/main'},
              ]}
            />
            <UnorderedList
              title={{icon: gantelIcon, text: t("SKILLS")}}
              items={[
                {
                  title: 'Frontend',
                  points: [
                    'CSS/HTML',
                    'JS/TS',
                    'REACT',
                    'Redux Toolkit',
                    'styled-components',
                    'sass/scss',
                    'Jest/React Testing Library',
                  ]
                },
                {
                  title: 'UI',
                  points: [
                    'Antd',
                    'Material',
                    'Consta design',
                  ]
                },
                {
                  title: 'Backend',
                  points: [
                    'Python',
                    'Django',
                    'PostgreSQL',
                    'REST API/GraphQL',
                  ]
                },
                {
                  title: t("Others"),
                  points: [
                    'gitlab/github',
                    'aws',
                    'ci/cd',
                    'docker',
                  ]
                },
              ]}
            />
          </LeftBlockStyled>
          <RightBlockStyled>
            <UnorderedList
              isDecorated
              title={{icon: educationIcon, text: t("EDUCATION")}}
              items={[
                {
                  title: t("Astrakhan State University (Information Security 2016-2020)"),
                  points: [
                    t("1year"),
                    t("2year"),
                    t("3year"),
                    t("4year"),
                  ]
                },
              ]}
            />
    
            <UnorderedList
              isDecorated
              title={{icon: experienceIcon, text: t("EXPERIENCE")}}
              items={[
                {
                  title: 'Junior developer',
                  description: t('WebSailors'),
                  points: [
                    <>
                      Soundgram ({t("music mobile service")})<br/>
                      {t("Team")}: 2 backend, 1 frontend <br/>
                      {t("Technology stack")}: python/django/react native/graphql <br/>
                      {t("My role: create a new django backend, and integrate it with the existing frontend. Then write a ci/cd and deploy the project to aws")} <br/>
                    </>,
                    <>
                      Cityline ({t("real estate rental services")})<br/>
                      {t("Team")}: 3 frontend, 1 backend, 1 devops, 1 {t("designer")}, 1 pm<br/>
                      {t("Technology stack")}: ts/react/redux toolkit/antd/styled <br/>
                      {t("My role: Creating and updating react components, interacting with the backend, working with the world3d map.")} <br/>
                    </>,
                    <>
                      Icowork ({t("coworking rental services")})<br/>
                      {t("Team")}: 1 frontend, 1 backend, 1 devops, 1 {t("designer")}, 1 pm<br/>
                      {t("Technology stack")}: ts/react/redux toolkit/antd/styled <br/>
                      {t("My role: Creating and updating new react app.")} <br/>
                    </>,
                  ]
                },
                {
                  title: `Pre-middle Developer(${t("Project work")})`,
                  description: t("mst"),
                  points: [
                    <>
                      CBI ({t("information security services")})<br/>
                      {t("Team")}: 3 frontend, 1 backend, 1 {t("designer")}, 1 pm<br/>
                      {t("Technology stack")}: ts/react/redux toolkit/sass <br/>
                      {t("My role: creating an app and integrating with wordpress.")} <br/>
                    </>,
                  ]
                },
              ]}
            />
            <UnorderedList
              isDecorated
              title={{icon: bulbIcon, text: 'PETS'}}
              items={[
                {
                  title: "Invest Map",
                  points: [
                    <TextStyled>{t("The service allows you to view statistics on the map.")}</TextStyled>,
                    <TextStyled>{t("More than 10,000 entries in the database.")}</TextStyled>,
                    <>
                      <LinkStyled href="/cv/map" target={'_blank'} className="not-to-pdf">{t("Learn more ...")}</LinkStyled>
                      <TextStyled className="to-pdf">{t("Link")}: http://81.200.145.79:3000/cv/map</TextStyled>
                    </>
                  ]
                },
                {
                  title: 'Restaurant',
                  points: [
                      <TextStyled>{t("Restaurant service, with cool design.")}</TextStyled>,
                      <TextStyled>{t("Full interactive.")}</TextStyled>,
                    <>
                      <LinkStyled href="/cv/restaurant" target={'_blank'} className="not-to-pdf">{t("Learn more ...")}</LinkStyled>
                      <TextStyled className="to-pdf">{t("Link")}: http://81.200.145.79:3000/cv/restaurant</TextStyled>
                    </>,
                  ]
                },
                {
                  title: 'react-redux-hook-form',
                  points: [
                    <TextStyled>{t("The library is designed to simplify working with forms in React.")}</TextStyled>,
                    <>
                      <LinkStyled href="/cv/library" target={'_blank'} className="not-to-pdf">{t("Learn more ...")}</LinkStyled>
                      <TextStyled className="to-pdf">{t("Link")}: http://81.200.145.79:3000/cv/library</TextStyled>
                    </>
                  ]
                },
              ]}
            />
          </RightBlockStyled>
        </MainBlockStyled>
        <BottomBlockStyled>
          <Footer/>
        </BottomBlockStyled>
      </MainPageStyled>
    </WrapperMainPageStyled>
  )
}