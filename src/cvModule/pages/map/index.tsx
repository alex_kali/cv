import React, {useState} from "react";
import {Description} from "src/cvModule/components/description";
import {Footer} from "src/cvModule/components/footer";
import {Header} from "src/cvModule/components/header";
import {ImageList} from "src/cvModule/components/imageList";
import {List} from "src/cvModule/components/list";
import {Title} from "src/cvModule/components/title";
import {UnorderedList} from "src/cvModule/components/unorderedList";
import linksIcon from "./icons/links.svg";
import gantelIcon from "src/cvModule/pages/main/icons/gear.svg";
import {
  BottomBlockStyled,
  LeftBlockStyled, LinkStyled,
  MainBlockStyled, MainPageStyled,
  RightBlockStyled,
  WrapperMainPageStyled
} from "src/cvModule/pages/main/styled";
import screen1 from "./icons/screen1.png";
import screen3 from "./icons/screen3.png";
import screen4 from "./icons/screen4.png";
import screens from "./icons/screens.svg";

export const MapPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <WrapperMainPageStyled>
    
      <MainPageStyled
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        <Header isOpen={isOpen}/>
        <MainBlockStyled>
          <LeftBlockStyled>
            <Description
              subTitle="View statistics on the map"
              description="
                The service allows you to view statistics on the map.
                The map shows relative values, i.e. for population, to show population density. gdp - gdp of the region / number of people.
                Also implemented a change of color palette.
                At the beginning, I wanted to take data from Rosstat and integrate the service with them.
                But they publish the data in PDF format, which is impossible to parse, so they took the data from other sources.
                
              "
            />
            <UnorderedList
              title={{icon: gantelIcon, text: 'STACK'}}
              items={[
                {
                  title: 'Frontend',
                  points: [
                    'CSS/HTML',
                    'TS',
                    'REACT',
                    'Redux Toolkit',
                    'styled-components',
                  ]
                },
                {
                  title: 'Backend',
                  points: [
                    'Python',
                    'Django',
                    'Graphql',
                  ]
                },
              ]}
            />
            <UnorderedList
              title={{icon: linksIcon, text: 'LINKS'}}
              items={[
                {
                  title: 'Gitlab backend',
                  points: [
                    <LinkStyled href="https://gitlab.com/alex_kali/investsolutionbackend/-/tree/develop" target={'_blank'}>https://gitlab.com/alex_kali/investsolutionbackend/-/tree/develop</LinkStyled>
                  ]
                },
                {
                  title: 'Gitlab frontend',
                  points: [
                    <LinkStyled href="https://gitlab.com/alex_kali/cv/-/tree/main/src/mapViewerModule" target={'_blank'}>https://gitlab.com/alex_kali/cv/-/tree/main/src/mapViewerModule</LinkStyled>
                  ]
                },
                {
                  title: 'Deploy',
                  points: [
                    <LinkStyled href="/map" target={'_blank'}>http://81.200.145.79:3000/map</LinkStyled>
                  ]
                },
              ]}
            />
            
          </LeftBlockStyled>
          <RightBlockStyled>
            <Title text="SCREENS" icon={screens}/>
            <ImageList images={[
              screen1,
              screen3,
              screen4,
            ]}/>
          </RightBlockStyled>
        </MainBlockStyled>
        <BottomBlockStyled>
          <Footer/>
        </BottomBlockStyled>
      </MainPageStyled>
    </WrapperMainPageStyled>
  )
}