import React, {useState} from "react";
import {Description} from "src/cvModule/components/description";
import {Footer} from "src/cvModule/components/footer";
import {Header} from "src/cvModule/components/header";
import {List} from "src/cvModule/components/list";
import {Title} from "src/cvModule/components/title";
import {UnorderedList} from "src/cvModule/components/unorderedList";
import {CodeTextStyled, DescriptionTextStyled} from "src/cvModule/pages/library/styled";
import linksIcon from "./icons/links.svg";
import descriptionIcon from "./icons/description.svg";
import gantelIcon from "src/cvModule/pages/main/icons/gear.svg";
import {
  BottomBlockStyled,
  LeftBlockStyled, LinkStyled,
  MainBlockStyled, MainPageStyled,
  RightBlockStyled,
  WrapperMainPageStyled
} from "src/cvModule/pages/main/styled";

export const LibraryPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <WrapperMainPageStyled>
      
      <MainPageStyled
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        <Header isOpen={isOpen}/>
        <MainBlockStyled>
          <LeftBlockStyled>
            <Description
              subTitle="react-redux-hook-form"
              description="
                The library is designed to simplify working with forms in React.
                
              "
            />
            <UnorderedList
              title={{icon: gantelIcon, text: 'STACK'}}
              items={[
                {
                  title: 'Frontend',
                  points: [
                    'TS',
                    'REACT',
                    'Redux Toolkit',
                  ]
                },
              ]}
            />
            <UnorderedList
              title={{icon: linksIcon, text: 'LINKS'}}
              items={[
                {
                  title: 'npm',
                  points: [
                    <LinkStyled href="https://www.npmjs.com/package/react-redux-hook-form" target={'_blank'}>https://www.npmjs.com/package/react-redux-hook-form</LinkStyled>
                  ]
                },
              ]}
            />
          
          </LeftBlockStyled>
          <RightBlockStyled>
            <Title text="DESCRIPTION" icon={descriptionIcon}/>
            <DescriptionTextStyled>
              The main hook is useField.
              useField - used to initialize the form field
              From useField we get the following hooks useData, useIsValidate, useIsTouch, useMessageError
              Work on the principle of useState
              value is written to redux state.
            </DescriptionTextStyled>
            <CodeTextStyled className={"language-typescript"}>
              {`
interface IEmail {
  name: string;
  required?: boolean;
}

export const EmailInput: FC<IEmail> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => validate(value).string().max(30).email(),
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <EmailInputStyled
        type={"email"} value={data}
        onChange={(e:any)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
        id={props.name}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
              `}
            </CodeTextStyled>
            <DescriptionTextStyled>
              The final form of the form made using the library
            </DescriptionTextStyled>
            <CodeTextStyled className={"language-typescript"}>
              {`
export const Login:FC = memo( () => {
  const form = useForm({name: 'login'})

  return (
    <WrapperForm form={form} onSubmit={(data:ILogin) => {User.options.login(data)}}>
      <InputTitleStyled>Номер телефона</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Пароль</InputTitleStyled>
      <PasswordInput name={'password'} required/>

      <SubmitButton text={'Авторизироваться'}/>
    </WrapperForm>
  )
})
              `}
            </CodeTextStyled>
          </RightBlockStyled>
        </MainBlockStyled>
        <BottomBlockStyled>
          <Footer/>
        </BottomBlockStyled>
      </MainPageStyled>
    </WrapperMainPageStyled>
  )
}