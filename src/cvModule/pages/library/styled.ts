import styled from "styled-components";
import Highlight from 'react-highlight';


export const DescriptionTextStyled = styled.div`
  margin-top: 6px;
  margin-bottom: 6px;
  font-family: 'Roboto';
  font-weight: 400;
  font-size: 8px;
  line-height: 12px;
  text-align: justify;
  color: #414042;
`

export const CodeTextStyled = styled(Highlight)`
  font-family: 'Roboto';
  font-weight: 400;
  font-size: 8px;
  background: rgb(240,240,240);
  padding: 0 5px !important;
  .hljs-tag {
    color: black !important;
  }
`