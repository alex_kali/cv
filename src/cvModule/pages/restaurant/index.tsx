import React, {useState} from "react";
import {Description} from "src/cvModule/components/description";
import {Footer} from "src/cvModule/components/footer";
import {Header} from "src/cvModule/components/header";
import {ImageList} from "src/cvModule/components/imageList";
import {List} from "src/cvModule/components/list";
import {Title} from "src/cvModule/components/title";
import {UnorderedList} from "src/cvModule/components/unorderedList";
import linksIcon from "./icons/links.svg";
import gantelIcon from "src/cvModule/pages/main/icons/gear.svg";
import {
  BottomBlockStyled,
  LeftBlockStyled, LinkStyled,
  MainBlockStyled, MainPageStyled,
  RightBlockStyled,
  WrapperMainPageStyled
} from "src/cvModule/pages/main/styled";
import screen1 from "./icons/screen1.png";
import screen2 from "./icons/screen2.png";
import screen3 from "./icons/screen3.png";
import screens from "./icons/screens.svg";

export const RestaurantPage = () => {
  const [isOpen, setIsOpen] = useState(false);
  return (
    <WrapperMainPageStyled>
      
      <MainPageStyled
        onMouseEnter={() => setIsOpen(true)}
        onMouseLeave={() => setIsOpen(false)}
      >
        <Header isOpen={isOpen}/>
        <MainBlockStyled>
          <LeftBlockStyled>
            <Description
              subTitle="Restaurant"
              description="
                Restaurant service, with professional design.
                Implemented only the frontend part.
                The site is fully interactive.
              "
            />
            <UnorderedList
              title={{icon: gantelIcon, text: 'STACK'}}
              items={[
                {
                  title: 'Frontend',
                  points: [
                    'CSS/HTML',
                    'TS',
                    'REACT',
                    'Redux Toolkit',
                    'styled-components',
                  ]
                },
              ]}
            />
            <UnorderedList
              title={{icon: linksIcon, text: 'LINKS'}}
              items={[
                {
                  title: 'Gitlab frontend',
                  points: [
                    <LinkStyled href="https://gitlab.com/alex_kali/cv/-/tree/main/src/restaurantModule" target={'_blank'}>https://gitlab.com/alex_kali/cv/-/tree/main/src/restaurantModule</LinkStyled>
                  ]
                },
                {
                  title: 'Deploy',
                  points: [
                    <LinkStyled href="/restaurant/center/home" target={'_blank'}>http://81.200.145.79:3000/restaurant/center/home</LinkStyled>
                  ]
                },
              ]}
            />
          
          </LeftBlockStyled>
          <RightBlockStyled>
            <Title text="SCREENS" icon={screens}/>
            <ImageList images={[
              screen3,
              screen1,
              screen2,
            ]}/>
          </RightBlockStyled>
        </MainBlockStyled>
        <BottomBlockStyled>
          <Footer/>
        </BottomBlockStyled>
      </MainPageStyled>
    </WrapperMainPageStyled>
  )
}