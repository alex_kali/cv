import React from "react";
import { Route, Switch, useRouteMatch} from "react-router-dom";
import {LibraryPage} from "src/cvModule/pages/library";
import {MainPage} from "src/cvModule/pages/main";
import {MapPage} from "src/cvModule/pages/map";
import {RestaurantPage} from "src/cvModule/pages/restaurant";
import './locales/init';

export const CVApp = () => {
  // @ts-ignore
  import('./style.css');
  
  let { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/main`}>
        <MainPage />
      </Route>
      <Route path={`${path}/map`}>
        <MapPage />
      </Route>
      <Route path={`${path}/restaurant`}>
        <RestaurantPage />
      </Route>
      <Route path={`${path}/library`}>
        <LibraryPage />
      </Route>
    </Switch>
  );
}