import React from "react";
import {ListLinks} from "src/cvModule/components/listLinks";

import {ReactComponent as linkedin} from "./icons/linkedin.svg";
import {ReactComponent as telegram} from "./icons/telegram.svg";
import {ReactComponent as hh} from "./icons/hh.svg";
import {ReactComponent as github} from "./icons/github.svg";

export const Footer = () => {
  return (
    <ListLinks
      links={[
        {href: 'http://linkedin.com/in/alex-kali', icon: linkedin, text: 'linkedin.com/in/alex-kali'},
        {href: 'https://gitlab.com/alex_kali', icon: github, text: 'https://gitlab.com/alex_kali'},
        {href: 'https://t.me/lexa29031999', icon: telegram, text: 'https://t.me/lexa29031999'},
        {href: 'https://rostov.hh.ru/resume/fe947fbcff09cc096c0039ed1f4d737a534f4c', icon: hh, text: 'https://inlnk.ru/jENk3K'},
      ]}
    />
  )
}