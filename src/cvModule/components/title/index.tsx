import {FC} from "react";
import {IconStyled, TextStyled, TitleContentStyled, TitleStyled} from "src/cvModule/components/title/styled";

export interface ITitle {
  text: string;
  icon: string;
}

export const Title:FC<ITitle> = ({text, icon}) => {

  return (
    <TitleStyled>
      <TitleContentStyled>
        <IconStyled src={icon}/>
        <TextStyled>
          {text}
        </TextStyled>
      </TitleContentStyled>
    </TitleStyled>
  )
}