import styled from "styled-components";

export const TitleStyled = styled.div`
  margin-top: 25px;
  
  &:before {
    content: '';
    display: block;
    height: 1px;
    width: 100%;
    background: #414042;
  }
  
  &:after {
    content: '';
    display: block;
    height: 1px;
    width: 100%;
    background: #414042;
  }
`

export const TitleContentStyled = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 25px;
`

export const IconStyled = styled.img`
  width: 15px;
  height: 15px;
  margin-right: 5px;
  object-fit: cover;
`

export const TextStyled = styled.div`
  font-weight: 700;
  font-size: 11px;
  line-height: 14px;
  height: 11px;
`