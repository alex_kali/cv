import jsPDF from 'jspdf';
import {FC, useEffect, useRef, useState} from "react";
import {useTranslation} from "react-i18next/hooks";
import {HeaderStyled, WrapperFlagStyled, WrapperIconStyled} from "src/cvModule/components/header/styled";
import {ReactComponent as Download} from "./icons/download.svg";
import {ReactComponent as Exit} from "./icons/exit-full-screen.svg";
import {ReactComponent as Full} from "./icons/full-screen.svg";
import {ReactComponent as Russia} from "./icons/russia.svg";
import {ReactComponent as England} from "./icons/england.svg";
import html2canvas from 'html2canvas';

interface IHeader {
  isOpen: boolean;
}
export const Header:FC<IHeader> = ({isOpen}) => {
  const ref = useRef() as any;
  const [isFull, setIsFull] = useState(false);
  const [ , i18n ] = useTranslation();

  useEffect(() => {
    if(ref.current){
      const element = document.body;
      if(isFull){
        element.style.transform = 'scale(2)';
        element.style.transformOrigin = '0 0';
        element.style.width = '50%';
        window.scrollTo( window.screen.width/2, 0 );
      } else {
        element.style.transform = 'scale(1)';
        element.style.transformOrigin = '0 0';
        element.style.width = '100%';
      }
    }
  }, [isFull, ref])
  
  const handleDownloadPDF = async () => {
    setIsFull(false);
    await setTimeout(()=>{}, 0);
    const element = ref.current.parentElement;
    element.classList.add("screen-pdf")
    const canvas = await html2canvas(element, {
      scale: 5,
    });
  
    const data = canvas.toDataURL('image/jpg',1);
    
    const doc = new jsPDF("p", "mm", "a4");
  
    const width = doc.internal.pageSize.getWidth();
    const height = doc.internal.pageSize.getHeight();
    
    const pdf = new jsPDF();
    pdf.addImage(data, 'PNG', 0, 0, width, height);
    pdf.save("download.pdf");
    element.classList.remove("screen-pdf")
  }

  return (
    <HeaderStyled ref={ref} isOpen={isOpen} className="not-to-pdf">
      <WrapperFlagStyled
        isActive={i18n.language === 'ru'}
        onClick={() => i18n.changeLanguage('ru')}
      >
        <Russia/>
      </WrapperFlagStyled>
      
      <WrapperFlagStyled
        isActive={i18n.language === 'en'}
        onClick={() => i18n.changeLanguage('en')}
      >
        <England/>
      </WrapperFlagStyled>
      
      <WrapperIconStyled onClick={handleDownloadPDF}><Download /> </WrapperIconStyled>
      <WrapperIconStyled onClick={() => setIsFull((value) => !value)}>
        {isFull && <Exit/>}
        {!isFull && <Full/>}
      </WrapperIconStyled>
    </HeaderStyled>
  )
}