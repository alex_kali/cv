import styled from "styled-components";

export const HeaderStyled = styled.div<{isOpen: boolean}>`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  background: linear-gradient(0deg, rgba(0,0,0,0) 0%, rgba(240,240,240,1) 100%);
  transition-duration: 0.3s;
  opacity: ${props => props.isOpen ? 1 : 0};
  height: ${props => props.isOpen ? '50px' : '0'};
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: right;
  overflow: hidden;
`

export const WrapperIconStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 30px;
  height: 30px;
  position: relative;
  text-align: center;
  transition-duration: 0.2s;
  opacity: 0.7;
  cursor: pointer;
  margin-right: 20px;
  .main-color {
    transition-duration: 0.4s;
  }
  &:hover {
    opacity: 0.9;
    .main-color {
      fill: #d08e1b;
    }
    svg {
      color: #d08e1b;
    }
  }
  & svg {
    max-height: 100%;
    max-width: 100%;
  }
`

export const WrapperFlagStyled = styled.div<{isActive: boolean}>`
  display: flex;
  justify-content: center;
  align-items: center;
  
  width: 30px;
  height: 30px;
  position: relative;
  text-align: center;
  transition-duration: 0.2s;
  opacity: 0.7;
  cursor: pointer;
  margin-right: 20px;
  ${({isActive}) => isActive ? 'filter: grayscale(0%);' : 'filter: grayscale(100%);'}
  &:hover {
    opacity: 0.9;
    filter: grayscale(0%);
  }
`