import {FC} from "react";
import {ImageListStyled, ImageStyled} from "src/cvModule/components/imageList/styled";

interface IImageList {
  images: Array<string>;
}

export const ImageList:FC<IImageList> = ({images}) => {
  
  return (
    <ImageListStyled>
      {images.map((image) => <ImageStyled src={image}/>)}
    </ImageListStyled>
  )
}