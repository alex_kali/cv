import styled from "styled-components";

export const ImageListStyled = styled.div`
  width: 100%;
`

export const ImageStyled = styled.img`
  width: 100%;
  height:  auto;
  margin-top: 20px;
  object-fit: contain;
`