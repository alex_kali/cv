import styled from "styled-components";
import avatar from "./icons/avatar2.png";

export const NameStyled = styled.div`
  font-weight: 700;
  font-size: 30px;
  line-height: 38px;
  text-align: center;
  margin-bottom: 3px;
`

export const JobStyled = styled.div`
  font-family: 'Roboto';
  font-weight: 400;
  font-size: 15px;
  line-height: 18px;
  text-align: center;
  margin-bottom: 15px;
`

export const AvatarStyled = styled.div`
  margin: auto auto 25px;
  width: 132px;
  height: 118px;
  background-size: cover;
  background-position: center;
  background-image: url(${avatar});
`

export const DescriptionStyled = styled.div`
  font-family: 'Roboto';
  font-weight: 400;
  font-size: 8px;
  line-height: 12px;
  text-align: justify;
  color: #414042;
`