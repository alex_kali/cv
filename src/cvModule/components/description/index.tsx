import {FC} from "react";
import {DescriptionStyled, JobStyled, NameStyled} from "src/cvModule/components/description/styled";

interface IDescription {
  title?: string;
  subTitle?: string;
  description: string;
}
export const Description:FC<IDescription> = ({title, subTitle, description}) => {
  return (
    <>
      {title && <NameStyled>{title}</NameStyled>}
      {subTitle && <JobStyled>{subTitle}</JobStyled>}
      <DescriptionStyled>{description}</DescriptionStyled>
    </>
  )
}