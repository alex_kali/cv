import styled from "styled-components";

export const LinkListStyled = styled.div`
  display: flex;
  flex-direction: row;
  gap: 22px;
  justify-content: center;
  align-items: center;
  height: 100%;
`

export const LinkStyled = styled.a`
  cursor: pointer;
  min-width: 90px;

  * {
    transition-duration: 0.3s;
  }

  &:hover {
    * {
      color: #d08e1b;
      fill: #d08e1b;
    }
    .second-color {
      fill: white;
    }
  }
`

export const IconStyled = styled.div`
  margin-left: auto;
  margin-right: auto;
  width: 20px;
  height: 20px;
  svg {
    width: 100%;
    height: 100%;
  }
`

export const TitleStyled = styled.div`
  margin-top: 8px;
  
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 9px;
  line-height: 11px;
  text-align: center;
`