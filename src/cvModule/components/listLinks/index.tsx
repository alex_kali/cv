import {FC} from "react";
import {IconStyled, LinkListStyled, LinkStyled, TitleStyled} from "src/cvModule/components/listLinks/styled";

interface IListLinks {
  links: Array<{
    icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
    text: string;
    href: string;
  }>
}

export const ListLinks:FC<IListLinks> = ({links}) => {
  return (
    <LinkListStyled>
      {links.map(({icon, text, href}) => {
        const Icon = icon as React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
        return (
          <LinkStyled href={href} target={"_blank"}>
            <IconStyled><Icon/></IconStyled>
            <TitleStyled>{text}</TitleStyled>
          </LinkStyled>
        )
      })}
    </LinkListStyled>
  )
}