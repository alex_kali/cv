import {FC} from "react";
import {DotStyled, MarkStyled, NameStyled, RowStyled} from "src/cvModule/components/list/styled";

export interface IItem {
  name: string;
  mark?: number;
}

export const Row:FC<IItem> = ({name, mark}) => {
  return (
    <RowStyled>
      <NameStyled>{name}</NameStyled>
      {mark && (
        <MarkStyled>
          {Array.from({ length: 5 }, (_, i) => <DotStyled key={i} isActive={i < mark}/>)}
        </MarkStyled>
      )}
    </RowStyled>
  )
}