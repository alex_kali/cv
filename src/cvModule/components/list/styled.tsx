import styled from "styled-components";

export const ListStyled = styled.div`

`

export const RowStyled = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`

export const NameStyled = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 9px;
`

export const MarkStyled = styled.div`
  display: flex;
  flex-direction: row;
  width: 60px;
  justify-content: space-between;
`

export const DotStyled = styled.div<{isActive: boolean}>`
  height: 6px;
  width: 6px;
  border-radius: 50%;
  border: 1px solid #414042;
  ${props => props.isActive ? `
    background: #414042;
  ` : ``}
`