import {FC} from "react";
import {IItem, Row} from "src/cvModule/components/list/row";
import {ListStyled} from "src/cvModule/components/list/styled";
import {ITitle, Title} from "src/cvModule/components/title";

interface IList {
  title: ITitle;
  items: Array<IItem>;
}

export const List:FC<IList> = ({title, items}) => {
  return (
    <ListStyled>
      <Title {...title} />
      {items.map((item) => <Row {...item}/>)}
    </ListStyled>
  )
}