import {FC} from "react";
import {RowStyled, TextStyled, WrapperIconStyled} from "src/cvModule/components/table/styled";

export interface IRow {
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  text: string;
}


export const Row:FC<IRow> = ({icon, text}) => {
  const Icon = icon as React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  return (
    <RowStyled>
      <WrapperIconStyled>
        <Icon />
      </WrapperIconStyled>
      <TextStyled>
        {text}
      </TextStyled>
    </RowStyled>
  )
}