import styled from "styled-components";

export const TableStyled = styled.div`

`

export const RowStyled = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: relative;
  border-bottom: 1px solid #414042;
  height: 19px;
`

export const WrapperIconStyled = styled.div`
  width: 34px;
  height: 100%;
  position: relative;
  text-align: center;
  &:after {
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    bottom: 0;
    margin: auto;
    height: 13px;
    width: 1px;
    background: #414042;
    opacity: 0.5;
  }
`

export const TextStyled = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 9px;
  
  &:before {
    content: "";
    display: inline-block;
    height: 100%;
    vertical-align: middle;
  }
`