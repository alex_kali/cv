import {FC} from "react";
import {IRow, Row} from "src/cvModule/components/table/row";
import {TableStyled} from "src/cvModule/components/table/styled";
import {ITitle, Title} from "src/cvModule/components/title";

interface ITable {
  title: ITitle;
  rows: Array<IRow>;
}

export const Table:FC<ITable> = ({title, rows}) => {
  
  return (
    <TableStyled>
      <Title {...title} />
      {rows.map((row) => <Row {...row}/>)}
    </TableStyled>
  )
}