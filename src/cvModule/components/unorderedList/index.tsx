import {FC} from "react";
import {ITitle, Title} from "src/cvModule/components/title";
import {
  DecorationStyled, DescriptionStyled,
  ItemStyled, PointStyled, SubtitleStyled, TitleStyled,
  UnorderedListStyled,
  WrapperItemStyled
} from "src/cvModule/components/unorderedList/styled";

interface IItem {
  title?: string;
  subtitle?: string;
  description?: string;
  points?: Array<string | JSX.Element>;
};

interface ITable {
  title: ITitle;
  items: Array<IItem>;
  isDecorated?: boolean;
}

export const UnorderedList:FC<ITable> = ({title, items, isDecorated}) => {
  return (
    <UnorderedListStyled>
      <Title {...title} />
      {items.map(({title, subtitle, description, points}) => (
        <WrapperItemStyled>
          {isDecorated && <DecorationStyled/>}
          <ItemStyled>
            {title && <TitleStyled>{title}</TitleStyled>}
            {subtitle && <SubtitleStyled>{subtitle}</SubtitleStyled>}
            {description && <DescriptionStyled>{description}</DescriptionStyled>}
            {points && points.map((point) => <PointStyled>{point}</PointStyled>)}
          </ItemStyled>
        </WrapperItemStyled>
      ))}
    </UnorderedListStyled>
  )
}