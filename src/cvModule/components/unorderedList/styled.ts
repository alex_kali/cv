import styled from "styled-components";

export const UnorderedListStyled = styled.div`
  overflow: hidden;
`

export const WrapperItemStyled = styled.div`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  
  & > div {
    padding-bottom: 15px;
  }

  &:nth-child(2) {
    margin-top: 10px;
  }
  
  &:last-child {
    & > div {
      padding-bottom: 0;
    }
  }
`

export const ItemStyled = styled.div`
  padding-bottom: 15px;
`

export const DecorationStyled = styled.div`
  top: 3px;
  width: 1px;
  background: #414042;
  margin-right: 15px;
  position: relative;
  margin-left: 2px;
  &:before {
    content: '';
    display: block;
    position: absolute;
    left: -2px;
    width: 5px;
    height: 5px;
    background: #414042;
    border-radius: 50%;
  }
`

export const TitleStyled = styled.div`
  font-weight: 700;
  font-size: 10px;
  line-height: 13px;
  letter-spacing: 0.02em;
`

export const SubtitleStyled = styled.div`
  margin-bottom: 3px;
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 9px;
`

export const DescriptionStyled = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 140%;
  color: #414042;

  opacity: 0.7;
`

export const PointStyled = styled.div`
  margin-top: 4px;
  
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 400;
  font-size: 8px;
  line-height: 9px;
  
  padding-left: 13px;
  position: relative;

  &:before {
    content: "\\A";
    width: 3px;
    height: 3px;
    border-radius: 50%;
    background: #414042;
    display: inline-block;
    vertical-align: middle;
    position: absolute;
    left: 5px;
    top: 3px;
  }
`