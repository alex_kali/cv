import React, {FC, memo, useEffect, useState, createContext} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {formDataParser} from "./formDataParser";
import {changeIsValidateForm, createForm} from "../../store/formController/slices";
import {useForm} from "./useForm";

export const formControllerContext = createContext({});

interface IWrapperForm {
  children: any;
  form: any;
  onChange?: any;
  initialValue?: any;
  isValidForm?: boolean;
  onSubmit?: any;
}


export const WrapperForm: FC<IWrapperForm> = memo((props) => {
  const dispatch = useDispatch()
  const formData:any = useSelector(props.form.formSelector)
  props.form.submitForm = props.onSubmit
  const [isValidate, changeIsValidate] = useState(props.isValidForm)

  if(!formData){
    dispatch(createForm({formName: props.form.formName,onSubmit: props.onSubmit, isValidForm: props.isValidForm}))
  }

  useEffect(()=> {
    if(isValidate !== formData?.isValidate){
      changeIsValidate(formData?.isValidate || false)
    }else{
      const [newData, newIsValidForm] = formDataParser(formData)
      if(newIsValidForm !== formData.isValidForm){
        dispatch(changeIsValidateForm({formName: props.form.formName, isValidForm: newIsValidForm}))
      }
      if(props.onChange){
        props.onChange(newData)
      }
    }
  }, [formData])

  return (
    <formControllerContext.Provider value={props.form || undefined}>
      {props.children}
    </formControllerContext.Provider>
);
});

interface IUpdatedFormData {
  formController?: any;
}
