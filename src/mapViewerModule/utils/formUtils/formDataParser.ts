export const formDataParser = (value:any) => {
  const data = {...value}
  delete data.isValidForm
  delete data.onSubmit
  const newData:any = {}
  let isValidate = true
  for(let i in data){
    newData[i] = data[i].data
    isValidate = data[i].isValidate && isValidate
  }
  return [newData, isValidate]
}