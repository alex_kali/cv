import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {
  changeDataField,
  changeIsErrorField,
  changeIsRequiredField,
  changeIsTouchField,
  changeIsValidateField,
  changeMessageErrorField,
  createField,
  createForm,
  formControllerDataSelector
} from "../../store/formController/slices";

interface IUseForm {
  name: string;
  onSubmit?: any;
  // onChange?: any;
  initialValue?: any;
  isValidForm?: boolean;
}

interface IUseField {
  name: string,
  initialValue?: any;
  isRequired?: boolean,
  isDisableAuto?: boolean,
  messageError?: string,
  isTouch?: boolean,
  isValidate?: boolean,
  validateFunction?(data: any): {isValidate: boolean, messageError?: string},
  formatterFunction?(data: any): {isValidate: boolean, messageError?: string},
}

export const useForm = (props:IUseForm) => {
  const formName = props.name
  const initialValue = props.initialValue
  const dispatch = useDispatch()

  const useField = (props: IUseField) => {
    useEffect(()=>{
      if(initialValue?.[props?.name]){
        props.initialValue = initialValue[props.name]
      }
      dispatch(createField({...props,...{formName: formName}}))
    }, [])

    return {
      selectorField: (state:any) => state.formController.formController[formName][props.name],


      useData: () => [
        useSelector((state:any) => state.formController?.formController[formName]?.[props.name]?.data) as any,
        (data: any) => {dispatch(changeDataField({name: props.name, data: data, formName: formName}))}
      ],
      useIsRequired: () => [
        useSelector((state:any) => state.formController.formController[formName][props.name]?.isRequired) as boolean,
        (data: boolean) => {dispatch(changeIsRequiredField({name: props.name, data: data, formName: formName}))}
      ],
      useMessageError: () => [
        useSelector((state:any) => state.formController.formController[formName][props.name]?.messageError) as string,
        (data: string) => {dispatch(changeMessageErrorField({name: props.name, data: data, formName: formName}))}
      ],
      useIsValidate: () => [
        useSelector((state:any) => state.formController.formController[formName][props.name]?.isValidate) as boolean,
        (data: boolean) => {dispatch(changeIsValidateField({name: props.name, data: data, formName: formName}))}
      ],
      useIsTouch: () => [
        useSelector((state:any) => state.formController.formController[formName][props.name]?.isTouch) as boolean,
        (data: boolean) => {dispatch(changeIsTouchField({name: props.name, data: data, formName: formName}))}
      ]
    }
  }
  return {
    useField: useField,
    submitForm: props.onSubmit,
    formSelector: (state:any) => state.formController.formController[formName],
    changeDataField: (fieldName: string, data: any) => {
      dispatch(changeDataField({name: fieldName, data: data, formName: formName}))
    },
    formName: formName,
  }
}
