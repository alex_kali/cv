import styled from "styled-components";
import InputMask from 'react-input-mask';

interface ICustomStyled {
  default?: string;
  defaultByProps?: (props: any | IProps) => string
  colorStyle?: {
    default?: string;
    silver?: string;
    retro?: string;
    dark?: string;
    night?: string;
    aubergine?: string;
    green?: string;
    blue?: string;
    red?: string;
  };
  screenExtension?: {
    largePc?: string;
    pc?: string,
    smallPc?: string,
    ipad?: string,
    mobile?: string,
  };
  screenOrientation?: {
    ipadPortrait?: string,
    ipadLandscape?: string,
    mobilePortrait?: string,
    mobileLandscape?: string,
  }
}

interface IProps {
  theme: {
    colorStyle: 'default' | 'silver' | 'retro' | 'dark' | 'night' | 'aubergine'| 'green' | 'blue' | 'red';
    screenExtension: 'largePc' | 'pc' | 'smallPc' | 'ipad' | 'mobile';
    screenOrientation: 'ipadPortrait' | 'ipadLandscape' | 'mobilePortrait' | 'mobileLandscape';
  }
}

export const customStyled = (styles:ICustomStyled) => {
  return styled.div`
    ${styles.default}
    ${(props:IProps)=> {
      return styles?.colorStyle?.[props?.theme?.colorStyle]
    }}
    ${(props:IProps)=> {
      return styles?.screenExtension?.[props?.theme?.screenExtension]
    }}
    ${(props:IProps)=> {
      return styles?.screenOrientation?.[props?.theme?.screenOrientation]
    }}
    ${styles.defaultByProps}
  ` as any
}

export const customInputStyled = (styles:ICustomStyled) => {
  return styled.input`
    ${styles.default}
    ${(props:IProps)=> {
      return styles?.colorStyle?.[props?.theme?.colorStyle]
    }}
    ${(props:IProps)=> {
      return styles?.screenExtension?.[props?.theme?.screenExtension]
    }}
    ${(props:IProps)=> {
      return styles?.screenOrientation?.[props?.theme?.screenOrientation]
    }} 
    ${styles.defaultByProps}
  `
}

export const customMaskInputStyled = (styles:ICustomStyled) => {
  return styled(InputMask)`
    ${styles.default}
    ${(props:IProps)=> {
      return styles?.colorStyle?.[props?.theme?.colorStyle]
    }}
    
    ${(props:IProps)=> {
      return styles?.screenExtension?.[props?.theme?.screenExtension]
    }}
    
    ${(props:IProps)=> {
      return styles?.screenOrientation?.[props?.theme?.screenOrientation]
    }}
    ${styles.defaultByProps}
  `
}
