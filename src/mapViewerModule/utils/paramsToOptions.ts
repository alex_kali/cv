export const paramsToOptions = (params: any) => {
  let result = ``
  for(let i in params){
    result += `${i}: "${params[i]}" ,`
  }
  return result
}