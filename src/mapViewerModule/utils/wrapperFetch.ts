export const wrapperFetch = async (query:string) => {
  // @ts-ignore
  return await fetch(process.env.REACT_APP_SERVER_API_URL + '/graphql/', {
    "headers": {
      "content-type": "application/json",
    },
    "body": JSON.stringify({query: query, variables: null}),
    "method": "POST",
    "mode": "cors",
    "credentials": "include"
  });
}