const colorStyleOptions:any = {
  default: {from:[0,0,0],to:[111,111,111],strokeColor: "rgb(100,100,100)",strokeWeight: 2},

  silver: {from:[50,50,50],to:[250,250,250],strokeColor: "rgb(50,50,50)",strokeWeight: 1},

  night: {from:[26, 37, 52],to:[111,111,111],strokeColor: "rgb(116, 104, 85)",strokeWeight: 2},

  aubergine: {from:[14, 22, 38],to:[176, 213, 206],strokeColor: "rgb(2,131,5)",strokeWeight: 2},

  dark: {from:[33,33,33],to:[100,100,100],strokeColor: "rgb(117,117,117)",strokeWeight: 2},

  retro: {from:[50,50,50],to:[219, 133, 85],strokeColor: "rgb(142, 195, 185)",strokeWeight: 2},

  green: {from:[26, 50, 15],to:[246, 247, 205],strokeColor: "rgb(51,105,21)",strokeWeight: 2},

  blue: {from:[40, 51, 71],to:[244, 247, 252],strokeColor: "rgb(54,66,87)",strokeWeight: 2},

  red: {from:[30,17,17],to:[243, 244, 245],strokeColor: "rgb(66,27,27)",strokeWeight: 2},
}

export const changePolygonColor = (polygon: any,data: string,averageData: string, colorStyle: string, isActive?: boolean) => {
  if(!isActive){


    const from = colorStyleOptions[colorStyle].from
    const to = colorStyleOptions[colorStyle].to
    const min = +averageData / 2
    const max = +averageData / 2 * 3
    let newColor:any;
    if(+data > max){
      newColor = to
    }else if (+data < min){
      newColor = from
    }else{
      newColor = []
      const calc = (+data-min)/(max-min) * 100
      for(let i in from){
        const color = ((to[i] - from[i]) / 100 * calc) + from[i]
        newColor.push(Math.round(color))
      }
    }

    polygon.setOptions({
      fillColor: `rgba(${newColor[0]},${newColor[1]},${newColor[2]},1)`,
      strokeColor: colorStyleOptions[colorStyle].strokeColor,
      strokeOpacity: 0.8,
      strokeWeight: colorStyleOptions[colorStyle].strokeWeight,
      fillOpacity: 0.4,
    })
  }
}