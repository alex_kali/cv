export const onlyNumber = (value:string) => {
  return parseInt(value.replace(/\D+/g,""))
}