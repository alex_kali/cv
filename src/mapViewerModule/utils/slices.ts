import {useDispatch, useSelector} from "react-redux";
import {AppThunk} from "../store/store";
import {wrapperFetch} from "./wrapperFetch";

export interface ICreateDefaultData {
  data?: any;
  getDataFunction?: any;
  params?: any;
  isLoading?: boolean;
  isError?: boolean;
  optionalFunction?: Object;
}

export interface IQuery {
  query: string;
  model: string;
  type: string;
}

export const createDefaultData = (props:ICreateDefaultData={}) => {
  return {
    data: props.data,
    isLoading: false,
    isError: false,
    getDataFunction: props.getDataFunction,
    params: props.params || {},
    optionalFunction: props.optionalFunction
  }
}

export const useCustomSelector = (selector:any, isGetData=false, params:any={}) => {
  const data:ICreateDefaultData = useSelector(selector)
  const dispatch = useDispatch()
  if(!data.isLoading && !data.isError) {
    if (isGetData && !data.data) {
      dispatch(data.getDataFunction(params, data))
    } else if (data.data && data.params !== undefined && JSON.stringify(params) !== JSON.stringify(data.params)) {
      console.log('данные есть, но необходимо получить данные с другими параметрами')
    }
  }
  return data.data
}

export const useDataController = (selector:any) => {
  const dispatch = useDispatch()
  const data:any = {...useSelector(selector)}

  for(let i in data.optionalFunction){
    data[i] = (params: any, formName:string | undefined = undefined) => {
      dispatch(data.optionalFunction[i](params, data, formName))
    }
  }

  return data
}


const requestHub:any = {
  outstandingRequests: {
    queryArray: {},
    mutationArray: {}
  },
  isHaveTimer: false,
}

export const requestController = (query:IQuery, data: ICreateDefaultData, action:any, serializer:any = undefined,errorHandler:any = undefined): AppThunk => async (
  dispatch: any
) => {
  const newData = {...data}
  newData.isLoading = true
  dispatch(action(newData))

  if(query.type === 'query'){
    requestHub.outstandingRequests.queryArray[query.model] = {
      query: query,
      data: data,
      action: action,
      serializer: serializer,
      errorHandler: errorHandler,
    }
  }else if(query.type === 'mutation'){
    requestHub.outstandingRequests.mutationArray[query.model] = {
      query: query,
      data: data,
      action: action,
      serializer: serializer,
      errorHandler: errorHandler,
    }
  }

  if(!requestHub.isHaveTimer){
    requestHub.isHaveTimer = true
    setTimeout(()=>{
      requestHub.isHaveTimer = false
      makeRequest(requestHub.outstandingRequests.queryArray, requestHub.outstandingRequests.mutationArray,dispatch)
      requestHub.outstandingRequests.queryArray = {}
      requestHub.outstandingRequests.mutationArray = {}
    }, 10);
  }
}

const makeRequest = (queryArray:any, mutationArray:any, dispatch: any) => {
  let query = ''
  if(Object.keys(queryArray).length){
    query = `query { `
    for(let i in queryArray){
      query += queryArray[i].query.model + ' '
      query += queryArray[i].query.query + ' '
    }
    query += ' }'
  }

  let mutation = ''
  if(Object.keys(mutationArray).length){
    mutation = `mutation { `
    for(let i in mutationArray){
      mutation += mutationArray[i].query.model + ' '
      mutation += mutationArray[i].query.query + ' '
    }
    mutation += ' }'
  }
  query = query + ' ' + mutation

  const unionArray = {...queryArray, ...mutationArray}

  wrapperFetch(query)
    .then((response) => {
      if(response.status === 200){
        return response.json();
      }else{
        for(let i in unionArray){
          const newData = {...unionArray[i].data}
          newData.isLoading = false
          newData.isError = true
          dispatch(unionArray[i].action(newData))
        }
      }
    })
    .then((data) => {
      for(let i in unionArray){
        if(!data.data[i]?.errors) {
          const newData = {...unionArray[i].data}
          newData.isLoading = false
          newData.isError = false
          if (unionArray[i].serializer) {
            newData.data = unionArray[i].serializer(data.data[i])
          } else {
            newData.data = data.data[i]
          }
          dispatch(unionArray[i].action(newData))
        }else{
          const newData = {...unionArray[i].data}
          newData.isLoading = false
          newData.isError = false
          dispatch(unionArray[i].action(newData))
          if(unionArray[i].errorHandler){
            unionArray[i].errorHandler(data.data[i].errors)
          }
        }
      }
    });

}