import {stringMin} from "./stringMin";
import {stringMax} from "./stringMax";
import {stringEmail} from "./stringEmail";
import {numberMin} from "./numberMin";
import {numberMax} from "./numberMax";
import {onlyNumber} from "./formatterOnlyNumber";
import {formatterPrice} from "./formatterPrice";

export interface IValidate {
  data: any;
  messageError: string;
  isValidate: boolean;

  string: () => IStringValidate;
  number: () => INumberValidate;
}

export interface IStringValidate {
  data: any;
  messageError: string;
  isValidate: boolean;

  min: (value: any, messageError?: string) => IStringValidate;
  max: (value: any, messageError?: string) => IStringValidate;
  email: (messageError?: string) => IStringValidate;
}

export interface INumberValidate {
  data: any;
  messageError: string;
  isValidate: boolean;

  min: (value: any, messageError?: string) => INumberValidate;
  max: (value: any, messageError?: string) => INumberValidate;
}

export interface IFormatter {
  data: any;

  string: () => IStringFormatter;
}

export interface IStringFormatter {
  data: any;

  onlyNumber: () => IStringFormatter;
  price: () => IStringFormatter;
}

export function yap(data:any){
  const validate:IValidate = {
    data: data,
    messageError: '',
    isValidate: true,
    string: function(this: any) {

      this.min = stringMin
      this.max = stringMax
      this.email = stringEmail
      return this as IStringValidate
    },
    number: function(this: any) {

      this.min = numberMin
      this.max = numberMax
      return this as INumberValidate
    },
  }

  const formatter:IFormatter = {
    data: data,
    string: function(this: any) {

      this.onlyNumber = onlyNumber
      this.price = formatterPrice
      return this as IStringFormatter
    }
  }

  return {
    validate: validate,
    formatter: formatter
  }
}