/* eslint-disable @typescript-eslint/indent */
import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import mapSliceReducer from './map/slices';
import formControllerReducer from './formController/slices';
import styleControllerReducer from './styleController/slices';
import authReducer from './auth/slices';


export const store = configureStore({
  reducer: {
    map: mapSliceReducer,
    formController: formControllerReducer,
    styleController: styleControllerReducer,
    auth: authReducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
  >;