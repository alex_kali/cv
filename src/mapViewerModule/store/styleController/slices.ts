import {createSlice, current} from "@reduxjs/toolkit";
import {RootState} from "../store";

//screens largePc , pc , smallPc ,ipad, mobile
//orientation ipadPortrait , ipadLandscape , mobilePortrait , mobileLandscape , none

export const initialState: any = {
  colorStyle: 'default',
  screenExtension: '',
  screenOrientation: '',
};



export const styleControllerSlice = createSlice({
  name: 'styleController',
  initialState,
  reducers: {
    setColorStyle: (state, action) => {
      state.colorStyle = action.payload;
    },
    setScreenExtension: (state, action) => {
      state.screenExtension = action.payload;
    },
    setScreenOrientation: (state, action) => {
      state.screenOrientation = action.payload;
    },
  },
});

export const { setColorStyle, setScreenExtension, setScreenOrientation } = styleControllerSlice.actions;

export const colorStyleSelector = (state: RootState) => state.styleController.colorStyle;

export const screenExtensionSelector = (state: RootState) => state.styleController.screenExtension;

export const screenOrientationSelector = (state: RootState) => state.styleController.screenOrientation;


export default styleControllerSlice.reducer;