import { RootState } from '../store';

export const mapDataSelector = (state: RootState) => state.map.mapData;

export const optionsMapSelector = (state: RootState) => state.map.optionsMap;

export const optionsAverageDynamicsSelector = (state: RootState) => state.map.optionsAverageDynamics;