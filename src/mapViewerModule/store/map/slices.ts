import { createSlice } from '@reduxjs/toolkit';
import {createDefaultData} from "../../utils/slices";
import {getMapData, getOptionsAverageDynamics, getOptionsMap} from "./thunks";

export const initialState: any = {
  mapData: createDefaultData({getDataFunction: getMapData}),
  optionsMap: createDefaultData({getDataFunction: getOptionsMap}),
  optionsAverageDynamics: createDefaultData({getDataFunction: getOptionsAverageDynamics}),
};

export const mapSlice = createSlice({
  name: 'map',
  initialState,
  reducers: {
    setMapData: (state, action) => {
      state.mapData = action.payload;
    },
    setOptionsMap: (state, action) => {
      state.optionsMap = action.payload;
    },
    setOptionsAverageDynamics: (state, action) => {
      state.optionsAverageDynamics = action.payload;
    },
  },
});

export const { setMapData, setOptionsMap, setOptionsAverageDynamics } = mapSlice.actions;



export default mapSlice.reducer;