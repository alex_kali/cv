export const getMapDataQuery =  {
  query: `
    {
      name
      coords
      regiondataSet{
        name
        year
        isAbsolute
        data
      }
      averagedynamicsSet{
        name
        data
      }
    }
  `,
  model: 'regions',
  type: 'query',
}

export const getOptionsMapQuery =  {
  query: `
  {
    name
    year
    data
    min
    max
  }
  `,
  model: 'regionsDataOptions',
  type: 'query',
}

export const getOptionsAverageDynamicsQuery =  {
  query: `
  {
    name
    min
    max
    data
  }
  `,
  model: 'averageDynamicsOptions',
  type: 'query',
}