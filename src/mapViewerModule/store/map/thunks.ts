import { AppThunk } from '../store';
import {ICreateDefaultData, requestController} from "../../utils/slices";
import {setMapData, setOptionsAverageDynamics, setOptionsMap} from "./slices";
import {getMapDataQuery, getOptionsAverageDynamicsQuery, getOptionsMapQuery} from "./query";

export const getMapData = (data: ICreateDefaultData): AppThunk => async (
  dispatch: any
) => {
  const query = getMapDataQuery
  const serialize = (data: any) => {
    for (let i of data) {
      let newRegionDataSet: any = {}

      for(let k in i.regiondataSet){
        if(!newRegionDataSet[i.regiondataSet[k].name]){
          newRegionDataSet[i.regiondataSet[k].name] = {}
        }
        if(!newRegionDataSet[i.regiondataSet[k].name][i.regiondataSet[k].year]){
          newRegionDataSet[i.regiondataSet[k].name][i.regiondataSet[k].year] = {}
        }
        newRegionDataSet[i.regiondataSet[k].name][i.regiondataSet[k].year][i.regiondataSet[k].isAbsolute] = i.regiondataSet[k].data
      }
      i.regiondataSet = newRegionDataSet

      let newAverageDynamicsSet: any = {}
      for(let k in i.averagedynamicsSet){
        newAverageDynamicsSet[i.averagedynamicsSet[k].name] = i.averagedynamicsSet[k].data
      }
      i.averagedynamicsSet = newAverageDynamicsSet
    }
    return data
  }
  dispatch(requestController(query, data, setMapData, serialize))
};

export const getOptionsMap = (data: ICreateDefaultData): AppThunk => async (
  dispatch: any
) => {
  const query = getOptionsMapQuery
  const serialize = (data: any) => {
    let newData:any = {}
    for(let i in data){
      if(!newData[data[i].name]){
        newData[data[i].name] = {}
      }
      if(!newData[data[i].name][data[i].year]){
        newData[data[i].name][data[i].year] = {}
      }
      newData[data[i].name][data[i].year].min = data[i].min
      newData[data[i].name][data[i].year].max = data[i].max
      newData[data[i].name][data[i].year].data = data[i].data
    }
    return newData
  }
  dispatch(requestController(query, data, setOptionsMap, serialize))
};


export const getOptionsAverageDynamics = (data: ICreateDefaultData): AppThunk => async (
  dispatch: any
) => {
  const query = getOptionsAverageDynamicsQuery
  const serialize = (data: any) => {
    let newData:any = {}
    for(let i in data){
      newData[data[i].name] = {}
      newData[data[i].name].min = data[i].min
      newData[data[i].name].max = data[i].max
      newData[data[i].name].data = data[i].data
    }
    return newData
  }
  dispatch(requestController(query, data, setOptionsAverageDynamics, serialize))
};