export const userDataQuery =  {
  query: `
    {
      firstName
      lastName
      username
      email
    }
  `,
  model: 'me',
  type: 'query',
}