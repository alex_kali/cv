import {RootState} from "../store";

export const userDataSelector = (state: RootState) => state.auth.userData;

export const tokenSelector = (state: RootState) => state.auth.token;

export const isViewSettingsSelector = (state: RootState) => state.auth.isViewSetting;