import {createSlice, current} from "@reduxjs/toolkit";
import {RootState} from "../store";
import {createDefaultData} from "../../utils/slices";
import {getUserData, loginUser, registrationUser} from "./thunks";

//screens largePc , pc , smallPc ,ipad, mobile
//orientation ipadPortrait , ipadLandscape , mobilePortrait , mobileLandscape , none

export const initialState: any = {
  userData: createDefaultData({getDataFunction: getUserData, optionalFunction: {login: loginUser, registration: registrationUser}}),
  token: undefined,
  isViewSetting: false,
};



export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.userData = action.payload;
    },
    setUserToken: (state, action) => {
      state.token = action.payload;
    },
    setUserData: (state, action) => {
      state.userData.data = action.payload;
    },
    setIsViewSetting: (state, action) => {
      state.isViewSetting = action.payload;
    },
  },
});

export const { setUser, setUserToken, setUserData, setIsViewSetting } = authSlice.actions;


export default authSlice.reducer;