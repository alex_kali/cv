import {paramsToOptions} from "../../utils/paramsToOptions";

export interface IGetUserDataMutation {
  username: string;
  password: string;
}

export const getUserDataMutation = (params:IGetUserDataMutation) => {
  return {
    query: `
    (${paramsToOptions(params)}) {
      success,
      errors,
      unarchiving,
      token,
      user {
        id,
        username,
        email,
      }
    }
  `,
    model: 'tokenAuth',
    type: 'mutation',
  }
}

export interface IRegistrationUserMutation {
  username: string;
  email: string;
  password1: string;
  password2: string;
}

export const registrationUserMutation = (params:IRegistrationUserMutation) => {
  return {
    query: `
    (${paramsToOptions(params)}) {
      success,
      errors,
      token,
    }
  `,
    model: 'register',
    type: 'mutation',
  }
}