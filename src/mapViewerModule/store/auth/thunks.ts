import {ICreateDefaultData, requestController} from "../../utils/slices";
import {AppThunk} from "../store";
import {userDataQuery} from "./query";
import {setMapData} from "../map/slices";
import {setUser, setUserData, setUserToken} from "./slices";
import {
  getUserDataMutation,
  IGetUserDataMutation,
  IRegistrationUserMutation,
  registrationUserMutation
} from "./mutation";
import {errorsToForm} from "../formController/slices";
import {wrapperFetch} from "../../utils/wrapperFetch";


export const getUserData = (data: ICreateDefaultData): AppThunk => async (
  dispatch: any
) => {
  const query = userDataQuery
  dispatch(requestController(query, data, setUser))
};


export const loginUser = (params: IGetUserDataMutation, data: ICreateDefaultData, formName:string | undefined): AppThunk => async (
  dispatch: any
) => {
  const query = getUserDataMutation(params)
  // dispatch(requestController(query, data, getUserDataMutation,undefined,errorHandler))
  const mutation = `mutation { ${query.model} ${query.query} }`
  const response = await wrapperFetch(mutation)
  const data = await response.json()
  if(data.data?.tokenAuth?.token){
    dispatch(setUserToken(data.data.tokenAuth.token))
    dispatch(setUserData(data.data.tokenAuth.user))
  }else{

  }
};


export const registrationUser = (params: IRegistrationUserMutation, data: ICreateDefaultData, formName:string): AppThunk => async (
  dispatch: any
) => {
  const query = registrationUserMutation(params)
  const errorHandler = (errors:any) => {
    dispatch(errorsToForm({errors,formName}))
  }
  dispatch(requestController(query, data, getUserDataMutation,undefined,errorHandler))
};