import {createSlice, current} from "@reduxjs/toolkit";
import {useSelector} from "react-redux";

export const initialState: any = {
  formController: {},
};

export const formControllerSlice = createSlice({
  name: 'formController',
  initialState,
  reducers: {
    errorsToForm: (state, action) => {
      for(let i in action.payload.errors){
        state.formController[action.payload.formName][i].isValidate = false
        state.formController[action.payload.formName][i].messageError = action.payload.errors[i][0].message
      }
    },
    createForm: (state, action) => {
      if(!state.formController[action.payload.formName]){
        state.formController[action.payload.formName] = {}
        state.formController[action.payload.formName].onSubmit = action.payload.onSubmit
        state.formController[action.payload.formName].isValidForm = action.payload.isValidForm
      }
    },
    createField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name] = {
        data: action.payload.initialValue,
        isRequired: action.payload.isRequired,
        isDisableAuto: action.payload.isDisableAuto,
        messageError: action.payload.messageError,
        isTouch: action.payload.isTouch,
        isValidate: action.payload.isValidate || !action.payload.isRequired,
        validateFunction: action.payload.validateFunction,
        formatterFunction: action.payload.formatter,
      }
    },
    changeDataField: (state, action) => {
      let data = action.payload.data
      const field = current(state.formController[action.payload.formName]?.[action.payload.name])
      if(!field.isDisableAuto) {
        state.formController[action.payload.formName][action.payload.name].isTouch = true;
        if (field.validateFunction) {
          const {isValidate, messageError} = field.validateFunction(data)
          if (!isValidate) {
            state.formController[action.payload.formName][action.payload.name].isValidate = false
            if (messageError) {
              state.formController[action.payload.formName][action.payload.name].messageError = messageError
            }
          } else {
            state.formController[action.payload.formName][action.payload.name].isValidate = true
            state.formController[action.payload.formName][action.payload.name].messageError = ''
          }
        } else if (field.isRequired && !data) {
          state.formController[action.payload.formName][action.payload.name].isValidate = false
          if (!field.messageError) {
            state.formController[action.payload.formName][action.payload.name].messageError = "Данное поле обязательно!"
          }
        } else {
          state.formController[action.payload.formName][action.payload.name].isValidate = true
        }
        if (field.formatterFunction) {
          data = field.formatterFunction(data)
        }
      }
      state.formController[action.payload.formName][action.payload.name].data = data;
    },
    changeIsRequiredField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name].isRequired = action.payload.isRequired;
    },
    changeIsErrorField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name].isError = action.payload.isError;
    },
    changeMessageErrorField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name].messageError = action.payload.messageError;
    },
    changeIsTouchField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name].isTouch = action.payload.isTouch;
    },
    changeIsValidateField: (state, action) => {
      state.formController[action.payload.formName][action.payload.name].isValidate = action.payload.isValidate;
    },
    changeIsValidateForm: (state, action) => {
      state.formController[action.payload.formName].isValidForm = action.payload.isValidForm;
    },
  },
});

export const { errorsToForm, createForm, createField, changeDataField, changeIsRequiredField, changeIsErrorField, changeMessageErrorField, changeIsTouchField, changeIsValidateField, changeIsValidateForm } = formControllerSlice.actions;

export const formControllerDataSelector = (state: any) => state.formController;

interface IUseFieldSelector {
  formName: string;
  fieldName: string;
}

export const useFieldSelector = (props:IUseFieldSelector) => {
  return useSelector((state: any) => state.formController.formController?.[props.formName]?.[props.fieldName]?.data)
}

export default formControllerSlice.reducer;