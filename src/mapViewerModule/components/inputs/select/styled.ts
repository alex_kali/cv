import styled from "styled-components";
import {customStyled} from "../../../utils/customStyled";
import {
  BORDER_INPUT,
  BORDER_RADIUS_INPUT,
  COLOR_INPUT,
  HEIGHT_INPUT,
  MARGIN_INPUT,
  PADDING_INPUT
} from "../stylesConstant";

export const WrapperSelectStyled = customStyled({
  default: `
    width: calc(100% - 40px);
    height: ${HEIGHT_INPUT};
    display: flex;
    align-items: center;
    padding-left: ${PADDING_INPUT};
    padding-right: ${PADDING_INPUT};
    margin-top: ${MARGIN_INPUT};
    margin-bottom: ${MARGIN_INPUT};
  `
})

export const TitleStyled = customStyled({
  default: `
    font-size: 16px;
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const ValueStyled = customStyled({
  default: `
    height: 100%;
    border: ${BORDER_INPUT}; 
    flex-grow: 3;
    margin-left: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    position: relative;
    background: white;
    cursor: pointer;
    border-radius:${BORDER_RADIUS_INPUT};
  `,
  defaultByProps: props =>`
    ${props.isView && `
      border-radius: 20px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
    `}
  `,
  colorStyle: {
    retro: `
      background: #f5f1e6;
      border-color: #523735;
      color: #523735;
    `,
    dark: `
      border-color: rgb(117,117,117);
      background: #2c2c2c;
      color: rgb(170,170,170);
    `,
    night: `
      border-color: #d59563;
      background: #2f3948;
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
      background-color: rgb(13,13,20);
      border-color: rgba(2,131,5,0.4);
    `,
    green: `
      color: #2b4a4a;
      background-color: #dae4e5;
      border-color: #2b4a4a;
    `,
    blue: `
      color: #283347;
      border-color: #283347;
      background: #dfe7f2;
    `
  }
})

export const OptionsStyled = customStyled({
  default: `
    border: ${BORDER_INPUT};
    border-top: none;
    position: absolute;
    width: 100%;
    left: -2px;
    top: 100%;
    background: white;
    border-bottom-left-radius: 20px;
    border-bottom-right-radius: 20px;
    overflow: hidden;
  `,
  colorStyle: {
    retro: `
      background: #f5f1e6;
      border-color: #523735;
    `,
    dark: `
      border-color: rgb(117,117,117);
      background: #2c2c2c;
    `,
    night: `
      border-color: #d59563;
      background: #2f3948;
    `,
    aubergine: `
      background-color: rgb(13,13,20);
      border-color: rgba(2,131,5,0.4);
    `,
    green: `
      background-color: #dae4e5;
      border-color: #2b4a4a;
    `,
    blue: `
      border-color: #283347;
      background: #dfe7f2;
    `
  }
})

export const OptionsItemStyled = customStyled({
  default: `
    height: 30px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-top: 1px solid ${COLOR_INPUT};
    &:hover{
      background: rgba(0,0,0,0.1);
    }
  `,
  defaultByProps: props => `
    ${props.checked && `
      background: rgba(0,0,0,0.1);
      
      ${(props.theme.colorStyle === 'retro' && `
        background: rgba(235,227,205,0.7);
      `) || ''}
      
      ${(props.theme.colorStyle === 'dark' && `
        background: rgba(117,117,117,0.3);
      `) || ''}
      
      ${(props.theme.colorStyle === 'aubergine' && `
        background: rgba(2,131,5,0.4);
      `) || ''}
    `}
  `,
  colorStyle: {
    retro: `
      color: #523735;
      border-color: #523735;
      &:hover{
        background: rgba(235,227,205,0.7);
      }
    `,
    dark: `
      color: rgb(170,170,170);
      border-color: rgb(117,117,117);
      &:hover{
        background: rgba(117,117,117,0.3);
      }
    `,
    aubergine: `
      color: rgb(2,131,5);
      border-color: rgba(2,131,5,0.4);
      &:hover{
        background: rgba(2,131,5,0.4);
      }
    `,
    green: `
      color: #2b4a4a;
      border-color: #2b4a4a;
    `,
    blue: `
      color: #283347;
      border-color: #283347;
    `
  }
})

