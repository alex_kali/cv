import React, { FC, memo, useContext} from 'react';
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {WrapperPasswordInputStyled} from "./styled";
import {yap} from "../../../utils/yap";
import {ErrorTitleStyled} from "../../titles/errorTitle";

interface IPassword {
  name: string;
  required?: boolean;
}

export const PasswordInput: FC<IPassword> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData, useIsValidate, useIsTouch, useMessageError} = formController.useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => yap(value).validate.string().min(8).max(20),
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <WrapperPasswordInputStyled
        type={'password'}
        value={data}
        onChange={(e:any)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
