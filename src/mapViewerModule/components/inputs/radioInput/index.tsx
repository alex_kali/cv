import React, {FC, memo, useContext, useState} from 'react';
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {
  CustomCheckStyled,
  CustomRadioStyled,
  RadioInputStyled,
  RadioTextStyled,
  WrapperRadioInputStyled,
  WrapperRadioItemStyled
} from "./styled";

interface IRadioInput {
  name: string;
  data: Array<{text: string, value: string}>
  required?: boolean;
  initialValue?: string
}

export const RadioInput: FC<IRadioInput> = (props) => {
  const formController:any = useContext(formControllerContext)

  const {useData} = formController.useField({
    name: props.name,
    isRequired: props.required,
    initialValue: props.initialValue,
  })
  
  const [data, changeData] = useData()

  return (
    <WrapperRadioInputStyled>
      {props.data.map((i)=>
        <label htmlFor={i.value} key={i.value}>
          <WrapperRadioItemStyled checked={data === i.value}>
            <RadioTextStyled>{i.text}</RadioTextStyled>
            <RadioInputStyled checked={data === i.value} id={i.value} type="radio" name={props.name} value={i.value} onChange={(e)=>{changeData(e.target.value)}}/>
            <CustomRadioStyled className={'radio'}><CustomCheckStyled /></CustomRadioStyled>
          </WrapperRadioItemStyled>
        </label>
      )}
    </WrapperRadioInputStyled>
  )
};