import {customInputStyled, customStyled} from "../../../utils/customStyled";

export const WrapperRadioInputStyled = customStyled({
  default: `
    
  `
})

export const RadioInputStyled = customInputStyled({
  default: `
    float: left;
    display: none;
    margin-left: 15px;
    margin-top: 5px;
  `
})

export const WrapperRadioItemStyled = customStyled({
  default: `
    width: 400px;
    max-width: 100%;
    clear: both;
    margin-top: 4px;
    margin-bottom: 4px;
    transition-duration: 0.2s;
    cursor: pointer;
    input:checked + .radio{
      & > *{
        transform: scale(1);
      }
    }
  `,
  defaultByProps: props => `
    ${props.checked ? `
      opacity: 1;
    ` : `
      opacity: 0.4;
    `}
  `
})

export const RadioTextStyled = customStyled({
  default: `
    display: inline-block;
    margin-left: 10px;
    font-size: 16px;
  `,
  colorStyle:{
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const CustomRadioStyled = customStyled({
  default: `
    width: 14px;
    height: 14px;
    float: left;
    border: 1px solid grey;
    margin-left: 15px;
    border-radius: 14px;
    display: flex;
    align-items: center;
    justify-content: center;
  `,
  colorStyle: {
    dark: `
      border-color: rgb(170,170,170);
    `,
    night: `
      border-color: #d59563;
    `,
    aubergine: `
      border-color: rgb(2,131,5);
    `,
    green: `
      border-color: #2b4a4a;
    `,
    blue: `
      border-color: #283347;
    `
  }
})

export const CustomCheckStyled = customStyled({
  default: `
    height: 8px;
    width: 8px;
    border-radius: 6px;
    background: grey;
    transition-duration: 0.2s;
    transform: scale(0);
  `,
  colorStyle:{
    retro: `
      background: #523735;
    `,
    dark: `
      background: rgb(170,170,170)
    `,
    night: `
      background: #d59563;
    `,
    aubergine: `
      background: rgb(2,131,5);
    `,
    green: `
      background: #2b4a4a;
    `,
    blue: `
      background: #283347;
    `
  }
})