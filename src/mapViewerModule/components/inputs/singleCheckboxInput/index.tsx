import React, {FC, memo, useContext, useState} from 'react';
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {
  CheckboxInputStyled,
  CheckboxTextStyled,
  CustomCheckStyled, CustomRadioStyled,
  WrapperCheckboxInputStyled,
  WrapperCheckboxItemStyled
} from "./styled";

interface ISingleCheckboxInput {
  name: string;
  text: string;
  required?: boolean;
}

export const SingleCheckboxInput: FC<ISingleCheckboxInput> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData} = formController.useField({
    name: props.name,
    isRequired: props.required,
    initialValue: false,
  })
  const [data, changeData]: [boolean | undefined, (data: boolean | undefined) => undefined] = useData()

  const onChange = () => {
    changeData(!data)
  }

  return (
    <WrapperCheckboxInputStyled>
        <label htmlFor={props.name}>
          <WrapperCheckboxItemStyled checked={data}>
            <CheckboxTextStyled>{props.text}</CheckboxTextStyled>
            <CheckboxInputStyled checked={data} id={props.name} type="checkbox" name={props.name} onChange={onChange}/>
            <CustomRadioStyled className={'checkbox'}><CustomCheckStyled /></CustomRadioStyled>
          </WrapperCheckboxItemStyled>
        </label>
    </WrapperCheckboxInputStyled>
  )
};