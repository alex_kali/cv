import {customInputStyled, customStyled} from "../../../utils/customStyled";
import styled from "styled-components";
import {ReactComponent as check} from "./icons/check.svg";

export const WrapperCheckboxInputStyled = customStyled({
  default: `
    
  `
})

export const CheckboxInputStyled = customInputStyled({
  default: `
    display: none;
  `
})

export const WrapperCheckboxItemStyled = customStyled({
  default: `
    display: flex;
    justify-content: space-between;
    flex-direction: row-reverse;
    align-items: center;
    width: 400px;
    max-width: 100%;
    clear: both;
    margin-top: 4px;
    margin-bottom: 4px;
    transition-duration: 0.1s;
    cursor: pointer;
    input:checked ~ .checkbox{
      & > *{
        height: 14px;
      }
    }
  `,
  defaultByProps: props => `
    ${props.checked ? `
        opacity: 1;
      ` : `
        opacity: 0.4;
      `}
    `
})

export const CheckboxTextStyled = customStyled({
  default: `
    margin-left: 15px;
    width: calc(100% - 50px);
    font-size: 16px;
  `,
  colorStyle:{
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const CustomRadioStyled = customStyled({
  default: `
    width: 14px;
    height: 14px;
    float: left;
    border: 2px solid grey;
    margin-left: 15px;
  `,
  colorStyle:{
    retro: `
      border-color: #523735;
    `,
    dark: `
      border-color: rgb(170,170,170);
    `,
    night: `
      border-color: #d59563;
    `,
    aubergine: `
      border-color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})


export const CustomCheckStyled = styled(check)`
  width: 14px;
  height: 0px;
  transition-duration: 0.1s;
  ${props=>props.theme.colorStyle === 'retro' && `
    & path, text{
      fill: #523735 !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'dark' && `
    & path, text{
      fill: rgb(170,170,170) !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'night' && `
    & path, text{
      fill: #d59563 !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'aubergine' && `
    & path, text{
      fill: rgb(2,131,5) !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'green' && `
    & path, text{
      fill: #2b4a4a !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'blue' && `
    & path, text{
      fill: #283347 !important; 
    }
  `}
`