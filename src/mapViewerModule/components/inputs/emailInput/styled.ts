import styled from "styled-components";
import {customInputStyled, customMaskInputStyled} from "../../../utils/customStyled";
import {
  BORDER_INPUT,
  BORDER_RADIUS_INPUT,
  COLOR_INPUT,
  ERROR_BORDER_COLOR_INPUT,
  ERROR_COLOR_INPUT,
  FONT_SIZE_INPUT,
  HEIGHT_INPUT,
  MARGIN_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT,
  PADDING_INPUT,
  WIDTH_INPUT
} from "../stylesConstant";

export const CustomEmailInputStyled:any = customMaskInputStyled({
  default: `
    background: none;
    margin-top: ${MARGIN_INPUT};
    margin-bottom: ${MARGIN_INPUT};
    width: ${WIDTH_INPUT};
    border: ${BORDER_INPUT};
    border-radius: ${BORDER_RADIUS_INPUT};
    height: ${HEIGHT_INPUT};
    font-size: ${FONT_SIZE_INPUT};
    padding-left: ${PADDING_INPUT};
    padding-right: ${PADDING_INPUT};
    color: ${COLOR_INPUT};
    opacity: ${OPACITY_INPUT};
    &:focus{
      outline: none;
      opacity: ${OPACITY_FOCUS_INPUT};
    }
  `,
  defaultByProps: props => `
    color: ${props.isValidate ? '' : ERROR_COLOR_INPUT} !important;
    border-color: ${props.isValidate ? '' : ERROR_BORDER_COLOR_INPUT} !important;
    margin-bottom: ${props.isValidate ? '' : '4px'} !important;
    &:focus{
      color: ${props.isValidate ? '' : ERROR_COLOR_INPUT} !important;
      border-color: ${props.isValidate ? '' : ERROR_BORDER_COLOR_INPUT} !important;
    }
  `,
  colorStyle: {
    retro: `
      color: #523735;
      border-color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
      border-color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
      border-color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
      border-color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
      border-color: #2b4a4a;
    `,
    blue: `
      color: #283347;
      border-color: #283347;
    `
  }
})