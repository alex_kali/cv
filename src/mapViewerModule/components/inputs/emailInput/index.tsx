import React, { FC, memo, useContext} from 'react';
import {CustomEmailInputStyled} from "./styled";
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {yap} from "../../../utils/yap";
import {ErrorTitleStyled} from "../../titles/errorTitle";

interface IEmail {
  name: string;
  required?: boolean;
}

export const EmailInput: FC<IEmail> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData, useIsValidate, useIsTouch, useMessageError} = formController.useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => yap(value).validate.string().max(30).email(),
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <CustomEmailInputStyled
        type={"email"} value={data}
        onChange={(e:any)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};