import React, { FC, memo, useContext} from 'react';
import {CustomInputStyled} from "./styles";
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {ErrorTitleStyled} from "../../titles/errorTitle";

interface IText {
  name: string;
  required?: boolean;
}

export const TextInput: FC<IText> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData, useIsValidate, useIsTouch, useMessageError} = formController.useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <CustomInputStyled
        value={data}
        onChange={(e:any)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
