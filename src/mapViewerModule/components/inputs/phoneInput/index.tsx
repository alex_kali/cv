import React, { FC, memo, useContext} from 'react';
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {CustomPhoneInputStyled} from "../phoneInput/styled";
import {yap} from "../../../utils/yap";
import {ErrorTitleStyled} from "../../titles/errorTitle";

interface IPhone {
  name: string;
  required?: boolean;
}

export const PhoneInput: FC<IPhone> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData, useIsValidate, useIsTouch, useMessageError} = formController.useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => yap(value + '').validate.string().min(11,'Данное поле обязательно.'),
    initialValue: '7',
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <CustomPhoneInputStyled
        mask="+7 (999) 999-99-99"
        value={data}
        onChange={(e:any)=> {
          changeData(yap(e.target.value).formatter.string().onlyNumber().data )
        }}
        isValidate={isValidate || !isTouch}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};
