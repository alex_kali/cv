import React, {FC, memo, useContext, useState} from 'react';
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {CheckboxInputStyled, CheckboxTextStyled, WrapperCheckboxInputStyled, WrapperCheckboxItemStyled} from "./styled";

interface ICheckboxInput {
  name: string;
  data: Array<{text: string, value: string}>
  required?: boolean;
}

export const CheckboxInput: FC<ICheckboxInput> = (props) => {
  const formController:any = useContext(formControllerContext)
  const {useData} = formController.useField({
    name: props.name,
    isRequired: props.required,
  })
  const [data, changeData]: [Array<string> | undefined, (data: Array<string> | undefined) => undefined] = useData()

  const onChange = (e: any) => {
    if(data){
      if(data.indexOf(e.target.value) === -1){
        changeData([...data,e.target.value])
      }else{
        const newData = (data.filter((i) => i !== e.target.value))
        if(newData.length) {
          changeData(newData)
        }else{
          changeData(undefined)
        }
      }
    }else{
      changeData([e.target.value])
    }
  }


  return (
    <WrapperCheckboxInputStyled>
      {props.data.map((i)=>
        <label htmlFor={i.value} key={i.value}>
          <WrapperCheckboxItemStyled>
            <CheckboxTextStyled>{i.text}</CheckboxTextStyled>
            <CheckboxInputStyled id={i.value} type="checkbox" name={props.name} value={i.value} onChange={onChange}/>
          </WrapperCheckboxItemStyled>
        </label>
      )}
    </WrapperCheckboxInputStyled>
  )
};