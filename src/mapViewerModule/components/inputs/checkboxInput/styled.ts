import {customInputStyled, customStyled} from "../../../utils/customStyled";

export const WrapperCheckboxInputStyled = customStyled({
  default: `
    
  `
})

export const CheckboxInputStyled = customInputStyled({
  default: `
    float: left;
    display: inline-block;
    margin-left: 15px;
    margin-top: 5px;
  `
})

export const WrapperCheckboxItemStyled = customStyled({
  default: `
    width: 400px;
    clear: both;
  `
})

export const CheckboxTextStyled = customStyled({
  default: `
    display: inline-block;
    margin-left: 15px;
  `
})