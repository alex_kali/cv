import styled from "styled-components";
import {customStyled} from "../../../utils/customStyled";
import {BORDER_INPUT, BORDER_RADIUS_INPUT, OPACITY_FOCUS_INPUT, OPACITY_INPUT} from "../../inputs/stylesConstant";

export const SubmitButtonStyled = customStyled({
  default: `
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-left: auto;
    margin-right: auto;
    cursor: pointer;
    border: ${BORDER_INPUT};
    border-radius: ${BORDER_RADIUS_INPUT};
    margin-top: 15px;
    font-size: 18px;
    &:hover{
      background: rgb(0,0,0);
      border-color: rgb(0,0,0);
      color: white;
    }
  `,
  defaultByProps: props => `
    opacity: ${props.isValidForm ? OPACITY_FOCUS_INPUT : OPACITY_INPUT};
    &:hover{
      cursor: ${props.isValidForm ? '' : 'not-allowed'};
    }
  `,
  colorStyle: {
    retro: `
      color: #523735;
      border-color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
      border-color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
      border-color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
      border-color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
      border-color: #2b4a4a;
    `,
    blue: `
      color: #283347;
      border-color: #283347;
    `
  }
})