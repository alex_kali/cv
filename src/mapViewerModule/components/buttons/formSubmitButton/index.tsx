import React, { FC, memo, useContext} from 'react';
import {SubmitButtonStyled} from "./styles";
import {formControllerContext} from "../../../utils/formUtils/wrapperForm";
import {useSelector} from "react-redux";
import {formDataParser} from "../../../utils/formUtils/formDataParser";

interface ISubmitButton {
  text: string;
}

export const SubmitButton: FC<ISubmitButton> = (props) => {
  const formController:any = useContext(formControllerContext)
  const formData:any = useSelector(formController.formSelector)

  const onClick = () => {
    if(formData.isValidForm){
      formController.submitForm(formDataParser(formData)[0])
    }
  }

  return (
    <SubmitButtonStyled isValidForm={formData.isValidForm} onClick={onClick}>{props.text}</SubmitButtonStyled>
  )
};
