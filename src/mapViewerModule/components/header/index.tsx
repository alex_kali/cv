import { FC, memo } from 'react';
import {useNavigation} from "src/mapViewerModule/utils/useNavigation";
import {HeaderSectionStyled, ItemStyled, WrapperHeaderStyled, WrapperLogoStyled} from "./styled";
import { useLocation } from 'react-router-dom';
import {useDispatch} from "react-redux";
import {setIsViewSetting} from "../../store/auth/slices";

const paths = {

}

export const Header: FC = memo(() => {
  const navigate = useNavigation();
  const location = useLocation();
  const dispatch = useDispatch();

  return (
    <WrapperHeaderStyled>
      <HeaderSectionStyled>
        <WrapperLogoStyled/>
        <ItemStyled onClick={()=>navigate("/map")} checked={location.pathname === '/map'}>Map</ItemStyled>
      </HeaderSectionStyled>
      <HeaderSectionStyled>
        <ItemStyled>About Us</ItemStyled>
        <ItemStyled onClick={()=>dispatch(setIsViewSetting(true))}>Settings</ItemStyled>
        <ItemStyled onClick={()=>navigate("/map/auth")} checked={location.pathname === '/map/auth'}>Sign in</ItemStyled>
      </HeaderSectionStyled>
    </WrapperHeaderStyled>
  );
});