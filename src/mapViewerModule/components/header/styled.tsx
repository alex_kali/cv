import styled from "styled-components";
import {ReactComponent as ReactLogo} from '../icons/logo.svg';
import {customStyled} from "../../utils/customStyled";

export const WrapperHeaderStyled = customStyled({
  default: `
    height: 69px;
    width: calc(100% - 60px);
    background: rgb(253,251,250);
    border-bottom: 1px solid rgba(0,0,0,0.1);
    display: flex;
    justify-content: space-between;
    padding: 10px;
    padding-left: 30px;
    padding-right: 30px;
  `,
  colorStyle:{
    silver: `
      background: white;
      border-bottom: 1px solid rgba(0,0,0,0.2);
    `,
    retro: `
      background: #f5f1e6;
    `,
    dark: `
      background: #2c2c2c;
      border-bottom: 1px solid rgb(117,117,117);
    `,
    night: `
      background: #2f3948;
      border-bottom: 1px solid rgba(116, 104, 85, 0.4);
    `,
    aubergine: `
      background: rgb(13,13,20);
      border-bottom: 1px solid rgba(2,131,5,0.4);
    `,
    green: `
      background: #dae4e5;
    `,
    blue: `
      background: #dfe7f2;
    `
  }
})
export const WrapperLogoStyled = styled(ReactLogo)`
  height: 70px;
  width: 160px;
  margin-top: -7px;
  opacity: 0.9;
  ${props=>props.theme.colorStyle === 'retro' && `
    & path, text{
      fill: #523735 !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'dark' && `
    & path, text{
      fill: rgb(170,170,170) !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'night' && `
    & path, text{
      fill: #d59563 !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'aubergine' && `
    & path, text{
      fill: rgb(2,131,5) !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'green' && `
    & path, text{
      fill: #2b4a4a !important; 
    }
  `}
  ${props=>props.theme.colorStyle === 'blue' && `
    & path, text{
      fill: #283347 !important; 
    }
  `}
`

export const HeaderSectionStyled = customStyled({
  default: `
    display: flex;
    align-items: center;
  `,
  defaultByProps: props => `
    
  `
})

export const ItemStyled = customStyled({
  default: `
    padding-left: 10px;
    padding-right: 10px;
    font-size: 18px;
    cursor: pointer;
    opacity: 0.6;
    &:hover{
      opacity: 1;
    }
    position: relative;
    &:before{
      content: '';
      display: block;
      position: absolute;
      width: 0%;
      height: 2px;
      left: 50%;
      bottom: -5px;
      background: black;
      transition-duration: 0.4s;
      transition-timing-function: ease-in-out;
    }
    &:after{
      content: '';
      display: block;
      position: absolute;
      width: 0%;
      height: 2px;
      right: 50%;
      bottom: -5px;
      background: black;
      transition-duration: 0.4s;
      transition-timing-function: ease-in-out;
    }
  `,
  defaultByProps: props => `
    ${props.checked && `
      opacity: 1;
      &:before{
        width: 50%;
      }
      &:after{
        width: 50%;
      }
    `}
  `,
  colorStyle:{
    retro: `
      color: #523735;
      &:before{
        background: #523735;
      }
      &:after{
        background: #523735;
      }
    `,
    dark: `
      color: rgb(170,170,170);
        &:before{
          background: rgb(170,170,170);
        }
        &:after{
          background: rgb(170,170,170);
        }
    `,
    night: `
      color: #d59563;
        &:before{
          background: #d59563;
        }
        &:after{
          background: #d59563;
        }
    `,
    aubergine: `
      color: rgb(2,131,5);
        &:before{
          background: rgb(2,131,5);
        }
        &:after{
          background: rgb(2,131,5);
        }
    `,
    green: `
      color: #2b4a4a;
        &:before{
          background: #2b4a4a;
        }
        &:after{
          background: #2b4a4a;
        }
    `,
    blue: `
      color: #283347;
        &:before{
          background: #283347;
        }
        &:after{
          background: #283347;
        }
    `
  }
})