import {customStyled} from "../../utils/customStyled";
import {ERROR_COLOR_INPUT, MARGIN_INPUT} from "../inputs/stylesConstant";

export const InputTitleStyled = customStyled({
  default: `
    margin-top: 15px;
    margin-bottom: 4px;
    font-size: 18px;
    padding-left: 5px;
    padding-right: 5px;
    color: rgba(0,0,0,0.7);
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})