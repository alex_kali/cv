import {customStyled} from "../../utils/customStyled";
import {ReactComponent as close} from "./icons/close.svg";
import styled from "styled-components";

export const WrapperSettingsStyled = customStyled({
  default: `
    position: fixed;
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    background: rgba(0,0,0,0.7);
    z-index: 1;
    display: flex;
    align-items: center;
    justify-content: center;
  `,
})

export const SettingsStyled = customStyled({
  default: `
    width: 450px;
    height: 600px;
    background: rgb(253,251,250);
    border-radius: 20px;
    border: 1px solid rgb(50,50,50);
    display: flex;
    flex-direction: column;
    position: relative;
  `,
  colorStyle:{
    silver: `
      background: white;
    `,
    retro: `
      background: #f5f1e6;
    `,
    dark: `
      background: #2c2c2c;
      border-color: rgb(117,117,117);
    `,
    night: `
      background: #2f3948;
      border-color: rgba(116,104,85,0.4);
    `,
    aubergine: `
      background: rgb(13,13,20);
      border-color: rgba(2,131,5,0.4);
    `,
    green: `
      background: #dae4e5;
    `,
    blue: `
      background: #dfe7f2;
    `
  }

})

export const SettingTitleStyled = customStyled({
  default: `
    text-align: center;
    margin-top: 20px;
    font-size: 22px;
    font-weight: 500;
    margin-bottom: 30px;
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const BrStyled = customStyled({
  default: `
    height: 1px;
    width: 100%;
    background: rgba(0,0,0,0.2);
    margin-top: 10px;
    margin-bottom: 10px;
  `,
  colorStyle: {
    dark: `
      background: rgba(117,117,117);
    `,
    night: `
      background: rgba(116,104,85,0.4); 
    `,
    aubergine: `
      background: rgba(2,131,5,0.4);
    `
  }
})

export const NoLoginMessageStyled = customStyled({
  default: `
    padding-bottom: 10%;
    width: 60%;
    margin-left: 20%;
    flex-grow: 3;
    display: flex;
    align-items: center;
    justify-content: center;
    text-align: center;
    font-size: 20px;
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const CustomCloseStyled = styled(close)`
  position: absolute;
  right: 10px;
  top: 15px;
  width: 30px;
  height: 30px;
  cursor: pointer;
  fill: rgba(0,0,0,1);
  ${(props=>props.theme.colorStyle === 'retro' && `
    & path, text{
      fill: #523735 !important; 
    }
  `) || ''}
  ${(props=>props.theme.colorStyle === 'dark' && `
    & path, text{
      fill: rgb(170,170,170) !important; 
    }
  `) || ''}
  ${(props=>props.theme.colorStyle === 'night' && `
    & path, text{
      fill: #d59563 !important; 
    }
  `) || ''}
  ${(props=>props.theme.colorStyle === 'aubergine' && `
    & path, text{
      fill: rgb(2,131,5) !important; 
    }
  `) || ''}
`