import {FC, memo, useEffect, useState} from "react";
import {
  BrStyled,
  CustomCloseStyled,
  NoLoginMessageStyled,
  SettingsStyled,
  SettingTitleStyled,
  WrapperSettingsStyled
} from "./styled";
import {useDispatch, useSelector} from "react-redux";
import {isViewSettingsSelector} from "../../store/auth/selectors";
import {useForm} from "../../utils/formUtils/useForm";
import { WrapperForm } from "../../utils/formUtils/wrapperForm";
import {SelectInput} from "../inputs/select";
import {setIsViewSetting} from "../../store/auth/slices";
import {useFieldSelector} from "../../store/formController/slices";
import {colorStyleSelector, setColorStyle} from "../../store/styleController/slices";

const data = [
  {
    "value": "default",
    "text": "default"
  },
  {
    "value": "silver",
    "text": "silver"
  },
  {
    "value": "retro",
    "text": "retro"
  },
  {
    "value": "dark",
    "text": "dark"
  },
  {
    "value": "night",
    "text": "night"
  },
  {
    "value": "aubergine",
    "text": "aubergine"
  },
  {
    "value": "green",
    "text": "green"
  },
  {
    "value": "blue",
    "text": "blue"
  },
  // {
  //   "value": "red",
  //   "text": "Красная"
  // },
]

export const Settings: FC = memo(() => {
  const isViewSetting = useSelector(isViewSettingsSelector)
  const colorStyle = useSelector(colorStyleSelector)
  const dispatch = useDispatch()

  const form = useForm({
    name: 'setting',
    initialValue: {colorStyle: colorStyle},
  })

  if(!isViewSetting){
    return null
  }

  return (
    <WrapperSettingsStyled>
      <WrapperForm form={form} isValidForm={true}>
        <SettingsStyled>
          <CustomCloseStyled onClick={()=>dispatch(setIsViewSetting(false))}/>
          <SettingTitleStyled>Settings</SettingTitleStyled>
          <SelectInput title={'Color palette'} name={'colorStyle'} data={data} onChange={(value)=>{dispatch(setColorStyle(value))}}/>
          <BrStyled style={{marginTop: 30}}/>
          <NoLoginMessageStyled>Login for more settings!</NoLoginMessageStyled>
        </SettingsStyled>
      </WrapperForm>
    </WrapperSettingsStyled>
  );
});