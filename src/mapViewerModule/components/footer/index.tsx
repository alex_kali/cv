import { FC, memo } from 'react';
import {WrapperFooterStyled} from "./styled";

export const Footer: FC = memo(() => {
    return <WrapperFooterStyled>Footer</WrapperFooterStyled>;
});
