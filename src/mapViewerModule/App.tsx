import {Header} from "./components/header";
import {Map} from "./pages/map";
import {
  BrowserRouter as Router, Route, Switch
} from "react-router-dom";
import {Auth} from "./pages/auth";
import {UserPage} from "./pages/userPage";
import {Settings} from "./components/settings";
import React from 'react';
import {store} from "./store/store";
import {Provider} from "react-redux";
import {WrapperSizeController} from "./utils/wrapperSizeController";

export function MapApp() {
  // @ts-ignore
  import('./index.css');
  // @ts-ignore
  return (
    <Provider store={store}>
      <WrapperSizeController>
        <Header/>
        <Settings/>
        <div style={{height: 'calc(100vh - 90px)', width: '100%'}}>
          <Switch>
            <Route path="/">
              <Map />
            </Route>
            <Route path="/auth">
              <Auth />
            </Route>
            <Route path="/user">
              <UserPage />
            </Route>
          </Switch>
        </div>
        {/*<Footer/>*/}
      </WrapperSizeController>
    </Provider>
  );
}
