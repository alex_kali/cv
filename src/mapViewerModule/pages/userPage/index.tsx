import {FC, memo, useEffect} from "react";
import {useSelector} from "react-redux";
import {useNavigation} from "src/mapViewerModule/utils/useNavigation";
import {tokenSelector} from "../../store/auth/selectors";

export const UserPage: FC = memo(() => {
  const navigate = useNavigation();
  const token = useSelector(tokenSelector)
  useEffect(()=>{
    if(!token){
      navigate("/auth");
    }
  },token)

  return (
    <>
      UserPage
    </>
  );
});