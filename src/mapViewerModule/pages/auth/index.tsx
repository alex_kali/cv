import {FC, memo, useEffect, useState} from "react";
import {useNavigation} from "src/mapViewerModule/utils/useNavigation";
import {
  ChoiceLeftStyled, ChoiceRightStyled,
  ContentStyled,
  WrapperAuthStyled,
  WrapperChoiceStyled,
  WrapperContentStyled
} from "./styled";
import {Login} from "./login";
import {Registration} from "./registration";
import {tokenSelector} from "../../store/auth/selectors";
import {useSelector} from "react-redux";

export const Auth: FC = memo(() => {
  const navigate = useNavigation();
  const [isLogin, setIsLogin] = useState(true)
  const token = useSelector(tokenSelector)

  useEffect(() => {
    if(token){
      navigate("/user");
    }
  }, token)

  return (
    <WrapperAuthStyled>
      <WrapperContentStyled>

          <WrapperChoiceStyled>
            <ChoiceLeftStyled checked={isLogin} onClick={()=>{setIsLogin(true)}}>Авторизация</ChoiceLeftStyled>
            <ChoiceRightStyled checked={!isLogin} onClick={()=>{setIsLogin(false)}}>Регистрация</ChoiceRightStyled>
          </WrapperChoiceStyled>
          <ContentStyled checked={isLogin}>
            {isLogin && <Login />}
            {!isLogin && <Registration />}
          </ContentStyled>

      </WrapperContentStyled>
    </WrapperAuthStyled>
  );
});