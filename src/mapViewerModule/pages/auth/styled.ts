import {customStyled} from "../../utils/customStyled";
import {retry} from "@reduxjs/toolkit/query";
import styled from "styled-components";

export const WrapperAuthStyled = customStyled({
  default: `
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    min-height: 80%;
    display: flex;
    align-items: center;
    justify-content: center;
  `},
)

export const WrapperChoiceStyled = customStyled({
  default: `
    width: 100%;
    height: 60px;
    clear: both;
  `,
  },
)

export const WrapperContentStyled = customStyled({
  default: `
    max-width: 500px;
    width: 100%;
    position: relative;
    &:before{
      content: '';
      position: absolute;
      left: 0;
      right: 0;
      width: calc(100% - 2px);
      height: calc(100% - 2px);
      background: white;
      z-index: -1;
      border-radius: 22px;
      border: 1px solid rgba(0,0,0,0.1);
    }
  `,
  colorStyle:{
    silver: `
      &:before{
        background: white;
        border-color: rgba(0,0,0,0.2) !important;
      }
    `,
    retro: `
      &:before{
        background: #f5f1e6;
      }
    `,
    dark: `
      &:before{
        background: #2c2c2c;
        border-color: rgb(117,117,117) !important;
      }
    `,
    night: `
      &:before{
        background: #2f3948;
        border-color: rgba(116, 104, 85, 0.4) !important;
      }
    `,
    aubergine: `
      &:before{
        background: rgb(13,13,20);
        border-color: rgba(2,131,5,0.4) !important;
      }
    `,
    green: `
      &:before{
        background: #dae4e5;
      }
    `,
    blue: `
      &:before{
        background: #dfe7f2;
      }
    `
  }
  }
)

export const ContentStyled = customStyled({
  default: `
    width: calc(100% - 82px);
    background: rgb(253,251,250);
    overflow: hidden;
    border: 1px solid rgba(0,0,0,0.1);
    padding-left: 40px;
    padding-right: 40px;
    padding-bottom: 30px;
    border-bottom-right-radius: 20px;
    border-bottom-left-radius: 20px;
  `,
  defaultByProps: props => `
     ${props.checked ? `
        border-top-right-radius: 20px;
    ` : `
        border-top-left-radius: 20px;
    `}
  `,
  colorStyle:{
    silver: `
      background: white;
      border-color: rgba(0,0,0,0.2);
    `,
    retro: `
      background: #f5f1e6;
    `,
    dark: `
      background: #2c2c2c;
      border-color: rgb(117,117,117);
    `,
    night: `
      background: #2f3948;
      border-color: rgba(116, 104, 85, 0.4);
    `,
    aubergine: `
      background: rgb(13,13,20);
      border-color: rgba(2,131,5,0.4);
    `,
    green: `
      background: #dae4e5;
    `,
    blue: `
      background: #dfe7f2;
    `
  }
  },
)


export const ChoiceLeftStyled:any = customStyled({
  default: `
    width: calc(50% - 1px);
    height: 100%;
    float: left;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    border-top-right-radius: 20px;
    border-top-left-radius: 20px;
    position: relative;
    font-size: 18px;
  `,
  defaultByProps: props => `
    ${props.checked ? `
      border-left: 1px solid rgba(0,0,0,0.1);
      border-right: 1px solid rgba(0,0,0,0.1);
      border-top: 1px solid rgba(0,0,0,0.1);
      background: rgb(253,251,250);
      margin-bottom: -1px;
      height: calc(100% + 1px);
      ${props.theme.colorStyle === 'silver' ? `
        background: white;
      ` : ''}
      ${props.theme.colorStyle === 'retro' ? `
        background: #f5f1e6;
      ` : ''}
      ${props.theme.colorStyle === 'dark' ? `
        background: #2c2c2c;
      ` : ''}
      ${props.theme.colorStyle === 'night' ? `
        background: #2f3948;
      ` : ''}
      ${props.theme.colorStyle === 'aubergine' ? `
        background: rgb(13,13,20);
      ` : ''}
      ${props.theme.colorStyle === 'green' ? `
        background: #dae4e5;
      ` : ''}
      ${props.theme.colorStyle === 'blue' ? `
        background: #dfe7f2;
      ` : ''}
    ` : `
      opacity: 0.3;
    `
  }
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    silver: `
     border-color: rgba(0,0,0,0.2) !important;
    `,
    dark: `
      color: rgb(170,170,170);
      border-color: rgb(117,117,117) !important;
    `,
    night: `
      color: #d59563;
      border-color: rgba(116, 104, 85, 0.4) !important;
    `,
    aubergine: `
      color: rgb(2,131,5);
      border-color: rgba(2,131,5,0.4) !important;
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const ChoiceRightStyled:any = customStyled({
  default: `
    width: calc(50% - 1px);
    height: 100%;
    float: left;
    display: flex;
    align-items: center;
    justify-content: center;
    cursor: pointer;
    border-top-right-radius: 20px;
    border-top-left-radius: 20px;
    font-size: 18px;
  `,
  defaultByProps: props => `
    ${props.checked ? `
      border-left: 1px solid rgba(0,0,0,0.1);
      border-right: 1px solid rgba(0,0,0,0.1);
      border-top: 1px solid rgba(0,0,0,0.1);
      background: rgb(253,251,250);
      margin-bottom: -1px;
      height: calc(100% + 1px);
      ${props.theme.colorStyle === 'silver' ? `
        background: white;
      ` : ''}
      ${props.theme.colorStyle === 'retro' ? `
        background: #f5f1e6;
      ` : ''}
      ${props.theme.colorStyle === 'dark' ? `
        background: #2c2c2c;
      ` : ''}
      ${props.theme.colorStyle === 'night' ? `
        background: #2f3948;
      ` : ''}
      ${props.theme.colorStyle === 'aubergine' ? `
        background: rgb(13,13,20);
      ` : ''}
      ${props.theme.colorStyle === 'green' ? `
        background: #dae4e5;
      ` : ''}
      ${props.theme.colorStyle === 'blue' ? `
        background: #dfe7f2;
      ` : ''}
    ` : `
      opacity: 0.3;
    `}
  `,
  colorStyle: {
    retro: `
      color: #523735;
    `,
    silver: `
     border-color: rgba(0,0,0,0.2) !important;
    `,
    dark: `
      color: rgb(170,170,170);
      border-color: rgb(117,117,117) !important;
    `,
    night: `
      color: #d59563;
      border-color: rgba(116, 104, 85, 0.4) !important;
    `,
    aubergine: `
      color: rgb(2,131,5);
      border-color: rgba(2,131,5,0.4) !important;
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})
