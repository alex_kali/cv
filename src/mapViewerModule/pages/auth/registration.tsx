import {FC, memo, useState} from "react";
import {useForm} from "../../utils/formUtils/useForm";
import {WrapperForm} from "../../utils/formUtils/wrapperForm";
import {PhoneInput} from "../../components/inputs/phoneInput";
import {PasswordInput} from "../../components/inputs/password";
import {SubmitButton} from "../../components/buttons/formSubmitButton";
import {EmailInput} from "../../components/inputs/emailInput";
import {useDataController} from "../../utils/slices";
import {userDataSelector} from "../../store/auth/selectors";
import {InputTitleStyled} from "../../components/titles/inputTitle";

export const Registration: FC = memo(() => {
  const form = useForm({
    name: 'registration',
  })
  const user = useDataController(userDataSelector)

  const onSubmit = (data: any) => {
    user.registration(data,'registration')
  }

  return (
    <WrapperForm form={form} onSubmit={onSubmit}>

      <InputTitleStyled>Имя пользователя</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Емайл</InputTitleStyled>
      <EmailInput name={'email'} required/>

      <InputTitleStyled>Пароль</InputTitleStyled>
      <PasswordInput name={'password1'} required/>

      <InputTitleStyled>Повторите пароль</InputTitleStyled>
      <PasswordInput name={'password2'} required/>

      <SubmitButton text={'Зарегестрироваться'}/>
    </WrapperForm>
  );
});