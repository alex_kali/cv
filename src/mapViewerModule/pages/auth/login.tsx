import {FC, memo, useState} from "react";
import {useForm} from "../../utils/formUtils/useForm";
import {WrapperForm} from "../../utils/formUtils/wrapperForm";
import {PhoneInput} from "../../components/inputs/phoneInput";
import {PasswordInput} from "../../components/inputs/password";
import {SubmitButton} from "../../components/buttons/formSubmitButton";
import {useDispatch, useSelector} from "react-redux";
import {loginUser} from "../../store/auth/thunks";
import {userDataSelector} from "../../store/auth/selectors";
import {useDataController} from "../../utils/slices";
import {InputTitleStyled} from "../../components/titles/inputTitle";

export const Login: FC = memo(() => {
  const form = useForm({
    name: 'login',
  })
  const user = useDataController(userDataSelector)

  const onSubmit = (data: any) => {
    user.login(data,'login')
  }

  return (
    <WrapperForm form={form} isValidForm={true} onSubmit={onSubmit}>

      <InputTitleStyled>Номер телефона</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Пароль</InputTitleStyled>
      <PasswordInput name={'password'} required/>

      <SubmitButton text={'Авторизоваться'}/>
    </WrapperForm>
  );
});