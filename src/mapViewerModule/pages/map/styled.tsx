import styled from "styled-components";

export const WrapperMapStyled = styled.div`
  width: 100%;
  height: 100%;
  background: grey;
  display: block;
  position: relative;
  & .gmnoprint{
    display: none;
  }
`