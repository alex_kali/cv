import {customStyled} from "../../../utils/customStyled";
import {ActiveRegionInformation} from "./activeRegionInformation";

export const WrapperMenuStyled = customStyled({
  default: `
    position: absolute;
    top: 0px;
    right: 0px;
    height: calc(100% - 20px);
    width: 300px;
    background: rgb(253,251,250);
    border-left: 1px solid rgba(0,0,0,0.1);
    padding-top: 10px;
    padding-bottom: 10px;
    display: flex;
    flex-direction: column;
  `,
  colorStyle:{
    silver: `
      background: white;
      border-left: 1px solid rgba(0,0,0,0.2);
    `,
    retro: `
      background: #f5f1e6;
    `,
    dark: `
      background: #2c2c2c;
      border-left: 1px solid rgb(117,117,117);
    `,
    night: `
      border-left: 1px solid rgba(116,104,85,0.4);
      background: #2f3948;
    `,
    aubergine: `
      background: rgb(13,13,20);
      border-left: 1px solid rgba(2,131,5,0.4);
    `,
    green: `
      background: #dae4e5;
    `,
    blue: `
      background: #dfe7f2;
    `
  }
})

export const TitleStyled = customStyled({
  default: `
    margin-bottom: 15px;
    font-size: 20px;
    margin-left: 15px;
  `,
  colorStyle:{
    retro: `
      color: #523735;
    `,
    dark: `
      color: rgb(170,170,170);
    `,
    night: `
      color: #d59563;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `,
    green: `
      color: #2b4a4a;
    `,
    blue: `
      color: #283347;
    `
  }
})

export const BrStyled = customStyled({
  default: `
    height: 1px;
    width: 100%;
    background: rgba(0,0,0,0.1);
    margin-top: 10px;
    margin-bottom: 10px;
  `,
  colorStyle: {
    silver: `
      background: rgba(0,0,0,0.2);
    `,
    dark: `
      background: rgb(117,117,117);
    `,
    night: `
      background: rgba(116,104,85,0.4);
    `,
    aubergine: `
      background: rgba(2,131,5,0.4);
    `
  }
})

export const WrapperActiveRegionInformation = customStyled({
  default: `
    width: 100%;
    flex-grow: 3;
  `,
  defaultByProps: props =>`
    & .recharts-line .recharts-line-dots circle:nth-child(${props.activeDot + 1}){
      stroke: grey !important;
      fill: grey;
      stroke-width: 5;
      ${(props.theme.colorStyle === 'retro' && `
        fill: #523735 !important;
        stroke: #523735 !important;
      `) || ''}
       ${(props.theme.colorStyle === 'dark' && `
        fill: rgb(170,170,170) !important;
        stroke: rgb(170,170,170) !important;
      `) || ''}
       ${(props.theme.colorStyle === 'night' && `
        fill: #d59563 !important;
        stroke: #d59563 !important;
      `) || ''}
      ${(props.theme.colorStyle === 'aubergine' && `
        fill: rgb(2,131,5) !important;
        stroke: rgb(2,131,5) !important;
      `) || ''}
      ${(props.theme.colorStyle === 'green' && `
        fill: #2b4a4a !important;
        stroke: #2b4a4a !important;
      `) || ''}
      ${(props.theme.colorStyle === 'blue' && `
        fill: #283347 !important;
        stroke: #283347 !important;
      `) || ''}
    }
  `,
  colorStyle: {
    retro: `
      & .recharts-line .recharts-line-dots circle{
        fill: #f5f1e6;
      }
      & *{
        stroke: #523735 !important; 
      }
      & text{
        fill: #523735 !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `,
    dark: `
      & .recharts-line .recharts-line-dots circle{
        fill: #2c2c2c;
      }
      & *{
        stroke: rgb(170,170,170) !important; 
      }
      & text{
        fill: rgb(170,170,170) !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `,
    night: `
      & .recharts-line .recharts-line-dots circle{
        fill: #2f3948;
      }
      & *{
        stroke: #d59563 !important; 
      }
      & text{
        fill: #d59563 !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `,
    aubergine: `
      & .recharts-line .recharts-line-dots circle{
        fill: rgb(13,13,20);
      }
      & *{
        stroke: rgb(2,131,5) !important; 
      }
      & text{
        fill: rgb(2,131,5) !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `,
    green: `
      & .recharts-line .recharts-line-dots circle{
        fill: #dae4e5;
      }
      & *{
        stroke: #2b4a4a !important; 
      }
      & text{
        fill: #2b4a4a !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `,
    blue: `
      & .recharts-line .recharts-line-dots circle{
        fill: #dfe7f2;
      }
      & *{
        stroke: #283347 !important; 
      }
      & text{
        fill: #283347 !important; 
      }
      & tspan{
        stroke: none !important;
      }
    `
  }
})

export const MessageStyled = customStyled({
  default: `
    width: 100%;
    flex-grow: 3;
    display: flex;
    align-items: center;
    justify-content: center;
  `,
  colorStyle:{
    retro: `
      color: #523735;
    `,
    aubergine: `
      color: rgb(2,131,5);
    `
  }
})