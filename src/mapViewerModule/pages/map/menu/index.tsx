import React, {FC, memo, useContext, useEffect, useRef} from 'react';
import {BrStyled, MessageStyled, TitleStyled, WrapperActiveRegionInformation, WrapperMenuStyled} from "./styled";
import {useCustomSelector} from "../../../utils/slices";
import {optionsAverageDynamicsSelector, optionsMapSelector} from "../../../store/map/selectors";
import {RadioInput} from "../../../components/inputs/radioInput";
import {WrapperForm} from "../../../utils/formUtils/wrapperForm";
import {useForm} from "../../../utils/formUtils/useForm";
import {useSelector} from "react-redux";
import {useFieldSelector} from "../../../store/formController/slices";
import {changePolygonColor} from "../../../utils/map/changePolygonColor";
import {SingleCheckboxInput} from "../../../components/inputs/singleCheckboxInput";
import {ActiveRegionInformation} from "./activeRegionInformation";
import {yap} from "../../../utils/yap";
import {colorStyleSelector} from "../../../store/styleController/slices";

const translate:any = {
  POPULATION : 'Population',
  GDP: 'GDP'
}

const typeCalc:any = {
  POPULATION : 'people',
  GDP: 'billion rubles'
}

interface IMenu {
  mapPolygons: any;
  activePolygon: any;
}

export const Menu: FC<IMenu> = (props) => {
  const optionsMap = useCustomSelector(optionsMapSelector,true)
  const optionsAverageDynamics = useCustomSelector(optionsAverageDynamicsSelector,true)

  const type = useFieldSelector({formName: 'mapMenu', fieldName: 'type'}) || 'POPULATION'
  const year = useFieldSelector({formName: 'mapMenu', fieldName: 'year'})
  const isDynamicAverage = useFieldSelector({formName: 'mapMenu', fieldName: 'isDynamicAverage'})
  const colorStyle = useSelector(colorStyleSelector)

  const form = useForm({
    name: 'mapMenu'
  })
  const prevActivePolygon:any = useRef()

  useEffect(()=>{
    if(prevActivePolygon.current){
      if(type && year){
        if (isDynamicAverage) {
          changePolygonColor(
            prevActivePolygon.current.polygon,
            prevActivePolygon.current.data.averagedynamicsSet[type] + 2,
            optionsAverageDynamics[type].data + 2,
            colorStyle
          )
        }else{
          changePolygonColor(
            prevActivePolygon.current.polygon,
            prevActivePolygon.current.data.regiondataSet[type]?.[year]['false'],
            optionsMap[type]?.[year].data,
            colorStyle
          )
        }
      }
    }
    prevActivePolygon.current = props.activePolygon
  },[props.activePolygon])

  useEffect(()=>{
    if(optionsMap){
      form.changeDataField('year',Object.keys(optionsMap[type])[0])
    }
  }, [type, optionsMap])

  useEffect(()=> {
    if (isDynamicAverage) {
      for (let i of props.mapPolygons) {
        changePolygonColor(
          i.polygon,
          i.averagedynamicsSet[type] + 2,
          optionsAverageDynamics[type].data + 2,
          colorStyle,
          i.name === props.activePolygon?.data?.name
        )
      }
    } else {
      for (let i of props.mapPolygons) {
        changePolygonColor(
          i.polygon,
          i.regiondataSet[type]?.[year]?.['false'],
          optionsMap[type]?.[year]?.data,
          colorStyle,
          i.name === props.activePolygon?.data?.name
        )
      }
    }
  },[year,isDynamicAverage, colorStyle, optionsMap, props.mapPolygons])
  
  if(!optionsMap || !props.mapPolygons){
    return null
  }

  const isActiveStaticMenu = props?.activePolygon?.data && props?.activePolygon?.data?.regiondataSet?.[type]
  
  return (
    <WrapperMenuStyled>
      <WrapperForm form={form} isValidForm={true}>
        <TitleStyled>Тип</TitleStyled>
        <RadioInput name={'type'} data={Object.keys(optionsMap).map((i)=>{return {value: i, text: translate[i]}}) as any} initialValue={'POPULATION'}/>
        <BrStyled />
        <TitleStyled>Год</TitleStyled>
        <RadioInput name={'year'} data={Object.keys(optionsMap[type]).map((i)=>{return {value: i, text: i}}) as any}/>
        <BrStyled />
        <SingleCheckboxInput name={'isDynamicAverage'} text={"Show on the map the average annual dynamics"}/>
        <BrStyled />
      </WrapperForm>
      {isActiveStaticMenu &&
        <WrapperActiveRegionInformation activeDot={Object.keys(optionsMap[type]).indexOf(year)}>
          <TitleStyled>{props.activePolygon.data.name}</TitleStyled>
          <ActiveRegionInformation activePolygon={props.activePolygon}/>
          <BrStyled />
          <TitleStyled style={{fontSize: 14}}>Mark: {yap(props.activePolygon.data.regiondataSet[type][year]?.true + '').formatter.string().price().data} {typeCalc[type]}.</TitleStyled>
          <TitleStyled style={{fontSize: 14}}>Average annual growth: {props.activePolygon.data.averagedynamicsSet[type]}%.</TitleStyled>
        </WrapperActiveRegionInformation>
      }
      {!isActiveStaticMenu &&
        <MessageStyled>
          Choose region!
        </MessageStyled>
      }
    </WrapperMenuStyled>
  )
};
