import {FC, memo} from "react";
import {WrapperActiveRegionInformation} from "./styled";
import { LineChart, Line, CartesianGrid, XAxis, YAxis } from 'recharts';
import {useFieldSelector} from "../../../store/formController/slices";
import {yap} from "../../../utils/yap";

interface IActiveRegionInformation {
  activePolygon: any;
}

export const ActiveRegionInformation: FC<IActiveRegionInformation> = memo((props) => {
  const type = useFieldSelector({formName: 'mapMenu', fieldName: 'type'})


  const data:any = Object.values(props?.activePolygon?.data?.regiondataSet?.[type])
  const length:any = (Object.values(props?.activePolygon?.data?.regiondataSet?.[type]) as any)[0]?.true.toString().length
  return (
    <>
      <LineChart width={300} height={150} data={data}>
        <Line
          width={30}
          type="monotone"
          dataKey="true"
          stroke="rgba(0,0,0,0.4)"
          animationDuration={500}
          dot={{ stroke: 'rgba(0,0,0,0.4)', strokeWidth: 1, r: 3,strokeDasharray:''}}
          activeDot={{stroke: 'red'}}
        />
        <YAxis
          width={length <= 4 ? 40 : 60}
          domain={['dataMin', 'dataMax']}
          style={{fontSize: 10}}
          tickFormatter={(value: any) => yap(value + '').formatter.string().price().data}
          axisLine={{width: 3}}
        />
      </LineChart>
    </>
  )
})