import {FC, memo, useEffect, useState} from 'react';
import {WrapperMapStyled} from "./styled";
import {coordsMap} from "./polygon";
import {mapDataSelector, optionsMapSelector} from "../../store/map/selectors";
import {useCustomSelector} from "../../utils/slices";
import {Menu} from "./menu";
import {silver} from "./mapOptions/silver";
import {colorStyleSelector} from "../../store/styleController/slices";
import {useSelector} from "react-redux";
import {night} from "./mapOptions/night";
import {aubergine} from "./mapOptions/aubergine";
import {dark} from "./mapOptions/dark";
import {retro} from "./mapOptions/retro";
import {green} from "./mapOptions/green";
import {red} from "./mapOptions/red";
import {blue} from "./mapOptions/blue";

const styles:any = {
  silver: silver,
  night: night,
  aubergine: aubergine,
  dark: dark,
  retro: retro,
  green: green,
  blue: blue,
  red: red,
}
export const Map: FC = memo(() => {
  const mapData = useCustomSelector(mapDataSelector,true)
  const [activePolygon, setActivePolygon] = useState<any>(undefined)
  const [mapPolygons, setMapPolygons] = useState([])
  const [map,setMap] = useState<any>(undefined)

  const colorStyle = useSelector(colorStyleSelector)

  useEffect(()=>{
    if(mapData){
      const map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: {lat: 54.397, lng: 40.644},
        zoom: 6,
        styles: styles[colorStyle] || []
      });
      setMap(map)
      const newMapPolygons:any = []
      for (let i in mapData){
        const triangleCoords = JSON.parse(mapData[i].coords);
        const polygon = new google.maps.Polygon({
          paths: triangleCoords,
          strokeColor: "rgb(100,100,100)",
          strokeOpacity: 0.8,
          strokeWeight: 1,
          fillColor: "rgba(0,0,0,0.2)",
          fillOpacity: 0.4,
        });
        newMapPolygons.push({...mapData[i],polygon})
        google.maps.event.addListener(polygon, 'click', function (event:any) {
          polygon.setOptions({
            fillColor: 'rgba(105,252,78,0.9)',
            strokeColor: "rgb(100,100,100)",
            strokeOpacity: 1,
            strokeWeight: 2,
            zIndex: 2,
          })
          setActivePolygon((activePolygon: any) => {
            if(activePolygon?.data?.name !== mapData[i].name){
              return {polygon: polygon, data: mapData[i]}
            }else{
              return activePolygon
            }
          })
        });
        polygon.setMap(map);
      }
      setMapPolygons(newMapPolygons)
    }
  },[mapData])

  useEffect(()=>{
    if(map){
      map.setOptions({styles: styles[colorStyle] || []})
    }
  },[colorStyle])

  return (
    <WrapperMapStyled>
      <div id="map" style={{width: '100%', height: '100%'}}></div>
      <Menu mapPolygons={mapPolygons} activePolygon={activePolygon}/>
    </WrapperMapStyled>
  );
});