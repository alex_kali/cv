import {useNavigation} from "src/restaurantModule/utils/useNavigation";
import {
  RangeStyled,
  SliderItemStyled,
  SliderStyled,
  TitleStyled,
  WrapperSliderStyled
} from "./styled";
import {FC, useCallback, useEffect, useRef, useState} from "react";
import Range from 'rc-slider';
import {useParams} from "react-router-dom";

export const Slider:FC<{images: Array<{url: string, name: string, urlPage: string, pageName: string}>}> = (props) => {
  const [isTouch, setIsTouch] = useState(false)
  const [startX, setStartX] = useState(0)
  const [x, setX] = useState(1)
  const SliderRef:any = useRef()
  const WrapperSliderRef:any = useRef()
  const clickRef = useRef({x: 0, y: 0})
  const navigate = useNavigation()
  const { position, page }:any = useParams()

  useEffect(()=>{
    setX(0)
  },[]) // force update

  const onMouseMove = useCallback((e:any) => {
    if(isTouch){
      setX(e.screenX)
    }
  },[isTouch])

  const onTouchMove = useCallback((e:any) => {
    if(isTouch){
      setX(e.nativeEvent.touches[0].screenX)
    }
  },[isTouch])

  const onTouchStop = useCallback(() => {
    setIsTouch(false)
    if(x - startX > 0){
      setStartX(x)
    }
    if(SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth < - (x-startX)){
      setStartX(SliderRef.current.offsetWidth - WrapperSliderRef.current.offsetWidth + x)
    }
  },[startX, x])

  return (
    <>
      <WrapperSliderStyled
        position={position}
        onMouseMove={onMouseMove}
        onMouseDown={(e)=>{
          setIsTouch(true);
          setStartX(e.screenX - x + startX);
          setX(e.screenX)}
        }
        onMouseUp={()=>onTouchStop()}
        onMouseLeave={()=>onTouchStop()}
        onTouchMove={onTouchMove}
        onTouchStart={(e)=>{
          setIsTouch(true);
          setStartX(e.nativeEvent.touches[0].screenX - x + startX);
          setX(e.nativeEvent.touches[0].screenX)}
        }
        onTouchEnd={()=>{onTouchStop()}}
        ref={WrapperSliderRef}
      >
        <SliderStyled style={{left: `max(-${(SliderRef.current?.offsetWidth || 0) - (WrapperSliderRef.current?.offsetWidth || 0)}px, min(${x-startX}px , 0px))`}} ref={SliderRef}>
          {props.images.map((image)=> <SliderItemStyled
            className={'slide'}
            onMouseDown={(e) => {clickRef.current = {x: e.screenX, y: e.screenY}}}
            onMouseUp={(e) => {if(Math.abs(e.screenX - clickRef.current.x) < 15){navigate(image.urlPage)}}}
            style={{backgroundImage: `url(${image.url})`}}
          ><TitleStyled className={'title'} isActive={page === image.pageName}>{image.name}</TitleStyled></SliderItemStyled>)}
        </SliderStyled>
      </WrapperSliderStyled>
      <RangeStyled className={'range'} position={position}>
        <Range max={(SliderRef.current?.offsetWidth || 0) - (WrapperSliderRef.current?.offsetWidth || 0)} value={-(x-startX)} onChange={(e)=>setX(-(e as number) + startX)}/>
      </RangeStyled>
    </>
  )
}