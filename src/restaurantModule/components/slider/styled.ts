import styled from "styled-components";

export const WrapperSliderStyled = styled.div<{position: string}>`
  overflow: hidden;
  width: 100%;
  position: relative;
  height: 576px;
  @media only screen and (max-width : 1024px) {
    height: 400px;
  }
  @media only screen and (max-width : 768px) {
    height: 280px;
  }
  &:before{
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    height: 100%;
    width: 120px;
    background: linear-gradient(270deg, #16181B 0%, rgba(22, 24, 27, 0) 100%);
    transform: rotate(180deg);
    z-index: 1;
    transition-duration: 0.5s;
  }
  &:after{
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 0;
    height: 100%;
    width: 120px;
    background: linear-gradient(270deg, #16181B 0%, rgba(22, 24, 27, 0) 100%);
    z-index: 2;
    transition-duration: 0.5s;
  }
  border-top-left-radius: 11px;
  border-bottom-left-radius: 11px;
  transition-duration: 0.5s;
  ${props => props.position === 'bottom' ? `
    height: 60px;
    &:after{
      background: linear-gradient(270deg, rgba(22, 24, 27, 0) 0%, rgba(22, 24, 27, 0) 100%);
    }
    
    &:before{
      background: linear-gradient(270deg, rgba(22, 24, 27, 0) 0%, rgba(22, 24, 27, 0) 100%);
    }
    .slide {
      height: 60px;
      background-color: rgba(0,0,0,0.9);
      &:before{
        background: rgba(0, 0, 0, 0.8);
      }
    }
    
    .title {
      bottom: 0;
      width: auto;
      margin-left: auto;
      margin-right: auto;
      white-space: nowrap;
      background: rgba(0,0,0,0);
      color: rgba(170, 170, 170, 1);
      padding: 0;
      left: 50%;
      transform: translateX(-50%);
      text-align: center;
    }
    
    .slide:first-child{
      margin-left: 15px;
    }
    .slide:last-child{
      margin-right: 15px;
    }
  ` : ''}
`

export const SliderStyled = styled.div`
  display: flex;
  flex-wrap: nowrap;
  position: absolute;
  left: -1130px;
`

export const SliderItemStyled = styled.div`
  width: 300px;
  height: 576px;
  margin-right: 20px;
  background-size: cover;
  background-position: center;
  border-radius: 11px;
  position: relative;
  overflow: hidden;
  transition-duration: 0.5s;
  &:last-child{
    margin-right: 0;
  }
  &:after{
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 3;
  }
  &:before{
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    z-index: 1;
    background: rgba(0,0,0,0);
    transition-duration: 0.5s;
  }
`

export const TitleStyled = styled.div<{isActive: boolean}>`
  position: absolute;
  left: 0;
  bottom: 50px;
  padding-left: 20px;
  padding-right: 25px;
  width: 175px;
  background: rgba(0,0,0,0.7);
  height: 60px;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  font-size: 16px;
  color: #FF9100;
  font-family: "LatoWebLight", sans-serif;
  transition-duration: 0.5s;
  z-index: 2;
  line-height: 60px;
  ${props => props.isActive ? 'color: #FF9100 !important;' : ''}
`

export const RangeStyled = styled.div<{position: string}>`
  display: inline-block;
  margin-top: 40px;
  width: calc(100% - 280px);
  transition-duration: 0.5s;
  .rc-slider-rail{
    height: 2px;
    background: #36393E;
  }
  .rc-slider-handle{
    border: none !important;
    box-shadow: none !important;
    width: 24px;
    height: 24px;
    margin-top: -11px;
    background: #FF9100;
    opacity: 1;
  }
  .rc-slider-track{
    display: none;
  }
  ${props => props.position === 'bottom' ? 'margin-top: 0; height: 0; overflow: hidden;' : ''}
`

export const ViewAllStyled = styled.div<{isView: boolean}>`
  margin-top: 35px;
  display: inline-block;
  color: #FF9100;
  float: right;
  width: 220px;
  position: relative;
  cursor: pointer;
  transition-duration: 0.5s;
  white-space: nowrap;
  overflow: hidden;
  ${props => props.isView ? '' : 'width: 0;opacity: 0;'}
  &:before{
    content: '';
    display: block;
    position: absolute;
    right: 0;
    top: 9px;
    height: 2px;
    width: 145px;
    background: #FF9100;
  }
`