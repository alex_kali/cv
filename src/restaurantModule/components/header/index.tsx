import {FC} from "react";
import {useNavigation} from "src/restaurantModule/utils/useNavigation";
import {
  HeaderStyled,
  InstagramStyled,
  LogoStyled,
  PageLinksStyled,
  PageLinkStyled,
  SocialLinksStyled,
  TelegramStyled,
  TwitterStyled,
  UserIconStyled,
  UserLinksStyled,
  UsernameStyled,
  VkStyled,
  WrapperLinksStyled,
  WrapperLogoStyled
} from "./styled";
import {useLocation, useParams} from "react-router-dom";
import {User} from "../../store/user/model";
import {useToken} from "../../store/user/useToken";

export const Header:FC = () => {
  const location = useLocation()
  const navigate = useNavigation()

  const { position, page }:any = useParams()

  useToken()
  const userData = User.useData()

  return (
    <HeaderStyled position={position as string}>
      <WrapperLogoStyled page={page as string}>
        <LogoStyled page={page as string}/>
      </WrapperLogoStyled>

      <WrapperLinksStyled position={position as string}>
        <SocialLinksStyled position={position as string}>
          <InstagramStyled/>
          <TelegramStyled/>
          <TwitterStyled/>
          <VkStyled/>
        </SocialLinksStyled>

        <PageLinksStyled>
          <PageLinkStyled onClick={()=>navigate('/restaurant/center/home')} isActive={location.pathname === '/restaurant/center/home'}>HOME</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/restaurant/center/menu')} isActive={location.pathname === '/restaurant/center/menu'}>MENU</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/restaurant/top/aboutAs')} isActive={location.pathname === '/restaurant/top/aboutAs'}>ABOUT US</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/restaurant/top/gallery')} isActive={location.pathname === '/restaurant/top/gallery'}>GALLERY</PageLinkStyled>
          <PageLinkStyled onClick={()=>navigate('/restaurant/top/vacancies')} isActive={location.pathname === '/restaurant/top/vacancies'}>VACANCIES</PageLinkStyled>
        </PageLinksStyled>

        <UserLinksStyled onClick={()=> {
          userData ? navigate('/restaurant/right/userPage') : navigate('/restaurant/right/login')
        }}>
          {userData ? <><UserIconStyled/><UsernameStyled>+{userData.username}</UsernameStyled></> : 'LOGIN / REGISTRATION'}
        </UserLinksStyled>


      </WrapperLinksStyled>
    </HeaderStyled>
  )
}