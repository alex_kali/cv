import React, {FC, useEffect} from "react";
import {useNavigation} from "src/restaurantModule/utils/useNavigation";
import {ArrowStyled, AuthMenuStyled, ItemMenuStyled} from "./styled";
import {useParams} from "react-router-dom";
import {User} from "../../store/user/model";

export const AuthMenu:FC = () => {
  const navigate = useNavigation()
  const { page }:any = useParams()
  const userData = User.useData()

  useEffect(() => {
    if((page === 'login' || page === 'registration') && userData){
      navigate('/restaurant/right/userPage')
    }
  },[page, userData, navigate])

  return (
    <AuthMenuStyled>
      <ItemMenuStyled isActive={true} onClick={()=>navigate('/restaurant/center/menu')}>
        <ArrowStyled/>
      </ItemMenuStyled>
      {!userData &&
        <>
          <ItemMenuStyled isActive={page === 'login'} onClick={()=>navigate('/restaurant/right/login')}>
            Authorization
          </ItemMenuStyled>

          <ItemMenuStyled isActive={page === 'registration'} onClick={()=>navigate('/restaurant/right/registration')}>
            Registration
          </ItemMenuStyled>
        </>
      }
      {userData &&
      <>
        <ItemMenuStyled isActive={page === 'userPage'} onClick={()=>navigate('/restaurant/right/userPage')}>
          Profile
        </ItemMenuStyled>
        <ItemMenuStyled isActive={false} onClick={()=> {
          User.options.logout();
          navigate('/restaurant/right/login')
        }}>
          Go out
        </ItemMenuStyled>
      </>
      }

    </AuthMenuStyled>
  )
}