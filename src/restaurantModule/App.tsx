import React from 'react';
import {Provider} from "react-redux";
import {
  Route, Switch, useRouteMatch,
} from "react-router-dom";
import 'rc-slider/assets/index.css';
import {store} from "src/restaurantModule/store/store";
import {PageController} from "./pages/pageController";

declare global {
  interface Window {
    dispatch:any;
  }
}

export function RestaurantApp() {
  let { path } = useRouteMatch();
  // @ts-ignore
  import('./index.css');
  // @ts-ignore
  import('./App.css');
  return (
    <Switch>
      <Route path={`${path}/:position/:page`}>
        <Provider store={store}>
          <PageController />
        </Provider>
      </Route>
    </Switch>
  );
}
