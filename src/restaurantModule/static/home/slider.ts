import png1 from "./icons/1.png"
import png2 from "./icons/2.png"
import png3 from "./icons/3.png"
import png4 from "./icons/4.jpg"
import png5 from "./icons/5.jpg"
import png6 from "./icons/6.jpg"
import png7 from "./icons/7.jpg"
import png8 from "./icons/8.jpg"
import png9 from "./icons/9.jpg"

export const sliderHomeData = [
  {
    url: png1,
    name: 'Events',
    urlPage: '/restaurant/bottom/eventOrganisation',
    pageName: 'eventOrganisation',
  },
  {
    url: png8,
    name: 'Delivery',
    urlPage: '/restaurant/bottom/delivery',
    pageName: 'delivery',
  },
  {
    url: png9,
    name: 'Booking',
    urlPage: '/restaurant/bottom/booking',
    pageName: 'booking',
  },
  {
    url: png2,
    name: 'Meat dishes',
    urlPage: '/restaurant/bottom/meatDishes',
    pageName: 'meatDishes',
  },
  {
    url: png3,
    name: 'Cold dishes',
    urlPage: '/restaurant/bottom/coldDishes',
    pageName: 'coldDishes',
  },
  {
    url: png4,
    name: 'Dessert',
    urlPage: '/restaurant/bottom/desserts',
    pageName: 'desserts',
  },
  {
    url: png5,
    name: 'Alcoholic drinks',
    urlPage: '/restaurant/bottom/alcoholicDrinks',
    pageName: 'alcoholicDrinks',
  },
  {
    url: png6,
    name: 'Hot drinks',
    urlPage: '/restaurant/bottom/hotDrinks',
    pageName: 'hotDrinks',
  },
  {
    url: png7,
    name: 'Cold drinks',
    urlPage: '/restaurant/bottom/coldDrinks',
    pageName: 'coldDrinks',
  },
]