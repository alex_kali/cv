import {FC} from "react";
import {useNavigation} from "src/restaurantModule/utils/useNavigation";
import {
  HomePagePageStyled,
  LeftBlockStyled,
  PromotionButtonStyled, PromotionLogoStyled,
  RightBlockStyled,
  ShowAllMenuStyled,
  TitleStyled
} from "./styled";
import {sliderHomeData} from "../../static/home/slider";
import {Slider} from "../../components/slider";
import {ViewAllStyled} from "../../components/slider/styled";
import {useParams} from "react-router-dom";

export const HomePage:FC = () => {
  const { page, position }: any = useParams()
  const navigate = useNavigation()
  return (
    <HomePagePageStyled position={position as string}>
      <LeftBlockStyled isView={!(page !== 'home')}>
        <TitleStyled>Exclusive menu to your taste</TitleStyled>
        <ShowAllMenuStyled onClick={() => navigate('/restaurant/center/authMenu')}>Watch all the menu</ShowAllMenuStyled>
        <PromotionButtonStyled><PromotionLogoStyled/>Promotions for today</PromotionButtonStyled>
      </LeftBlockStyled>

      <RightBlockStyled isView={!(page !== 'home')}>
        <Slider images={sliderHomeData}/>
        <ViewAllStyled isView={!(page !== 'home')} onClick={() => navigate('/restaurant/center/authMenu')}>Watch all</ViewAllStyled>
      </RightBlockStyled>
    </HomePagePageStyled>
  )
}