import styled from "styled-components";

export const AuthFormStyled = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`

export const WrapperFormStyled = styled.div`
  width: 600px;
  border: 2px solid #36393E;
  border-radius: 16px;
  padding-top: 60px;
  position: relative;
  white-space: nowrap;
  overflow: hidden;
  transition-duration: 0.5s;
`

export const WrapperTitleStyled = styled.div`
  position: absolute;
  left: 0;
  top: 0px;
  height: 60px;
  width: 100%;
`

export const TitleStyled = styled.div<{isActive: boolean}>`
  border-bottom: 2px solid #36393E;
  float: left;
  height: 100%;
  width: calc(50% - 1px);
  color: #73777D;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition-duration: 0.3s;
  ${props => props.isActive ? 'color: #FF9100;' : ''}
`

export const WrapperRegistrationStyled = styled.div<{isActive: boolean}>`
  width: calc(100% - 60px);
  height: calc(100% - 120px);
  padding: 30px;
  position: absolute;
  left: 100%;
  top: 60px;
  transition-duration: 0.5s;
  ${props => props.isActive ? 'left: 0;' : ''}
`

export const WrapperLoginStyled = styled.div<{isActive: boolean}>`
  width: calc(100% - 60px);
  //height: calc(100% - 120px);
  padding: 30px;
  
  position: absolute;
  left: -100%;
  top: 60px;
  transition-duration: 0.5s;
   ${props => props.isActive ? 'left: 0;' : ''}
`