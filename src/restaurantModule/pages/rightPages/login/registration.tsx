import {FC, useEffect} from "react";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {InputTitleStyled} from "../../../utils/phoenix/titles/inputTitle";
import {PhoneInput} from "../../../utils/phoenix/inputs/phoneInput";
import {EmailInput} from "../../../utils/phoenix/inputs/emailInput";
import {PasswordInput} from "../../../utils/phoenix/inputs/password";
import {SubmitButton} from "../../../utils/phoenix/buttons";
import {IRegistration, User} from "../../../store/user/model";

export const Registration:FC = () => {
  const form = useForm({name: 'registration'})

  const password1 = form.useFieldSelector('password')
  const password2 = form.useFieldSelector('password2')

  useEffect(()=>{
    if(password1 !== password2){
      form.changeIsValidateField('password2',false)
      form.changeMessageErrorField('password2', 'Fields must match')
    }
  },[password1, password2, form])

  return (
    <WrapperForm form={form} onSubmit={(data:IRegistration) => {User.options.registration(data)}}>
      <InputTitleStyled>Phone number</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Email</InputTitleStyled>
      <EmailInput name={'email'} required/>

      <InputTitleStyled>Password</InputTitleStyled>
      <PasswordInput name={'password'} required/>

      <InputTitleStyled>Repeat password</InputTitleStyled>
      <PasswordInput name={'password2'} required isNotValidate/>

      <SubmitButton text={'Register'}/>
    </WrapperForm>
  )
}