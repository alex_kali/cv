import {FC} from "react";
import {useForm, WrapperForm} from "react-redux-hook-form";
import {InputTitleStyled} from "../../../utils/phoenix/titles/inputTitle";
import {PhoneInput} from "../../../utils/phoenix/inputs/phoneInput";
import {PasswordInput} from "../../../utils/phoenix/inputs/password";
import {SubmitButton} from "../../../utils/phoenix/buttons";
import {ILogin, User} from "../../../store/user/model";

export const Login:FC = () => {
  const form = useForm({name: 'login'})
  return (
    <WrapperForm form={form} onSubmit={(data:ILogin) => {User.options.login(data)}}>
      <InputTitleStyled>Phone number</InputTitleStyled>
      <PhoneInput name={'username'} required/>

      <InputTitleStyled>Password</InputTitleStyled>
      <PasswordInput name={'password'} required/>

      <SubmitButton text={'Authorize'}/>
    </WrapperForm>
  )
}