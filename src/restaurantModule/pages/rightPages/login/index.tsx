import React, {FC} from "react";
import {useNavigation} from "src/restaurantModule/utils/useNavigation";
import {
  AuthFormStyled,
  TitleStyled,
  WrapperFormStyled,
  WrapperLoginStyled,
  WrapperRegistrationStyled,
  WrapperTitleStyled
} from "./styled";
import { useParams} from "react-router-dom";
import {Registration} from "./registration";
import {Login} from "./login";

export const AuthForm:FC = () => {
  const navigate = useNavigation()
  const { page }: any = useParams()
  return (
    <AuthFormStyled>
      <WrapperFormStyled style={{height: page === 'registration' ? '510px' : '320px'}}>
        <WrapperTitleStyled>
          <TitleStyled isActive={page === 'login'} onClick={()=>navigate('/restaurant/right/login')}>Authorization</TitleStyled>
          <TitleStyled isActive={page === 'registration'} onClick={()=>navigate('/restaurant/right/registration')} style={{borderLeft: '2px solid #36393E'}}>Registration</TitleStyled>
        </WrapperTitleStyled>

        <WrapperLoginStyled isActive={page !== 'registration'}>
          <Login/>
        </WrapperLoginStyled>

        <WrapperRegistrationStyled isActive={page === 'registration'}>
          <Registration/>
        </WrapperRegistrationStyled>
      </WrapperFormStyled>
    </AuthFormStyled>
  )
}