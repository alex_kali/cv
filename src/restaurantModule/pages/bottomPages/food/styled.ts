import styled from "styled-components";
import {ReactComponent as Plus} from './icons/plus.svg';
import {ReactComponent as More} from './icons/more.svg';
import {ReactComponent as Arrow} from './icons/arrow.svg';

export const FoodPageStyled = styled.div<{isView: boolean}>`
  width: 40%;
  height: 100%;
  min-width: 400px;
  margin-left: 30%;
  transition-duration: 0.5s;
  ${props => props.isView ? `
    margin-left: 55%;
  ` : ``}
`

export const TitleStyled = styled.div`
  color: #73777D;
  font-family: "LatoWebBold", sans-serif;
  font-size: 32px;
  margin-top: 30px;
  padding-bottom: 10px;
  margin-bottom: 10px;
  text-align: center;
  border-bottom: 2px solid #73777D;
`

export const MenuFoodStyled = styled.div`
  height: calc(100% - 110px);
  width: calc(100% - 20px);
  border-bottom: 2px solid #73777D;
  overflow-y: auto;
  padding-right: 10px;
  padding-left: 10px;
  position: relative;
`

export const MenuItemStyled = styled.div<{isActive: boolean}>`
  clear: both;
  padding-bottom: 10px;
  height: 15px;
  width: 100%;
  display: flex;
  cursor: pointer;
  & div,& div:before{
    transition-duration: 0.2s;
  }
  &:hover{
    & div,& div:before{
      color: #FF9100 !important;
    }
  }
  ${props => props.isActive ? `
    & div,& div:before{
      color: #FF9100 !important;
    }
  ` : ''}
`

export const MenuItemNameStyled = styled.div`
  color: #73777D;
  width: auto;
`

export const MenuItemPriceStyled = styled.div`
  color: #73777D;
  width: auto;
`

export const CenterDotsStyled = styled.div`
  height: 20px;
  margin-left: 20px;
  margin-right: 20px;
  width: auto;
  flex: 1 1 100px;
  overflow: hidden;
  &:before{
    float: left;
    width: 0;
    white-space: nowrap;
    color: #73777D;
    content:
            ". . . . . . . . . . . . . . . . . . . . "
            ". . . . . . . . . . . . . . . . . . . . "
            ". . . . . . . . . . . . . . . . . . . . "
            ". . . . . . . . . . . . . . . . . . . . "
            ". . . . . . . . . . . . . . . . . . . . "
            ". . . . . . . . . . . . . . . . . . . . "
  }
`

export const MoreStyled = styled(More)`
  color: #73777D !important;
  width: 20px;
  height: 20px;
  margin-left: 5px;
  margin-right: 5px;
  path{
    fill: #73777D;
  }
  &:hover{
    & path{
      fill: #FF9100 !important;
    }
  }
`

export const AddStyled = styled(Plus)`
  color: #73777D !important;
  width: 20px;
  height: 20px;
  margin-left: 10px;
  margin-right: 10px;
  path{
    fill: #73777D !important;
  }
  &:hover{
    & path{
      fill: #FF9100 !important;
    }
  }
`

export const MoreInformationStyled = styled.div<{isView: boolean}>`
  width: 45%;
  height: 100%;
  position: absolute;
  transition-duration: 0.5s;
  top: 30px;
  ${props => props.isView ? 'left: 5%;opacity: 1;' : 'left: -40%;opacity: 0;'}
`

export const ImageStyled = styled.div`
  height: 260px;
  width: 80%;
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
  margin-top: 20px;
  margin-bottom: 50px;
  background-size: cover;
  background-position: center;
  border-radius: 8px;
`

export const MoreInformationItem = styled.div`
  margin-top: 5px;
  padding-left: 40px;
  padding-right: 40px;
  clear: both;
  padding-bottom: 10px;
  height: 15px;
  width: calc(100% - 80px);
  display: flex;
  max-width: 640px;
  margin-left: auto;
  margin-right: auto;
`

export const MoreInformationTitleStyled = styled.div`
  margin-top: 20px;
  margin-bottom: 30px;

  color: #73777D;
  font-family: "LatoWebBold",sans-serif;
  font-size: 32px;
  padding-bottom: 10px;
  text-align: center;
`

export const ArrowStyled = styled(Arrow)`
  color: #73777D !important;
  width: 100px;
  height: 30px;
  position: absolute;
  top: 24px;
  right: 0px;
  cursor: pointer;
  path{
    fill: #73777D !important;
  }
  &:hover{
    & path{
      fill: #FF9100 !important;
    }
  }
`

export const IngredientsItemStyled = styled.div`
  width: calc(33.33333333333%);
  float: left;
  margin-bottom: 10px;
  &:before{
    content: '';
    width: 5px;
    height: 5px;
    display: block;
    background: #73777D;
    float: left;
    margin-top: 6px;
    margin-right: 10px;
    border-radius: 50%;
  }
`


export const MoreInformationAnimationStyled = styled.div`
  width: 45%;
  height: 100%;
  position: absolute;
  transition-duration: 0.5s;
  left: 5%;
  top: -100%;
  opacity: 1;
`

export const WrapperFoodPageStyled = styled.div`
  height: 100%;
  transition-duration: 0.5s;
  
  &.sliderToTop #MoreInformationStyled{
    transition-duration: 0s;
    top: -100%;
  }

  &.sliderToBottom #MoreInformationStyled{
    transition-duration: 0s;
    top: 100%;
  }

  &.sliderToTop #MoreInformationAnimationStyled, &.sliderToBottom #MoreInformationAnimationStyled{
    transition-duration: 0s !important;
    opacity: 1 !important;
    top: 30px !important;
  }
  &.sliderToCenter #MoreInformationAnimationStyled{
    transition-duration: 0s !important;
    opacity: 1 !important;
    top: 30px !important;
    left: 5% !important;
  }
`