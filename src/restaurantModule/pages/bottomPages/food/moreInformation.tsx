import {
  CenterDotsStyled,
  ImageStyled,
  MenuItemNameStyled,
  MenuItemPriceStyled,
  MoreInformationStyled,
  MoreInformationItem, MoreInformationTitleStyled, ArrowStyled, IngredientsItemStyled
} from "./styled";
import {FC} from "react";
import {IItemMenu} from "./menuItemView";
import {MoreInformationTextStyled} from "../styled";

export const MoreInformation:FC<{item: IItemMenu | undefined, setIsView: any, isView: boolean}> = (props) => {
  console.log(props.item?.image)
  return (
    <MoreInformationStyled isView={props.isView} id={'MoreInformationStyled'}>
      {props.item &&
        <div style={{height: '100%', width: '100%'}}>
          <MoreInformationTitleStyled>{props.item.name}</MoreInformationTitleStyled>
          <ImageStyled style={{backgroundImage:  `url(${props.item.image})`}}/>
          <MoreInformationItem>
            <MenuItemNameStyled>Proteins</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.proteins}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Fats</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.fats}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Carbohydrates</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.carbohydrates}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationItem>
            <MenuItemNameStyled>Calories</MenuItemNameStyled>
            <CenterDotsStyled />
            <MenuItemPriceStyled>{props.item.calories}</MenuItemPriceStyled>
          </MoreInformationItem>
          <MoreInformationTextStyled>{props.item.ingredients.map((item)=><IngredientsItemStyled>{item}</IngredientsItemStyled>)}</MoreInformationTextStyled>
          <ArrowStyled onClick={() => props.setIsView()}/>
        </div>
      }
    </MoreInformationStyled>
  )
}