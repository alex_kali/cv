import {FC, useEffect, useState} from "react";
import {DeliveryStyled, TitleStyled} from "./styled";
import {LeftBlockStyled, RightBlockStyled} from "../styled";
import {night} from "../../../utils/mapOptions/night";

export const Delivery:FC = () => {
  const [map, setMap] = useState()
  useEffect(()=>{
    if(!map) {
      const map = new google.maps.Map(document.getElementById("map") as HTMLElement, {
        center: {lat: 54.397, lng: 40.644},
        zoom: 6,
        styles: night
      });
      setMap(map as any)
    }
  },[map])

  return (
    <DeliveryStyled>
      <LeftBlockStyled style={{marginTop: 0, width: 700}}>
        <TitleStyled>Your basket is empty.</TitleStyled>
      </LeftBlockStyled>
      <RightBlockStyled style={{width: 'calc(100% - 700px)'}}>
        <div id={'map'} style={{width: 'calc(100% - 6%)', height: 'calc(100% - 6%)', marginLeft: '3%',marginTop: '3%', borderRadius: 8}}/>
      </RightBlockStyled>
    </DeliveryStyled>
  )
}