import {FC} from "react";
import {EventOrganisationStyled} from "./styled";
import {LeftBlockStyled, RightBlockStyled, TextStyled, TitleStyled} from "../styled";
import {eventOrganisationsExample} from "./examples";
import {Event} from "./event";

export const EventOrganisation:FC = () => {
  return (
    <EventOrganisationStyled>
        <LeftBlockStyled>
          <TitleStyled>Organization of holidays and turnkey events</TitleStyled>
          <TextStyled>We make the process of organization simple and invisible to customers, help save time and nerves.</TextStyled>
          <TextStyled>A wide selection of programs offered by the studio will impress anyone. All offers are a set of services in which you can choose events, focusing on preferences, tastes, scale and specificity of the event.</TextStyled>
        </LeftBlockStyled>
        <RightBlockStyled>
          {eventOrganisationsExample.map((item) => <Event event={item}/>)}
        </RightBlockStyled>
    </EventOrganisationStyled>
  )
}