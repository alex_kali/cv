import {EventOrganisation} from "./eventOrganization";
import {Delivery} from "./delivery";
import {Booking} from "./booking";
import {FoodPage} from "./food";

export const PagesBottom:any = {
  eventOrganisation: {
    component: <EventOrganisation/>,
    position: 1,
  },
  delivery: {
    component: <Delivery/>,
    position: 2,
  },
  booking: {
    component: <Booking/>,
    position: 3,
  },
  meatDishes: {
    component: <FoodPage name={'meatDishes'}/>,
    position: 4,
  },
  coldDishes: {
    component: <FoodPage name={'coldDishes'}/>,
    position: 5,
  },
  desserts: {
    component: <FoodPage name={'desserts'}/>,
    position: 6
  },
  alcoholicDrinks: {
    component: <FoodPage name={'alcoholicDrinks'}/>,
    position: 7,
  },
  hotDrinks: {
    component: <FoodPage name={'hotDrinks'}/>,
    position: 8,
  },
  coldDrinks: {
    component: <FoodPage name={'coldDrinks'}/>,
    position: 9,
  }
}