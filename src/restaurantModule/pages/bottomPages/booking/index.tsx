import {FC} from "react";
import {BookingStyled, ButtonStyled, TitleStyled} from "./styled";
import {Place} from "./place";
import {Time} from "./time";

export const Booking:FC = () => {
  return (
    <BookingStyled>
      <TitleStyled>Seating arrangement</TitleStyled>
      <Place />
      <TitleStyled>Booking time</TitleStyled>
      <Time />
      <ButtonStyled>Book</ButtonStyled>
    </BookingStyled>
  )
}