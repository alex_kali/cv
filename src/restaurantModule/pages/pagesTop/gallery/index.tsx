import {FC} from "react";
import {GalleryStyled} from "./styled";
import GalleryLibrary from "react-photo-gallery";
import {galleryPhotos} from "./galleryPhotos";

export const Gallery:FC = () => {
  return (
    <GalleryStyled>
      <GalleryLibrary photos={galleryPhotos} />
    </GalleryStyled>
  )
}