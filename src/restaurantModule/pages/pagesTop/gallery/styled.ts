import styled from "styled-components";

export const GalleryStyled = styled.div`
  width: calc(100% - 20px);
  height: calc(100% - 20px);
  overflow-y: auto;
  padding: 5px;
  margin: 5px;
  img {
    object-fit: cover;
    border-radius: 8px;
  }
`