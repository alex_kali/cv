import {FC, useState} from "react";
import {
  ButtonStyled,
  ColumnStyled, ItemColumnStyled,
  VacancyDescriptionStyled,
  VacancyExperienceStyled,
  VacancyPlaceStyled,
  VacancyStyled,
  VacancyTitleStyled,
  WrapperVacancyStyled
} from "./styled";

export const VacanciesView:FC = () => {
  const [idActive, setIdActive] = useState(0)
  return (
    <>
      <WrapperVacancyStyled isActive={idActive === 1} onClick={() => setIdActive(1)}>
        <VacancyStyled>
          <VacancyTitleStyled>MANAGER INTERNSHIP</VacancyTitleStyled>
          <VacancyPlaceStyled>Moscow</VacancyPlaceStyled>
          <VacancyExperienceStyled>from one year</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Leadership qualities</ItemColumnStyled>
            <ItemColumnStyled>Communication skills</ItemColumnStyled>
            <ItemColumnStyled>Activity</ItemColumnStyled>
            <ItemColumnStyled>Positivity</ItemColumnStyled>
            <ItemColumnStyled>Willingness to follow company standards</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Organization of the restaurant</ItemColumnStyled>
            <ItemColumnStyled>Personnel management: training, adaptation, performance evaluation</ItemColumnStyled>
            <ItemColumnStyled>Communication with guests</ItemColumnStyled>
            <ItemColumnStyled>Achievement of targets</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Stable salary + bonuses after graduation</ItemColumnStyled>
            <ItemColumnStyled>Flexible schedule</ItemColumnStyled>
            <ItemColumnStyled>Your interests are our priority</ItemColumnStyled>
            <ItemColumnStyled>Rapid career advancement to Deputy Restaurant Manager or Burger King Restaurant Director</ItemColumnStyled>
            <ItemColumnStyled>Free training according to the best international standards</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Fill out the form</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 2} onClick={() => setIdActive(2)}>
        <VacancyStyled>
          <VacancyTitleStyled>MANAGER INTERNSHIP</VacancyTitleStyled>
          <VacancyPlaceStyled>Moscow</VacancyPlaceStyled>
          <VacancyExperienceStyled>from one year</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Leadership qualities</ItemColumnStyled>
            <ItemColumnStyled>Communication skills</ItemColumnStyled>
            <ItemColumnStyled>Activity</ItemColumnStyled>
            <ItemColumnStyled>Positivity</ItemColumnStyled>
            <ItemColumnStyled>Willingness to follow company standards</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Organization of the restaurant</ItemColumnStyled>
            <ItemColumnStyled>Personnel management: training, adaptation, performance evaluation</ItemColumnStyled>
            <ItemColumnStyled>Communication with guests</ItemColumnStyled>
            <ItemColumnStyled>Achievement of targets</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Stable salary + bonuses after graduation</ItemColumnStyled>
            <ItemColumnStyled>Flexible schedule</ItemColumnStyled>
            <ItemColumnStyled>Your interests are our priority</ItemColumnStyled>
            <ItemColumnStyled>Rapid career advancement to Deputy Restaurant Manager or Burger King Restaurant Director</ItemColumnStyled>
            <ItemColumnStyled>Free training according to the best international standards</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Fill out the form</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 3} onClick={() => setIdActive(3)}>
        <VacancyStyled>
          <VacancyTitleStyled>MANAGER INTERNSHIP</VacancyTitleStyled>
          <VacancyPlaceStyled>Moscow</VacancyPlaceStyled>
          <VacancyExperienceStyled>from one year</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Leadership qualities</ItemColumnStyled>
            <ItemColumnStyled>Communication skills</ItemColumnStyled>
            <ItemColumnStyled>Activity</ItemColumnStyled>
            <ItemColumnStyled>Positivity</ItemColumnStyled>
            <ItemColumnStyled>Willingness to follow company standards</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Organization of the restaurant</ItemColumnStyled>
            <ItemColumnStyled>Personnel management: training, adaptation, performance evaluation</ItemColumnStyled>
            <ItemColumnStyled>Communication with guests</ItemColumnStyled>
            <ItemColumnStyled>Achievement of targets</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Stable salary + bonuses after graduation</ItemColumnStyled>
            <ItemColumnStyled>Flexible schedule</ItemColumnStyled>
            <ItemColumnStyled>Your interests are our priority</ItemColumnStyled>
            <ItemColumnStyled>Rapid career advancement to Deputy Restaurant Manager or Burger King Restaurant Director</ItemColumnStyled>
            <ItemColumnStyled>Free training according to the best international standards</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Fill out the form</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 4} onClick={() => setIdActive(4)}>
        <VacancyStyled>
          <VacancyTitleStyled>MANAGER INTERNSHIP</VacancyTitleStyled>
          <VacancyPlaceStyled>Moscow</VacancyPlaceStyled>
          <VacancyExperienceStyled>from one year</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Leadership qualities</ItemColumnStyled>
            <ItemColumnStyled>Communication skills</ItemColumnStyled>
            <ItemColumnStyled>Activity</ItemColumnStyled>
            <ItemColumnStyled>Positivity</ItemColumnStyled>
            <ItemColumnStyled>Willingness to follow company standards</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Organization of the restaurant</ItemColumnStyled>
            <ItemColumnStyled>Personnel management: training, adaptation, performance evaluation</ItemColumnStyled>
            <ItemColumnStyled>Communication with guests</ItemColumnStyled>
            <ItemColumnStyled>Achievement of targets</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Stable salary + bonuses after graduation</ItemColumnStyled>
            <ItemColumnStyled>Flexible schedule</ItemColumnStyled>
            <ItemColumnStyled>Your interests are our priority</ItemColumnStyled>
            <ItemColumnStyled>Rapid career advancement to Deputy Restaurant Manager or Burger King Restaurant Director</ItemColumnStyled>
            <ItemColumnStyled>Free training according to the best international standards</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Fill out the form</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
      <WrapperVacancyStyled isActive={idActive === 5} onClick={() => setIdActive(5)}>
        <VacancyStyled>
          <VacancyTitleStyled>MANAGER INTERNSHIP</VacancyTitleStyled>
          <VacancyPlaceStyled>Moscow</VacancyPlaceStyled>
          <VacancyExperienceStyled>from one year</VacancyExperienceStyled>
        </VacancyStyled>
        <VacancyDescriptionStyled>
          <ColumnStyled>
            <ItemColumnStyled>Leadership qualities</ItemColumnStyled>
            <ItemColumnStyled>Communication skills</ItemColumnStyled>
            <ItemColumnStyled>Activity</ItemColumnStyled>
            <ItemColumnStyled>Positivity</ItemColumnStyled>
            <ItemColumnStyled>Willingness to follow company standards</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Organization of the restaurant</ItemColumnStyled>
            <ItemColumnStyled>Personnel management: training, adaptation, performance evaluation</ItemColumnStyled>
            <ItemColumnStyled>Communication with guests</ItemColumnStyled>
            <ItemColumnStyled>Achievement of targets</ItemColumnStyled>
          </ColumnStyled>
          <ColumnStyled>
            <ItemColumnStyled>Stable salary + bonuses after graduation</ItemColumnStyled>
            <ItemColumnStyled>Flexible schedule</ItemColumnStyled>
            <ItemColumnStyled>Your interests are our priority</ItemColumnStyled>
            <ItemColumnStyled>Rapid career advancement to Deputy Restaurant Manager or Burger King Restaurant Director</ItemColumnStyled>
            <ItemColumnStyled>Free training according to the best international standards</ItemColumnStyled>
          </ColumnStyled>
          <ButtonStyled>Fill out the form</ButtonStyled>
        </VacancyDescriptionStyled>
      </WrapperVacancyStyled>
    </>
  )
}