import {FC, useState} from "react";
import {
  BottomLeftBlockStyled,
  BottomRightBlockStyled,
  ContentStyled,
  TitleStyled,
  TopLeftBlockStyled,
  TopRightBlockStyled,
  VacanciesStyled,
} from "./styled";
import {VacanciesView} from "./vacancies";
import {Advantages} from "./advantages";
import {Reviews} from "./reviews";
import {Hiring} from "./hiring";

export const Vacancies:FC = () => {
  const [page, changePage] = useState('')

  return (
    <VacanciesStyled>
      <TopLeftBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('jobs')} className={'title'}>Jobs</TitleStyled>
        <ContentStyled className={'content'}>
          <VacanciesView />
        </ContentStyled>
      </TopLeftBlockStyled>

      <TopRightBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('advantages')} className={'title'}>Principles</TitleStyled>
        <ContentStyled className={'content'}>
          <Advantages/>
        </ContentStyled>
      </TopRightBlockStyled>

      <BottomLeftBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('reviews')} className={'title'}>Reviews</TitleStyled>
        <ContentStyled className={'content'}>
          <Reviews />
        </ContentStyled>
      </BottomLeftBlockStyled>

      <BottomRightBlockStyled page={page}>
        <TitleStyled onClick={()=>changePage('hiring')} className={'title'}>Recruitment process</TitleStyled>
        <ContentStyled className={'content'}>
          <Hiring />
        </ContentStyled>
      </BottomRightBlockStyled>
    </VacanciesStyled>
  )
}