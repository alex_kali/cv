import {FC} from "react";
import {HiringStyled, ItemHiringStyled} from "./styled";

export const Hiring:FC = () => {
  return (
    <HiringStyled>
      <ItemHiringStyled>Sending a questionnaire</ItemHiringStyled>
      <ItemHiringStyled>Interview</ItemHiringStyled>
      <ItemHiringStyled>Education</ItemHiringStyled>
      <ItemHiringStyled>Signing an agreement</ItemHiringStyled>
    </HiringStyled>
  )
}