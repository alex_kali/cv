import {FC} from "react";
import {AdvantagesLeftStyled, AdvantagesRightStyled, AdvantagesTextStyled, AdvantagesTitleStyled} from "./styled";

export const Advantages:FC = () => {
  return (
    <>
      <AdvantagesLeftStyled>
        <AdvantagesTitleStyled style={{color: '#4b7f0b', borderTopColor: '#4b7f0b'}}>GUEST IS THE MAIN PERSON</AdvantagesTitleStyled>
        <AdvantagesTextStyled>IN OUR RESTAURANT. THE PURPOSE OF OUR WORK IS TO EXCEED HIS EXPECTATIONS.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#931116', borderTopColor: '#931116'}}>WE RESPECT EACH OTHER</AdvantagesTitleStyled>
        <AdvantagesTextStyled>AND ALWAYS READY TO RENDER A HELPING HAND AND SUBSTITUTE A SHOULDER.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#703c2e', borderTopColor: '#703c2e'}}>WE LOVE LIFE</AdvantagesTitleStyled>
        <AdvantagesTextStyled>AND WITH OPTIMISM WE ACCEPT ITS CHALLENGES. IT IS EASY FOR WE TO COMMUNICATE, BECAUSE WE ARE ABLE TO NEGOTIATE.</AdvantagesTextStyled>

      </AdvantagesLeftStyled>
      <AdvantagesRightStyled>
        <AdvantagesTitleStyled style={{color: '#827f59', borderTopColor: '#827f59'}}>COURAGE TO SET HIGH GOALS</AdvantagesTitleStyled>
        <AdvantagesTextStyled>AND PERSEVERANCE TO ACHIEVE THEM.</AdvantagesTextStyled>

        <AdvantagesTitleStyled style={{color: '#a36212', borderTopColor: '#a36212'}}>WELCOME INITIATIVE</AdvantagesTitleStyled>
        <AdvantagesTextStyled>AND SELF DEVELOPMENT. WE APPRECIATE MERITS. WE REWARD THE RESULT.</AdvantagesTextStyled>
      </AdvantagesRightStyled>
    </>
  )
}