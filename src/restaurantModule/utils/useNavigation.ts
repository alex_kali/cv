import {useHistory} from "react-router-dom";

export const useNavigation = () => {
  const history = useHistory()
  return (url: string) => {
    history.push(url)
  }
}