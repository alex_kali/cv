import React, { FC } from 'react';
import {EmailInputStyled} from "./styled";
import {ErrorTitleStyled} from "../../titles/errorTitle";
import {validate} from "../../validate";
import {useField} from "react-redux-hook-form";

interface IEmail {
  name: string;
  required?: boolean;
}

export const EmailInput: FC<IEmail> = (props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    validateFunction: (value:string) => validate(value).string().max(30).email(),
  })

  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  return (
    <>
      <EmailInputStyled
        type={"email"} value={data}
        onChange={(e:any)=> {changeData(e.target.value)}}
        isValidate={isValidate || !isTouch}
        id={props.name}
      />
      {!isValidate && isTouch &&
        <ErrorTitleStyled className={'error'}>
          {messageError}
        </ErrorTitleStyled>
      }
    </>
  )
};