import {FC, useEffect, useState} from "react";
import {OptionsItemStyled, OptionsStyled, TitleStyled, ValueStyled, WrapperSelectStyled} from "./styled";
import {useField} from "react-redux-hook-form";


interface ISelectInput {
  name: string;
  data: Array<{text: string, value: string}>
  title: string;
  required?: boolean;
  onChange?: (value: string) => void;
}

export const SelectInput: FC<ISelectInput> = (props) => {
  const [isView, setIsView] = useState(false)

  const {useData} = useField({
    name: props.name,
    isRequired: props.required,
  })

  const [data, changeData] = useData()

  useEffect(()=>{
    if(props.onChange && data){
      props.onChange(data as string)
    }
  },[data, props])

  return (
    <WrapperSelectStyled>
      <TitleStyled>{props.title}:</TitleStyled>
      <ValueStyled onClick={()=>{setIsView(!isView)}} onMouseLeave={()=>{setIsView(false)}} isView={isView}>
        {props.data.filter(i=>i.value === data)?.[0]?.text}
        {isView &&
        <OptionsStyled>
          {props.data.map((i)=>
            <OptionsItemStyled key={i.value} checked={i.value === data} onClick={()=>{changeData(i.value)}}>{i.text}</OptionsItemStyled>
          )}
        </OptionsStyled>
        }
      </ValueStyled>
    </WrapperSelectStyled>
  )
}