import React, {FC, memo, useRef} from 'react';
import {TextStyled, TimeInputStyled, WrapperTimeInputStyled} from "./styled";

import {ErrorTitleStyled} from "../../titles/errorTitle";
import {useField} from "react-redux-hook-form";

interface ITimeInput {
  name: string;
  text: string;
  required?: boolean;
  className?: string;
  isTouch?: boolean;
}

export const TimeInput: FC<ITimeInput> = memo((props) => {
  const {useData, useIsValidate, useIsTouch, useMessageError} = useField({
    name: props.name,
    isRequired: props.required,
    isTouch: props.isTouch,
    isValidate: props.isTouch,
    initialValue: ':',
    validateFunction(data: string): { isValidate: boolean; messageError?: string } {
      const hour = data.split(':')[0]
      const minute = data.split(':')[1]
      if(+hour < 24 && +minute < 60){
        return {isValidate: true}
      }else{
        return {isValidate: false, messageError: 'Введите корректное время'}
      }
    }
  })
  const [data, changeData] = useData()
  const [isValidate, ] = useIsValidate()
  const [isTouch, ] = useIsTouch()
  const [messageError, ] = useMessageError()

  const hourInput = useRef<any>(null);
  const minuteInput = useRef<any>(null);

  return (
    <>
      <WrapperTimeInputStyled>
        <TextStyled>{props.text}</TextStyled>
        <TimeInputStyled
          ref={hourInput}
          value={data.split(':')[0]}
          onChange={(e:any) => {
            if(e.target.value.length <= 2){
              changeData(`${e.target.value}:${data.split(':')[1]}`)
            }
            if(e.target.value.length === 2){
              minuteInput.current.focus()
            }
          }}
          isValidate={isValidate || !isTouch}
          className={props.className}
          id={props.name + '1'}
        />
        :
        <TimeInputStyled
          ref={minuteInput}
          value={data.split(':')[1]}
          onChange={(e:any) => {
            if(e.target.value.length <= 2){
              changeData(`${data.split(':')[0]}:${e.target.value}`)
            }
            if(e.target.value.length === 0){
              hourInput.current.focus()
            }
          }}
          isValidate={isValidate || !isTouch}
          className={props.className}
          id={props.name + '2'}
        />
      </WrapperTimeInputStyled>
    {!isValidate && isTouch &&
      <ErrorTitleStyled className={'error'}>
        {messageError}
      </ErrorTitleStyled>
    }
  </>
  )
});