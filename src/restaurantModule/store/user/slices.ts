import {createSlice} from "@reduxjs/toolkit";

export const initialState: any = {
  user: null
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload
    },
    logoutUser: state => {

    },
  },
});

export const { setUser, logoutUser } = userSlice.actions

export default userSlice.reducer;