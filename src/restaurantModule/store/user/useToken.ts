import {useSelector} from "react-redux";
import useCookie from 'react-use-cookie';
import {useEffect} from "react";
import {User} from "./model";
import { useBeforeRender } from "react-redux-hook-form";

declare global {
  interface Window {
    token: string | undefined;
  }
}

export const useToken = () => {
  const [userToken, setUserToken] = useCookie('token');
  const token = useSelector((state: any) => state.user.user?.token)

  useBeforeRender(()=>{
    if(userToken && !token){
      window.token = userToken
      User.options.getData()
    }
  })

  useEffect(()=>{
    if(token){
      setUserToken('Token ' + token)
      window.token = 'Token ' + token
    }
  }, [token, setUserToken])

  return [userToken, setUserToken as any]
}