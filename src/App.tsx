import React from 'react';
import 'normalize.css';
import './style.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {CVApp} from "src/cvModule/app";
import {MapApp} from "src/mapViewerModule/App";
import {RecipeApp} from "src/recipeModule/App";
import {RestaurantApp} from "src/restaurantModule/App";
import {TestsApp} from "src/testsModule/app";

//http://localhost:3000/restaurant/center/home
function App() {
  return (
    <Router>
      <Switch>
        <Route path="/cv">
          <CVApp/>
        </Route>
        <Route path="/restaurant">
          <RestaurantApp/>
        </Route>
        <Route path="/map">
          <MapApp/>
        </Route>
        <Route path="/recipe">
          <RecipeApp/>
        </Route>
        <Route path="/tests">
          <TestsApp/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
